cd ../Data
wget "<train link>"
wget "<val link>"
wget "<test link>"
# <rename files to train.tsv, val.tsv, and test.tsv manually>
hadoop fs -mkdir Master
hadoop fs -mkdir Master/2020recsystwitter
hadoop fs -mkdir Master/2020recsystwitter/Data
hadoop fs -mkdir Data

hadoop fs -put training.tsv Data/training.tsv
hadoop fs -put val.tsv Data/val.tsv
hadoop fs -put sample_first10000.tsv Data/sample_first10000.tsv
hadoop fs -put train_14m.tsv Data/train_14m.tsv
hadoop fs -put val_1400k.tsv Data/val_1400k.tsv

hadoop fs -put training.parquet Master/2020recsystwitter/Data/training.parquet
hadoop fs -put val.parquet Master/2020recsystwitter/Data/val.parquet
hadoop fs -put sample_first10000.parquet Master/2020recsystwitter/Data/sample_first10000.parquet
hadoop fs -put train_14m.parquet Master/2020recsystwitter/Data/train_14m.parquet
hadoop fs -put val_1400k.parquet Master/2020recsystwitter/Data/val_1400k.parquet