from collections import defaultdict
import pingouin as pg
import pandas as pd


def run_multiple_friedman_tests(prauc_cf_dic, rce_cf_dic, dict_keys=None, engs=None, dv="evaluation",
                                withins=["algorithms"], subject="evaluated_on", friedman_method="chisq",
                                try_balanced_on_subject_if_not_enough_values_in_balanced_on_within=False,
                                target_col="target"):

    if dict_keys is None:
        dict_keys = withins
    else:
        assert len(withins) == len(dict_keys)

    if engs is None:
        engs = list(prauc_cf_dic[dict_keys[0]].keys())
        if "all" in engs:
            engs.remove("all")

    results_dic = defaultdict(list)

    for key, within in zip(dict_keys, withins):
        if (within == subject) or (within == target_col) or (key == 'all factors combined'):
            continue

        if within[:3] == "to_":
            key = "trained_on"

        if within[:3] == "eo_":
            key = "evaluated_on"

        for eng in engs:
            for metric_name, cf_dic in zip(["PRAUC", "RCE"], [prauc_cf_dic, rce_cf_dic],):
                df = cf_dic[key][eng]
                balanced_on = key

                if df.shape[0] <= 1:
                    s = f"Skipping metric={metric_name}, key={key}, within={within}, eng={eng} as there are not " \
                        f"enough values in the dataframe (shape {df.shape}) to perform the tests."
                    if try_balanced_on_subject_if_not_enough_values_in_balanced_on_within:
                        df = cf_dic[subject][eng]
                        balanced_on = subject
                        if df.shape[0] <= 1:
                            print(s, f"Using the subject {subject} as key also did not work (shape {df.shape}).")
                            continue
                    else:
                        print(s)
                        continue

                test_results = pg.friedman(data=df, dv=dv, within=within, subject=subject, method=friedman_method)

                for index, row in test_results.iterrows():
                    results_dic["test"].append(index)
                    results_dic["metric"].append(metric_name)
                    results_dic["target"].append(eng)
                    results_dic["within"].append(within)
                    results_dic["subject"].append(subject)
                    results_dic["balanced_on"].append(balanced_on)
                    results_dic["balanced_on_within"].append(balanced_on == key)
                    based_on = df[within].value_counts()
                    results_dic["instances"].append(based_on.sum())
                    results_dic["within_len"].append(len(based_on.index))
                    results_dic["within_list"].append(list(based_on.index))
                    results_dic["within_min_fr"].append(based_on.min())
                    results_dic["within_max_fr"].append(based_on.max())
                    subjects = df[subject].value_counts()
                    results_dic["subject_len"].append(len(subjects.index))
                    results_dic["subject_list"].append(list(subjects.index))
                    results_dic["subject_min_fr"].append(subjects.min())
                    results_dic["subject_max_fr"].append(subjects.max())
                    for col in row.index:
                        results_dic[col].append(row[col])

    return pd.DataFrame(results_dic)
