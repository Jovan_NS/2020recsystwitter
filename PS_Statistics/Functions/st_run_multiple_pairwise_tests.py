import pingouin as pg


def run_multiple_pairwise_tests(test_results_df, prauc_cf_dic, rce_cf_dic, dv="evaluation", alpha=0.05,
                                   reject_H0_col="reject_H0", parametric="False", alternative="two-sided",
                                   nan_policy="pairwise", return_desc=True, pairwise_padjust="holm"):
    pairwise_test_results = {}
    test_results_df = test_results_df.loc[test_results_df[reject_H0_col]]

    for _, row in test_results_df.iterrows():
        cf_dic = prauc_cf_dic if row["metric"] == "PRAUC" else rce_cf_dic
        data = cf_dic[row["balanced_on"]][row["target"]]
        dic_key = "-".join(["PostHoc", row["test"], row["metric"], row["target"], row["within"], row["subject"], row["balanced_on"]])
        pairwise_test_results[dic_key] = pg.pairwise_tests(data=data, dv=dv, within=row["within"],
                                                           subject=row["subject"], parametric=parametric, alpha=alpha,
                                                           alternative=alternative, nan_policy=nan_policy,
                                                           return_desc=return_desc, padjust=pairwise_padjust)
        p_col = "p-corr" if "p-corr" in pairwise_test_results[dic_key].columns else "p-unc"
        pairwise_test_results[dic_key][reject_H0_col] = pairwise_test_results[dic_key][p_col].apply(lambda x: x < alpha)


    return pairwise_test_results
