from Functions.st_get_common_factor_combinations import get_common_factor_combinations
import itertools
from datetime import datetime


def get_pairwise_common_factor_combinations(main_df,  factors_to_investigate=None, pairs_dict=None, all_factors=None,
                                            eval_cols=["evaluation",], print_progress=True):
    """
    Filters out all rows from dataframe <df> for which the values are not one of the pairs in <pairs>. If <pairs> None,
    all pairs for all all_factors in <factors_to_investigate> are checked. Then, the dataframe <df> is grouped by columns
    from each <factors_to_investigate> and all rows for the remaining columns in <all_factors> apart from the target
    factor column are groups are filtered out so that only the intersection remains (cf. get_common_factor_combinations).
    Returns the filtered dfs as a three-dimensional dict whose first key is the factor, while second and third keys are
    the values in <pairs_dict> for that factor. Example: https://prnt.sc/OjiVwq-h-Te-

    Parameters
    ----------
    main_df: the dataframe for which the transformations are to be done
    factors_to_investigate: a list of all_factors (key columns) to be investigated
    pairs_dict: a dict with keys from factors_to_investigate with pairs of values in <factor_to_investigate>
    all_factors: columns to be filtered
    eval_cols: eval_cols to be carried over
    print_progress: whether to print how far along the calculation has progressed

    Returns
    -------
    A dict of dataframe(s)
    """


    if all_factors is None:
        # https://prnt.sc/sHzaCLyYejnC
        all_factors = [col for col in main_df.columns if col not in eval_cols]

    if factors_to_investigate is None:
        factors_to_investigate = all_factors

    if pairs_dict is None:
        pairs_dict = {}
        for f in factors_to_investigate:
            pairs_dict[f] = list(itertools.combinations(main_df[f], 2))
    else:
        for key in pairs_dict:
            if key not in factors_to_investigate:
                raise ValueError(f"All keys of <pairs_dict> must be in <factors_to_investigate>! {key} is missing.")

        for f in factors_to_investigate:
            if f not in pairs_dict.keys():
                raise ValueError(f"Every factor from {f} must be a key in the pairs_dict. ({pairs_dict.keys()}). {f} is missing.")

    result = {}

    for f in factors_to_investigate:
        result[f] = {}
        if print_progress:
            print(f"At {datetime.now().strftime('%d.%m.%Y %H:%M:%S')} beginning with common factor {f} and pairs {pairs_dict[f]}.")

        for p1, p2 in pairs_dict[f]:
            help_df = main_df.loc[(main_df[f] == p1) | (main_df[f] == p2)]
            if p1 not in result[f]:
                result[f][p1] = {}
            if p2 not in result[f]:
                result[f][p2] = {}

            result[f][p1][p2] = get_common_factor_combinations(main_df=help_df, factors_to_investigate=[f,],
                                                               all_factors=all_factors, eval_cols=eval_cols,
                                                               print_progress=print_progress)[f]
            result[f][p2][p1] = result[f][p1][p2]

    return result