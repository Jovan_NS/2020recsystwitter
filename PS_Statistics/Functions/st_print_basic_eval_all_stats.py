from datetime import datetime
import os
import pandas as pd


def custom_print(s, print_progress):
    if print_progress:
        print(s)


def custom_display(df, df_name, col, flatten, print_progress, export_path):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.width', None):
        dfvc = pd.DataFrame(df[col].value_counts())
        dfvc.index.rename(col, inplace=True)
        if not flatten:
            columns = ['like', 'reply', 'retweet', 'quote', 'react']
            for column in columns:
                non_null_values = df[df[column].notnull()][col]
                unique_count = non_null_values.value_counts()
                unique_count.index.rename(col, inplace=True)
                unique_count.name = column
                dfvc = pd.concat([dfvc, unique_count], axis=1)
            del dfvc[col]
        else:
            pd.DataFrame(dfvc).rename(columns={col:"evaluations"},inplace=True)

        dfvc_plot = dfvc.plot.bar(rot=0).get_figure()

        if print_progress:
            print(f"Present {col}s in {df_name}")
            display(dfvc)

        if export_path is not None:
            csv_export_path = os.path.join(export_path, "csv", "-".join([df_name, col, "value_counts"])+".csv")
            tex_export_path = csv_export_path.replace("csv", "tex")
            plot_export_path = csv_export_path.replace("csv", "png")
            dfvc.to_csv(csv_export_path, index=True)
            dfvc.to_latex(tex_export_path, index=True,  caption=f"Counts of values of {col} in {df_name}",
                          longtable=True, label=f"tab{df_name}{col}".replace("_", "\\_"), position="ht")
            dfvc_plot.savefig(plot_export_path)



def print_basic_eval_all_stats(df, df_name, data_to_be_cosidered="N/A", new_cols_prefixes=None, new_cols_suffixes=None,
                               flatten=False, export_path=None, print_progress=False):

    custom_print(f"____{datetime.now().strftime('%d.%m.%Y %H:%M:%S')}____\n", print_progress=print_progress)
    custom_print(f"Read final evals for {df_name} of shape {df.shape} for data_to_be_cosidered={data_to_be_cosidered}.", print_progress=print_progress)
    if flatten:
        custom_print(f'{df_name} shape: {df.shape}, of that non-nans for evaluation={df.loc[df["evaluation"].notna()].shape[0]}.', print_progress=print_progress)
    else:
        custom_print(f'{df_name} shape: {df.shape}, of that non-nans for like={df.loc[df["like"].notna()].shape[0]}; reply={df.loc[df["reply"].notna()].shape[0]}; retweet={df.loc[df["retweet"].notna()].shape[0]}; quote={df.loc[df["quote"].notna()].shape[0]}; react={df.loc[df["react"].notna()].shape[0]}.', print_progress=print_progress)

    for col in ["algorithm", "note", "feature_selection", "trained_on", "evaluated_on"]:
        custom_display(df=df, df_name=df_name, col=col, flatten=flatten, print_progress=print_progress, export_path=export_path)

    if (new_cols_prefixes is not None) and (new_cols_suffixes is not None):
        for p in new_cols_prefixes:
            for s in new_cols_suffixes:
                custom_display(df=df, df_name=df_name, col=p+s, flatten=flatten, print_progress=print_progress, export_path=export_path)

    if print_progress:
        display(df.head())
        print("________\n")
