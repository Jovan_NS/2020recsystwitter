def loc_by_dict(df, filters):
    """
    Applies the loc function on a pandas dataframe using a dict of column-value pairs as filters.

    Parameters
    ----------
    df: The dataframe to filter.
    filters: A dict whose keys are columns and values list of values to be filtersed

    Returns
    -------
    The filtered dataframe.
    """

    for col in filters:
        if filters[col]:
            df = df.loc[df[col].isin(filters[col])]
    return df

