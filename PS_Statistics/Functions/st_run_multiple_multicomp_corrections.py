import pingouin as pg


def run_multiple_multicomp_corrections(df, input_col="p-unc", output_col="p-corr", multicomp_padjust_method="holm",
                                       alpha=0.05,):
    rejects, pvals_corr = pg.multicomp(pvals=df[input_col], alpha=alpha, method=multicomp_padjust_method)
    df["multicomp_padjust_method"] = multicomp_padjust_method
    df["alpha"] = alpha
    df[output_col] = pvals_corr
    df["reject_H0"] = rejects
    return df
