from collections import defaultdict
import os
import pandas as pd


def get_best_results(prauc, rce, df_name="", eval_col = "evaluation", target_col = "target", print_progress = True,
                     important_columns=None, export_path=None):

    df_flattened_dict = {"PRAUC": prauc, "RCE": rce, }
    engs = prauc[target_col].unique()
    dfs = {}

    for key in df_flattened_dict:
        best_performance_dict = defaultdict(list)
        for eng in engs:
            df = df_flattened_dict[key]
            df = df.loc[df[target_col] == eng]
            idmax = df[eval_col].idxmax()
            full_df_name = df_name + key

            if print_progress:
                print(full_df_name, key, idmax, )

            for col in df.loc[idmax].index:
                if col == eval_col:
                    best_performance_dict[key].append(df.loc[idmax][col])
                else:
                    best_performance_dict[col].append(df.loc[idmax][col])

        dfs[key] = pd.DataFrame(best_performance_dict)
        display_df = dfs[key] if important_columns is None else dfs[key][important_columns+[key]]

        df_plot = display_df.plot.bar(x=target_col, y=key, rot=0).get_figure()

        if print_progress:
            display(display_df)

        if export_path is not None:
            csv_filename = os.path.join(export_path, "csv", "bestResults"+full_df_name+".csv")
            tex_filename = csv_filename.replace("csv", "tex")
            plt_filename = csv_filename.replace("csv", "png")
            display_df.to_csv(csv_filename, index=False)
            prec = 4 if key == "PRAUC" else 1
            display_df.style.format(precision=prec, escape="latex").hide(axis="index").to_latex(tex_filename, hrules=True,
                                                                                 caption=f"Beset {key} results {df_name}",
                                                                                 environment="longtable",
                                                                                 label=f"tab{full_df_name}",
                                                                                 position="ht")
            df_plot.savefig(plt_filename)

    return dfs
