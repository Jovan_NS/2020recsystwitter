import os
import pandas as pd


def nested_dic_to_pd_with_condition(features_count, overview_name=None, overview_folder=None, con1=None, con2=None):
    # Create lists to store the multi-index values
    ng_values = []
    tn_values = []
    selected_f_values = []
    counts_values = []

    # Iterate over the nested dictionary to extract values and construct the multi-index
    for ng, inner_dict in features_count.items():
        for tn, selected_f_dict in inner_dict.items():
            if (con1 is not None) and (con2 is not None) and (tn != con1) and (tn != con2):
                continue

            if (con1 is not None) and (tn != con1):
                continue

            for selected_f, counts in selected_f_dict.items():
                ng_values.append(ng)
                tn_values.append(tn)
                selected_f_values.append(selected_f)
                counts_values.append(counts)

    # Create the multi-index
    multi_index = pd.MultiIndex.from_tuples(list(zip(ng_values, tn_values, selected_f_values)),
                                            names=['target', 'features threshold', 'selected feature'])

    # Create the DataFrame
    df = pd.DataFrame(counts_values, index=multi_index, columns=['count'])

    # Export the results
    if (overview_name is not None) and (overview_folder is not None):
        csv_path = os.path.join(overview_folder, "csv", overview_name + ".csv")
        tex_path = csv_path.replace("csv", "tex")
        df.to_csv(csv_path)
        if (con1 is not None) and (con2 is None):
            df_for_latex = df.droplevel('features threshold', axis=0)
        else:
            df_for_latex = df
        df_for_latex.style.format(escape="latex").format_index(escape="latex").to_latex(tex_path,
                                                 hrules=True,
                                                 caption=overview_name,
                                                 environment="longtable",
                                                 label="tab" + overview_name,
                                                 position="ht")
    return df