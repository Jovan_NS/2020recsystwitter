import pandas as pd
import os
from Functions.pp_remove_duplicates_and_inconsistencies import remove_duplicates_and_inconsistencies
from Functions.pp_flatten_deflatten import flatten_deflatten_prauc_rce_as_needed

def split_sample_column_to_factors(col, datasets=["val+test", "train", "val", "test"],
                                   sampling_techniques=["random", "inter_EWU+EU", "EWU", "EU", "tweet"],
                                   sampling_percentages=["1pct", "2pct", "5pct", "10pct"]):
    for ds in datasets:
        if ds in col:
            if col == ds:
                return ds, "full", "full"
            for st in sampling_techniques:
                if st in col:
                    for sp in sampling_percentages:
                        if sp in col:
                            return ds, st, sp


def set_to_numeric(df, columns_to_be_set_to_numberic):
    # https://stackoverflow.com/questions/15891038/change-column-type-in-pandas
    for col in columns_to_be_set_to_numberic:
        if col in df.columns:
            df[col] = pd.to_numeric(df[col])

    return df


def import_merge_and_validate_final_evals(data_to_be_cosidered, final_evals_folder, final_prauc_filename,
                                          final_rce_filename, dev_prefix, all_engs, eval_col, flatten,
                                          sorting_order=None, keep="best", split_dataset_cols=True,
                                          print_progress=False):
    if data_to_be_cosidered is None:
        data_to_be_cosidered = "dev-"

    if (data_to_be_cosidered == "dev-") or (data_to_be_cosidered == "combined-"):
        dev_final_prauc = pd.read_csv(os.path.join(final_evals_folder, dev_prefix + final_prauc_filename))
        dev_final_rce = pd.read_csv(os.path.join(final_evals_folder, dev_prefix + final_rce_filename))
        dev_final_prauc, dev_final_rce = flatten_deflatten_prauc_rce_as_needed(prauc_evals=dev_final_prauc,
                                                                               rce_evals=dev_final_rce,
                                                                               flatten=True,
                                                                               print_progress=print_progress)

    if (data_to_be_cosidered == "") or (data_to_be_cosidered == "combined-"):
        cluster_final_prauc = pd.read_csv(os.path.join(final_evals_folder, final_prauc_filename))
        cluster_final_rce = pd.read_csv(os.path.join(final_evals_folder, final_rce_filename))
        cluster_final_prauc, cluster_final_rce = flatten_deflatten_prauc_rce_as_needed(prauc_evals=cluster_final_prauc,
                                                                                       rce_evals=cluster_final_rce,
                                                                                       flatten=True,
                                                                                       print_progress=print_progress)

    if data_to_be_cosidered == "combined-":
        final_prauc = pd.concat([dev_final_prauc, cluster_final_prauc], axis="index")
        final_rce = pd.concat([dev_final_rce, cluster_final_rce], axis="index")
    elif data_to_be_cosidered == "dev-":
        final_prauc = dev_final_prauc
        final_rce = dev_final_rce
    elif data_to_be_cosidered == "":
        final_prauc = cluster_final_prauc
        final_rce = cluster_final_rce
    else:
        raise ValueError('<data_to_be_cosidered> must be in one of: "dev-", "combined-", "", or None. Use "dev"- to '
                         'only consider locally produced data, "" to only consider cluster data or "combined-" to '
                         'consider both')

    if keep == "best":
        # best requires values to be set to numeric
        final_prauc = set_to_numeric(df=final_prauc, columns_to_be_set_to_numberic=all_engs + [eval_col])
        final_rce = set_to_numeric(df=final_rce, columns_to_be_set_to_numberic=all_engs + [eval_col])
    final_prauc, final_rce = remove_duplicates_and_inconsistencies(prauc=final_prauc,
                                                                   rce=final_rce,
                                                                   sorting_order=sorting_order,
                                                                   keep=keep,
                                                                   print_progress=print_progress)
    final_prauc, final_rce = flatten_deflatten_prauc_rce_as_needed(prauc_evals=final_prauc,
                                                                   rce_evals=final_rce,
                                                                   flatten=flatten,
                                                                   print_progress=print_progress)
    final_prauc = set_to_numeric(df=final_prauc, columns_to_be_set_to_numberic=all_engs + [eval_col])
    final_rce = set_to_numeric(df=final_rce, columns_to_be_set_to_numberic=all_engs + [eval_col])

    if split_dataset_cols:
        for dataset_col, dataset_prefix in zip(["trained_on", "evaluated_on"], ["to", "eo"],):
            column_order = final_prauc.columns.copy()
            new_columns = [dataset_prefix+"_dataset", dataset_prefix+"_technique", dataset_prefix+"_percent"]
            new_order = []
            for col in column_order:
                new_order.append(col)
                if col == dataset_col:
                    new_order += new_columns
            # https://prnt.sc/lkpYuT-GRits
            final_prauc = pd.concat([final_prauc[dataset_col].apply(lambda col: pd.Series(split_sample_column_to_factors(col), index=new_columns)), final_prauc], axis=1)
            final_prauc = final_prauc[new_order]
            final_rce = pd.concat([final_rce[dataset_col].apply(lambda col: pd.Series(split_sample_column_to_factors(col), index=new_columns)), final_rce], axis=1)
            final_rce = final_rce[new_order]

    return final_prauc, final_rce
