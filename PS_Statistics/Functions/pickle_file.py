import os, pickle


def pickle_file(var, name, path=None, print_confirmation=False, dev=False):
    """
    Dupms the <var> to pickled named <name> at <path>.

    Parameters
    ----------
    var: the variable to be dumped
    name: the filename; if it does not contain ".", ".pkl" will be added at the end
    path: the path to save the variable
    print_confirmation: whether to print the confirmation message
    dev: whether to add "dev_" to the filename

    Returns
    -------
    None

    """
    name = "dev-" + name if dev else name
    full_name = name if "." in name else (name + ".pkl")
    if (path is None) or (path == ""):
        full_path = full_name
    else:
        full_path = os.path.join(path, full_name)
        print(full_path)

    with open(full_path, "wb") as fp:  # Pickling
        pickle.dump(var, fp)
        if print_confirmation:
            print(f"Pickled {name} at {full_path}.")
