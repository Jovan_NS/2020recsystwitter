from Functions.st_print_basic_eval_all_stats import print_basic_eval_all_stats
import pandas as pd
import os

def custom_prauc_rce_display(final_prauc=None, final_rce=None, df_name=None, data_to_be_cosidered="combined-",
                             new_cols_prefixes=["to", "eo",], new_cols_suffixes=["_dataset","_technique", "_percent",],
                             flatten=False, export_path=None, print_progress=True):
    """
    Displays statistics and samples for dataframes <final_prauc> and final_rce if <print_progress> is True for
    <data_to_be_considered>.
    """

    if df_name is None:
        df_name = ""

    with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.width', None):
        if final_prauc is not None:
            print_basic_eval_all_stats(df=final_prauc, df_name=df_name+"_PRAUC", data_to_be_cosidered=data_to_be_cosidered,
                                           new_cols_prefixes=new_cols_prefixes, new_cols_suffixes=new_cols_suffixes,
                                           flatten=flatten, export_path=export_path, print_progress=print_progress)
            if print_progress:
                display(final_prauc.head())
                print("\n")
        if final_rce is not None:
            print_basic_eval_all_stats(df=final_rce, df_name=df_name+"_RCE", data_to_be_cosidered=data_to_be_cosidered,
                                           new_cols_prefixes=new_cols_prefixes, new_cols_suffixes=new_cols_suffixes,
                                           flatten=flatten, export_path=export_path, print_progress=print_progress)
            if print_progress:
                display(final_rce.head())
