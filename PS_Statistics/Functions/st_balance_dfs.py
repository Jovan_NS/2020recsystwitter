from Functions.dfs_intersection import dfs_intersection


def balance_dfs(df, subject="algorithm", within=["trained_on", "sample", "feature_selection"], print_results=True):
    """
    Balances a df given a subject column and list of within columns,  cf. https://stackoverflow.com/a/71177957.
    For instance,  say df has four columns: A=[tree, tree, tree, lr, lr], B=[1,2,3,1,2], C=[a,b,c,a,d], and D=[0.1, 0.2,
    0.3, 0.01, 0.05]. If subject_col=A, within=[B,C], then the resulting df would be A=[tree, lr], B=[1,1], C=[a,a] and
    D=[0.1, 0.05] because just for those two rows the both values in A (tree and lr) have corresponding matches in the
    within columns B and C (1 and a, respectively).

    This function is equivavalent to get_common_factor_combinations for just a single factor (called subject here),
    except that it also resets the index and changes the ordering of columns.

    Parameters
    ----------
    df: the dataframe to balance
    subject: the column to balance over
    within: the within column
    print_results: whether to print how many instances remain after balancing

    Returns
    -------
    the filtered df <result>
    """

    #  unique_combinations_set = {sc: df.loc[df[subject] == sc][within].drop_duplicates() for sc in df[subject].unique()}
    common_combos = None
    for sc in df[subject].unique():
        combos = df.loc[df[subject] == sc][within].drop_duplicates()
        common_combos = combos if common_combos is None else dfs_intersection(common_combos, combos, keys=None)

    result = dfs_intersection(common_combos, df, keys=None)
    if print_results:
        values_size = result.groupby(subject).size()
        if len(values_size) > 0:
            print("\t", subject, "values:", list(values_size.index), "len of values:", len(values_size.index),
              "min count:", min(values_size), "max count:", max(values_size))
        else:
            print(f"\t Subject {subject} with {within} as within, has no intersections.")

    return result
