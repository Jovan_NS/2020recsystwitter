from Functions.st_balance_dfs import balance_dfs

import pandas as pd
import pingouin as pg


def homoscedasticity_tests(evals_dict, balance_dfs_first=False, eval_cols=["evaluation", ], target_col="target", print_progress=True):
    results_dict = {}

    with pd.option_context('display.max_rows', 50, 'display.max_columns', None, 'display.width', None):
        for metric_name in evals_dict:
            if metric_name not in results_dict:
                results_dict[metric_name] = {}

            for factor in evals_dict[metric_name]:
                if factor not in results_dict[metric_name]:
                    results_dict[metric_name][factor] = {}


                if target_col is None:
                    engs = eval_cols
                elif type(evals_dict[metric_name][factor]) == dict:
                    engs = list(evals_dict[metric_name][factor].keys())  # todo: generalise
                else:
                    engs =list(evals_dict[metric_name][factor][target_col].unique())

                for eng in engs:
                    if target_col is None:
                        full_df = evals_dict[metric_name].copy()
                    elif type(evals_dict[metric_name][factor]) == dict:
                        full_df = evals_dict[metric_name][factor][eng].copy()
                    else:
                        full_df = evals_dict[metric_name][factor].loc[full_df["target"] == eng].copy()

                    this_shape = full_df.shape
                    if (this_shape[0] > 0) and (factor != "target"):
                        print(metric_name, factor, eng, this_shape, "↓")
                        if balance_dfs_first:
                            balanced_df = balance_dfs(df=full_df, subject=factor, within=[col for col in full_df.columns() if col != factor], print_results=print_progress)
                        else:
                            balanced_df = full_df

                        try:
                            result = pg.homoscedasticity(data=balanced_df, dv="evaluation", group=factor)
                            results_dict[metric_name][factor][eng] = result
                            if print_progress:
                                print(result)
                        except ValueError:
                            if print_progress:
                                print(
                                    f"Test failed because there are no evals for Homoscedasticity_{metric_name}_{factor}_{eng}.")
                    else:
                        if print_progress:
                            print(metric_name, factor, eng, this_shape, "skipt.")
                if print_progress:
                    print("\n________\n")

    return results_dict