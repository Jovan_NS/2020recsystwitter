import os
import numpy as np
import pandas as pd


def read_all_evaluations(classfier_names, evals_folder, final_evals_folder,
                         engs=("like", "reply", "retweet", "quote", "react"), target_col="target", print_progress=True,
                         dev=True):
    """
    Reads all eval datasets, both preliminary and final.
    """

    all_evals = {}
    flattened_statuses = {}
    for algorithm in (classfier_names + [final_evals_folder]):
        if algorithm == final_evals_folder:
            individual_evals_path = final_evals_folder
        else:
            individual_evals_path = os.path.join(evals_folder, algorithm, "csv")
        if not os.path.exists(individual_evals_path):
            if print_progress:
                print(individual_evals_path, "does not exist!")
            continue

        # read already existing preliminary evaluations
        eval_files = [f for f in os.listdir(individual_evals_path) if
                      os.path.isfile(os.path.join(individual_evals_path, f))]
        # keep only those that do (not) begin with "dev-", if <dev>  is (not) True
        eval_files = [os.path.join(individual_evals_path, f) for f in eval_files if ((f[:4] == "dev-") == dev)]
        for file in eval_files:
            # https://prnt.sc/8vKFEpREa6yD
            filename = file.split(os.path.sep)[-1].split(".")[0]
            df = pd.read_csv(file)
            all_evals[filename] = df
            if target_col in df.columns:
                flattened_statuses[filename] = True
            elif set(engs).issubset(set(df.columns)):
                flattened_statuses[filename] = False
            else:
                # https://prnt.sc/X7XL0rymhwEO
                for eng in engs:
                    if eng not in df.columns:
                        df[eng] = np.nan

                flattened_statuses[filename] = False
                df.to_csv(file, index=False)

    return all_evals, flattened_statuses


def get_flattened_statuses(evals, engs=["like", "reply", "retweet", "quote", "react", ], target_col="target",
                           print_progress=False):
    """
    Checks whether each of the dataframe in the dict <evals> is flattened, and returns a dict with bools with
    corresponding keys. For instance, in this printscreen the top dataframe is flattened and the bottom is
    deflattened: https://prnt.sc/xgp9n3OBmITI
    """

    flattened_statuses = {}
    for df_name in evals:
        if target_col in evals[df_name].columns:
            flattened_statuses[df_name] = True
        elif set(engs).issubset(set(evals[df_name].columns)):
            flattened_statuses[df_name] = False
        else:
            # https://prnt.sc/X7XL0rymhwEO
            for eng in engs:
                if eng not in evals[df_name].columns:
                    if print_progress:
                        print(f"\tAdded a missing NaN column for {eng} of {df_name}!")
                    evals[df_name][eng] = np.nan

            flattened_statuses[df_name] = False

    return evals, flattened_statuses


def flatten_evals(evals, flattened_statuses=None, engs=["like", "reply", "retweet", "quote", "react", ],
                  target_col="target", eval_col="evaluation", print_progress=True):
    """
    Flattens (projects the [<engs>] colums into column <target_col> and <eval_col>) the deflattened evals datasets.
    For instance, in this printscreen the top dataframe is flattened and the bottom is deflattened:
    https://prnt.sc/xgp9n3OBmITI
    Similar to: https://pandas.pydata.org/docs/reference/api/pandas.melt.html
    """

    if flattened_statuses is None:
        evals, flattened_statuses = get_flattened_statuses(evals, engs=engs, target_col=target_col,
                                                           print_progress=print_progress)
    assert list(evals.keys()) == list(flattened_statuses.keys())
    flattened_evals = {}
    for df_name in flattened_statuses:
        if not flattened_statuses[df_name]:
            df = evals[df_name]
            relevant_cols = [col for col in df.columns if col not in engs]
            # https://prnt.sc/32dKvFeaBk6Q
            dict_rel = df[relevant_cols].to_dict(orient="index")
            # https://prnt.sc/BCbo-D2-HM5H
            dict_engs = df[engs].to_dict(orient="index")
            new_dict = {}
            for index in dict_rel:
                ia = 0
                for eng in engs:
                    ni = index * len(engs) + ia
                    # copy() makes all the difference, cf. index 0 here: https://prnt.sc/6rA8Mumg8VQm and here:
                    # https://prnt.sc/WcyE3_wnCOGA
                    new_dict[ni] = dict_rel[index].copy()
                    new_dict[ni][target_col] = eng
                    new_dict[ni][eval_col] = dict_engs[index][eng]
                    ia += 1
            flattened_evals[df_name] = pd.DataFrame(new_dict).T

    if print_progress:
        print("Flattened", flattened_evals.keys())
    return flattened_evals


def deflatten_evals(evals, flattened_statuses=None, engs=["like", "reply", "retweet", "quote", "react", ],
                    target_col="target", eval_col="evaluation", print_progress=True):
    """
    Deflattens (projects the column <target_col> and <eval_col> into [<engs>] colums) the flattened evals datasets.
    For instance, in this printscreen the top dataframe is flattened and the bottom is deflattened:
    https://prnt.sc/xgp9n3OBmITI
    Similar to: https://pandas.pydata.org/docs/reference/api/pandas.pivot_table.html#pandas.pivot_table
    """

    # https://prnt.sc/aMB6F_busMMO
    if flattened_statuses is None:
        # https://prnt.sc/brF4d-nsT_9L
        evals, flattened_statuses = get_flattened_statuses(evals, engs=engs, target_col=target_col,
                                                           print_progress=print_progress)
    # https://prnt.sc/2cgq6SMfUTv6
    assert list(evals.keys()) == list(flattened_statuses.keys())
    deflattened_evals = {}
    for df_name in flattened_statuses:
        if flattened_statuses[df_name]:
            df = evals[df_name]
            # To remove gaps in indexing that came to be from e.g. inconsistencies removal, cf. index 20 here:
            # https: // prnt.sc / KGddYTqDwT9z and here: https://prnt.sc/dcIzLLrpdlL_
            df.reset_index(drop=True, inplace=True)
            relevant_cols = [col for col in df.columns if col not in [target_col, eval_col]]
            # https://prnt.sc/Zhrc-HMTxf7d
            dict_rel = df[relevant_cols].to_dict(orient="index")
            # https://prnt.sc/VTsdPthG9Mc1 (old: https://prnt.sc/IhL9-ZH-tyu2)
            dict_engs = df.to_dict(orient="index")

            index_new_dict = 0
            index_dict_rel = 0
            new_dict = {}
            # This loops until all rows have been reached: https://prnt.sc/8OsVHtvOt7dI
            while index_dict_rel in dict_rel:
                new_dict[index_new_dict] = dict_rel[index_dict_rel].copy()
                for eng in engs:
                    new_dict[index_new_dict][eng] = np.nan

                index_dict_engs = index_dict_rel
                while (index_dict_engs in dict_engs) and all(
                        [dict_rel[index_dict_rel][r_col] == dict_engs[index_dict_engs][r_col] for r_col in
                         relevant_cols]):
                    next_eng = dict_engs[index_dict_engs][target_col]
                    new_dict[index_new_dict][next_eng] = dict_engs[index_dict_engs][eval_col]
                    index_dict_engs += 1

                index_dict_rel = index_dict_engs
                index_new_dict += 1

                # https://prnt.sc/kbgKbesnmNUZ
                # print(f"index_new_dict={index_new_dict}, index_dict_rel={index_dict_rel}, index_dict_engs={index_dict_engs}, len(new_dict.keys())={len(new_dict.keys())}, len(dict_engs.keys())={len(dict_engs.keys())}")

            deflattened_evals[df_name] = pd.DataFrame(new_dict).T

    if print_progress:
        print("Deflattened", deflattened_evals.keys())
    # https://prnt.sc/wkEj_xOpbSAm
    return deflattened_evals


def flatten_deflatten_prauc_rce_as_needed(prauc_evals, rce_evals=None, flatten=True,
                                          engs=["like", "reply", "retweet", "quote", "react", ], target_col="target",
                                          eval_col="evaluation", print_progress=True):
    """
    Flattens or deflattens the prauc_evals and rce_evals as neccessary. For instance, in this printscreen the top
    dataframe is flattened and the bottom is deflattened: https://prnt.sc/xgp9n3OBmITI. In other words, flattened
    dataframes have columns <target_col> (whose values are in <engs>) and a column <eval_col> with evaluations, i.e. one
    evaluation per row. In contrast, deflattened evals have all <engs> in the same row, and thus do not have columns
    <target_col> and <eval_col>

    Parameters
    ----------
    prauc_evals: the pre-existing PRAUC evals dataframe (or any evals if rce_evals is None)
    rce_evals: the pre-existiong RCE evals dataframe (can be None)
    flatten: whether the desired evals dataframes should be flattened (=True) or deflattened (=False)
    engs: tweet engagement goals list
    target_col: column name in the flattened form describing whose values are individual tweet engagement goals
    eval_col: column name in the flattened form describing with an individual evaluation value
    print_progress: whether to print the transformation progress

    Returns
    -------
    prauc_evals, rce_evals, potentially transformed as necessary.
    """
    if rce_evals is None:
        evals = {"prauc_evals": prauc_evals}
    else:
        evals = {"prauc_evals": prauc_evals, "rce_evals": rce_evals}
    evals, flatten_statuses = get_flattened_statuses(evals, engs=engs, target_col=target_col,
                                                     print_progress=print_progress)
    # We use dict values, as np.nan columns are added if not all engs columns existed in deflattened form
    prauc_evals = evals["prauc_evals"]
    rce_evals = evals["rce_evals"] if "rce_evals" in evals else None
    all_okay = all([flatten_statuses[df] == flatten for df in flatten_statuses])
    if all_okay:
        if print_progress:
            print("Evals did not have to be (de)flattened.")

        return prauc_evals, rce_evals
    else:
        if print_progress:
            print(f"(De)flattenig needed: for flatten={flatten}, flatten_statuses={flatten_statuses}")

    if flatten:
        transformed_evals = flatten_evals(evals, flattened_statuses=flatten_statuses, engs=engs, target_col=target_col,
                                          eval_col=eval_col, print_progress=print_progress)
    else:
        transformed_evals = deflatten_evals(evals, flattened_statuses=flatten_statuses, engs=engs,
                                            target_col=target_col, eval_col=eval_col, print_progress=print_progress)

    if "prauc_evals" in transformed_evals:
        prauc_evals = transformed_evals["prauc_evals"]

    if "rce_evals" in transformed_evals:
        rce_evals = transformed_evals["rce_evals"]

    return prauc_evals, rce_evals
