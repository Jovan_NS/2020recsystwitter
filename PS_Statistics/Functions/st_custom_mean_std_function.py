def custom_mean_std(values_list):
    print("values_list", values_list)
    strs = [s for s in values_list if type(s)==str]
    print("strs", strs)
    s = sum(values_list)
    l = len(values_list)
    mean = s / l
    sd = np.sqrt(sum([(elem-mean)^2 for elem in values_list])/l)
    return mean,sd