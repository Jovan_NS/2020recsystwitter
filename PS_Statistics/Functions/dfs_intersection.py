import pandas as pd


def dfs_intersection(df1, df2, rename_left="left_", rename_right="right_",
                     keys=["algorithm", "note", "trained_on", "sample", "feature_selection"]):
    """
    Creates an interesection of <df1> and <df2> over shared columns in <keys>.

    Parameters
    ----------
    df1: the first (left) dataframe
    df2: the second (right) dataframe
    rename_left: the prefix for all columns but those in <keys> for the first dataframe (ignored if keys is None)
    rename_right: the prefix for all columns but those in <keys> for the second dataframe (ignored if keys is None)
    keys: the columns to be used as merger keys;if None, based on all common columns in df1 and df2

    Returns
    -------
    The intersection of <df1> and <df2>

    """

    if keys is None:
        keys = list(set(df1.columns).intersection(set(df2.columns)))
    else:
        if rename_left is not None:
            df1 = df1.rename(columns={col: rename_left + col for col in df1.columns if col not in keys}, inplace=False)

        if rename_right is not None:
            df2 = df2.rename(columns={col: rename_right + col for col in df2.columns if col not in keys}, inplace=False)

    df = pd.merge(df1, df2, how='inner', on=keys)
    return df.dropna(inplace=False)
