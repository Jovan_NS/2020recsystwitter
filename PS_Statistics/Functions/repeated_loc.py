def repeated_loc(df, value_conditions: dict = None, string_conditions: dict = None, list_conditions: dict = None):
    """
    A decorator-function that allows repeated calls of loc functions.

    Parameters
    ----------
    df: the Pandas dataframe
    value_conditions: for df = df.loc[df[col] == value_conditions[col]]
    string_conditions: for df = df.loc[df[col].str.contains(string_conditions[col])]
    list_conditions: for df = df.loc[df[col].isin(list_conditions[col])]

    Returns
    -------
    Returns the filtered df
    """

    if value_conditions is not None:
        for col in value_conditions:
            # print(df.shape, col, value_conditions[col])
            df = df.loc[df[col] == value_conditions[col]]

    if string_conditions is not None:
        for col in string_conditions:
            # print(df.shape, col, string_conditions[col])
            df = df.loc[df[col].str.contains(string_conditions[col])]

    if list_conditions is not None:
        for col in list_conditions:
            # print(df.shape, col, string_conditions[col])
            df = df.loc[df[col].isin(list_conditions[col])]

    return df
