import os, pickle


def unpickle_file(name, path=None, print_confirmation=False, dev=False):
    """
    Loads the pickle file named <name> at <path>.

    Parameters
    ----------
    name: the filename; if it does not contain ".", ".pkl" will be added at the end
    path: the path to save the variable
    print_confirmation: whether to print the confirmation message
    dev: whether to add "dev_" to the filename

    Returns
    -------
    The loaded file.

    """
    name = "dev-"+name if dev else name

    full_name = name if "." in name else (name + ".pkl")
    if (path is None) or (path == ""):
        full_path = full_name
    else:
        full_path = os.path.join(path, full_name)
    
    with open(full_path, "rb") as fp:   # Unpickling
        var =  pickle.load(fp)
        if print_confirmation:
            print(f"Unpickled {name} from {full_path}.")
        
        return var
