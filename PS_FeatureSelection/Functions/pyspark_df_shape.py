import locale


def pyspark_df_shape(df, tpl = True):
    """
    Imitates the shape field of Pandas dataframes for Pyspark dataframes.
    
    df: dataframe to get the shape of.
    tpl: if true, a tuple of the form (#columns, #rows) will be returned; else the string with local-aware form of (#columns, #rows)
    """

    if tpl:
        return (df.count(), len(df.columns),)
    
    # https://stackoverflow.com/a/37443934
    locale.setlocale(locale.LC_ALL, '')
    
    return f"({df.count():,d}; {len(df.columns):,d})"
