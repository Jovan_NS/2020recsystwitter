def check_if_columns_are_missing(dfs, trn_key):
    for key in dfs:
        missing_trn_cols = []
        for trn_col in dfs[trn_key].columns:
            if trn_col not in dfs[key].columns:
                missing_trn_cols.append(trn_col)
        if len(missing_trn_cols) > 0:
            print(f"Columns from {key} missing! {missing_trn_cols} from {trn_key} not in {key}!")

        missing_cols = []
        for col in dfs[key].columns:
            if col not in dfs[trn_key].columns:
                missing_cols.append(col)
        if len(missing_cols) > 0:
            print(f"Columns from {trn_key} missing! {missing_cols} from {key} not in {trn_key}!")
