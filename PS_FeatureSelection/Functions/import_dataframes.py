import os
from py4j.protocol import Py4JJavaError
from pyspark.sql.utils import AnalysisException


def help_import_dataframes(spark, previous_files: list, backup_files: list, always_load_backup: bool, filename: str,
                           backup_filenames: list, HDFS_datafolder):
    """
    This function imports a single dataframe. If that is not possible, a backup is returned. If that too fails for
    all backups, the error is forwarded. Depending on whether the original dataframe or a backup was loaded,
    the respective filename is appended to the list in either previous_files or backup_files, respectively.

    Parameters
    ----------
    spark: spark environment
    previous_files: list of primary (non-backup) files already loaded
    backup_files: list of backup files already loaded
    always_load_backup: load backup even if the primary (non-backup) file is available
    filename: the filename of the primary (non-backup) file
    backup_filenames:  the list of filenames of the backup files
    HDFS_datafolder: the path to the folder containing files to be loaded

    Returns df, previous_files, backup_files
    -------

    """
    file_loaded = False

    if not always_load_backup:
        try:
            df = spark.read.parquet(os.path.join(HDFS_datafolder, filename))

            previous_files.append(filename)
            file_loaded = True
        except (Py4JJavaError, AnalysisException) as e:
            pass

    if (always_load_backup or not file_loaded) and ((backup_filenames is not None) and
                                                    (type(backup_filenames) == list) and
                                                    (backup_filenames != [])):
        for backup_filename in backup_filenames:
            try:
                df = spark.read.parquet(os.path.join(HDFS_datafolder, backup_filename))

                backup_files.append(backup_filename)
                file_loaded = True
                break
            except (Py4JJavaError, AnalysisException) as e:
                pass

    if not file_loaded:
        raise ValueError('None of the (backup) filenames could be found!')

    return df, previous_files, backup_files


def verify_is_iterable_or_none(var):
    """
    Checks that var is an iterable or None. If it is string or empty, it will return ()
    Parameters
    """

    if var is None:
        return ()

    if type(var) == str:
        return (var, )

    return var


def import_dataframes(spark, datasets=("train", "val", "test"),
                      sampling_techniques=("random", "EWU", "EU", "inter_EWU+EU", "tweet"),
                      sampling_days=None,
                      sampling_percentages=("1pct", "2pct", "5pct", "10pct"), featureset_export_prefix="Encoding_",
                      backup_featureset_prefixes = None, HDFS_datafolder="Data", recreate_even_if_already_exist=False,
                      dev=False):
    """
    This function imports multiple dataframes (by calling the helper function help_import_dataframes). The primary
    and back-up names are created by combining dataset names, sampling techniques, sampling days, and sampling
    percentages, as well as export prefix. Each element can be empty/

    Parameters
    ----------
    spark: spark environment
    datasets: a subset of ("train", "val", "test")
    sampling_techniques: a subset of  ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
    sampling_days: a subset of (37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50)
    sampling_percentages: a subset of ("1pct", "2pct", "5pct", "10pct")
    featureset_export_prefix: the primary dataset prefix, e.g. "typed_", "Encoded_", "fe01", or ""
    backup_featureset_prefixes: the backup dataset prefix, e.g. "typed_", "Encoded_", "fe01", or ""
    HDFS_datafolder: the path to the folder containing files to be loaded
    recreate_even_if_already_exist: load the backup even if the dataset with the primary prefix is available
    dev: only take the first sample

    Returns dfs, files_to_be_changed
    -------

    """

    dfs = {}
    
    backup_featureset_prefixes = verify_is_iterable_or_none(backup_featureset_prefixes)
    sampling_percentages = verify_is_iterable_or_none(sampling_percentages)
    sampling_techniques = verify_is_iterable_or_none(sampling_techniques)

    process_day = lambda x: "day_" + x + "_" if x != "" else ""
    # process sampling days
    # assumption: sampling_days is None or dict of list-like with <datasets> as keys
    if sampling_days is None:
        sampling_days = {}
        for ds in datasets:
            sampling_days[ds] = ("",)
    else:
        for ds in datasets:
            if ds not in sampling_days:
                raise ValueError("sampling_days must be a None or a dict of list-like containing days")
            full_days_name = (process_day(x) for x in sampling_days[ds])
            sampling_days[ds] = tuple(full_days_name)  # convert the generator into a tuple

    if dev:
        sampling_techniques = sampling_techniques[:1]  # only take the first sample
        sampling_percentages = sampling_percentages[:1]
        for ds in datasets:
            sampling_days[ds] = list(sampling_days[ds][:3])
            if "" not in sampling_days[ds]:
                sampling_days[ds].append("")  # assure that non-dayed case is there as well
            sampling_days[ds] = tuple(sampling_days[ds])

    previous_files = []
    backup_files = []

    # read sampled datasets
    for sampling_percentage in sampling_percentages:
        for sampling_technique in sampling_techniques:
            for key in datasets:
                for day in sampling_days[key]:
                    filename = featureset_export_prefix + day + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                    backup_filenames = []
                    for backup_featureset_prefix in backup_featureset_prefixes:
                        if backup_featureset_prefix == "typed_":
                            backup_featureset_prefix = ""  # typed_ only for non-sampled datasets, cf. notebook DataPrep01
                            
                        backup_filename = backup_featureset_prefix + day + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                        backup_filenames.append(backup_filename)

                    sample_key = day + key + "_" + sampling_technique + "_sample_" + sampling_percentage
                    dfs[sample_key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                                           backup_files,
                                                                                           recreate_even_if_already_exist,
                                                                                           filename, backup_filenames,
                                                                                           HDFS_datafolder)

    # read non-sampled dataset if not in DEV
    if not dev:
        for key in datasets:
            filename = featureset_export_prefix + key + ".parquet"
            backup_filenames = []
            for backup_featureset_prefix in backup_featureset_prefixes:
                backup_filename = backup_featureset_prefix + key + ".parquet"
                backup_filenames.append(backup_filename)
                
            dfs[key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                            backup_files,
                                                                            recreate_even_if_already_exist, filename,
                                                                            backup_filenames, HDFS_datafolder)

    print(f"Done with reading dataframes! Read {len(previous_files)} previous versions and {len(backup_files)} backups."
          f" \nPrevious versions: {previous_files}; \nbackups: {backup_files}.")
    files_to_be_changed = set(
        [dfn.replace(backup_featureset_prefix, "").replace(".parquet", "") for dfn in backup_files])
    return dfs, files_to_be_changed