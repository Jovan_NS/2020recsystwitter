def combine_dataframes(dfs):
    """
    Returns a single combined dataframe from the set of dataframes in dfs.

    """
    df = None
    for next_df in dfs:
        df = df.union(next_df) if (df is not None) else next_df

    return df
