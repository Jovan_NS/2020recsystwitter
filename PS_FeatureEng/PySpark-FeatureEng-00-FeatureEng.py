#!/usr/bin/env python
# coding: utf-8

# # Encoding
# 
# This notebook takes the resulting datasets from ```PySpark-DataSampeling.ipynb``` and performs basic data preparation and feature engineering. The resulting datasets have "FE" in their names.

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
from pyspark.sql.functions import udf
import pyspark.sql.types as t
from operator import add
from pyspark.sql.utils import AnalysisException


# In[2]:


# Other imports
import os
import pandas as pd


# In[3]:


# Initialise Spark #session 
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_00")     .master('yarn')     .config("spark.executor.memory", "32G")     .config("spark.driver.memory", "5G")     .config("spark.driver.maxResultSize", "5G")     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", 8)     .config("spark.sql.broadcastTimeout", 900)     .getOrCreate()
    #.jar("glint-fmpair/target/scala-2.11/glint-fmpair-assembly-1.0.jar", "pyspark-shell") \

# from glintfmpair import WeightHotEncoderEstimator, WeightHotEncoderModel, GlintFMPair, GlintFMPairModel, PopRank, SAGH, KNN, TfIdfKNN    


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


RECREATE_EVEN_IF_ALREADY_EXIST = False

HDFS_DATAFOLDER = "Data"
SAMPLING_TECHNIQUES = ["random", "EWU", "EU", "inter_EWU+EU", "tweet"]
SAMPLING_PERCENTAGES = ["1pct", "2pct", "5pct", "10pct"]

TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"


# In[6]:


keys = ["train", "val", "test"]
dfs = {}
dfs_samples = {}
all_dfs = {}

for key in keys:
    previous_files = []
    backup_files = []
    # read non-sampled dataset
    try:
        dfs[key] =  spark.read.parquet(os.path.join(HDFS_DATAFOLDER, "FE_"+key+".parquet"))
        previous_files.append("FE_"+key+".parquet")
    except AnalysisException:
        dfs[key] =  spark.read.parquet(os.path.join(HDFS_DATAFOLDER, "typed_"+ key +".parquet"))
        backup_files.append('typed_'+key+'.parquet')
    dfs_samples[key] = {}
    all_dfs[key] = dfs[key]
    # read sampled datasets
    for sampling_technique in SAMPLING_TECHNIQUES:
        dfs_samples[key][sampling_technique] = {}
        for sampling_percentage in SAMPLING_PERCENTAGES:
            filename = "FE_"+key+"_"+sampling_technique+"_sample_"+sampling_percentage+".parquet"
            backup_filename = key+"_"+sampling_technique+"_sample_"+sampling_percentage+".parquet"
            try:
                dfs_samples[key][sampling_technique][sampling_percentage] = spark.read.parquet(os.path.join(HDFS_DATAFOLDER, filename))
                previous_files.append(filename)
            except AnalysisException:
                dfs_samples[key][sampling_technique][sampling_percentage] = spark.read.parquet(os.path.join(HDFS_DATAFOLDER, backup_filename))
                backup_files.append(backup_filename)
            all_dfs[key+"_"+sampling_technique+"_sample_"+sampling_percentage] = dfs_samples[key][sampling_technique][sampling_percentage]
            
    print(f"Done with reading {key}. Previous versions: {previous_files}; backups: {backup_files}.")


# In[7]:


all_dfs["train"].columns


# In[8]:


with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    display(all_dfs["train"].limit(10).toPandas())


# ## Feature Engineering
# 
# ### Reaction Flags

# In[9]:


'like' in all_dfs["train"].columns


# - like: a feature whose value is set to $0$ if like\_timestamp is None (an empty filed), and $1$ if it is a timestamp.
# - reply: analogues of like for reply\_timestamp.
# - retweet: analogues of like for retweet\_timestamp.
# - quote: analogues of like for retweet\_with\_comment\_timestamp.
# - react: true ($=1$) if any of the four engagements (like, reply, retweet, quote) occurred (is equal to $1$).

# In[10]:


reactions = ["like", "reply", "retweet", "quote"]
reaction_timestamps = [reaction + "_timestamp" for reaction in reactions]
reaction_timestamps = [reaction.replace("quote", "retweet_with_comment") for reaction in reaction_timestamps]


# In[11]:


if_not_null_then_1_udf = udf(lambda x: 0 if x is None else 1, t.ByteType())

for key in all_dfs:
    for reaction, ts in zip(reactions, reaction_timestamps):
        if (RECREATE_EVEN_IF_ALREADY_EXIST) or (reaction not in all_dfs[key].columns):
            all_dfs[key] = all_dfs[key].withColumn(reaction, if_not_null_then_1_udf(all_dfs[key][ts]))


# In[12]:


def if_any_reaction_then_1(row):
    flag = 0
    for reaction in ["reply_timestamp" ,"retweet_timestamp" ,"retweet_with_comment_timestamp", "like_timestamp"]:
        if row[reaction] is not None:
            return 1
        
    return 0

udf_if_any_reaction_then_1 = udf(if_any_reaction_then_1, t.ByteType())

for key in all_dfs:
    if (RECREATE_EVEN_IF_ALREADY_EXIST) or ("react" not in all_dfs[key].columns):
        all_dfs[key] = all_dfs[key].withColumn("react", udf_if_any_reaction_then_1(f.struct([all_dfs[key][x] for x in all_dfs[key].columns])))
        print(f"Done with {key}.")


# In[13]:


all_dfs["train"]["like_timestamp", "like", "reply", "react"].show(40)


# ### Tweet Tokenisation
# 
# We now want to detokenise tweet text.

# In[14]:


from transformers import BertTokenizer, BertModel

tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
model = BertModel.from_pretrained('bert-base-multilingual-cased')


# In[15]:


def separate_and_decrypt(row):    
    if row is None:
        return ""
    
    tweet_tokens = row.split("\t")
    return "".join(tokenizer.convert_tokens_to_string(tokenizer.convert_ids_to_tokens(tweet_tokens)))


udf_separate_and_decrypt = f.udf(separate_and_decrypt, t.StringType())

for key in all_dfs:
    if (RECREATE_EVEN_IF_ALREADY_EXIST) or ("text" not in all_dfs[key].columns):
        all_dfs[key] = all_dfs[key].withColumn("text", udf_separate_and_decrypt(all_dfs[key]["text_tokens"]))
        print(f"Done with {key}.")


# In[16]:


all_dfs["train"]["text_tokens", "text", "present_links", "hashtags"].show(40)


# ## Rename Misspelled Columns
# 
# The original data preparation snipped contained a spelling error as some columns contained a word "enaged" instead of "engaged".

# In[17]:


def replace_name_if_wrong(name):
    if name[:4] == "enag":
        return "engag" + name[4:]
    return name


for key in all_dfs:
    correct_column_names = {name:replace_name_if_wrong(name) for name in all_dfs[key].columns}
    for dict_key in correct_column_names:
        if dict_key != correct_column_names[dict_key]:
            all_dfs[key] = all_dfs[key].withColumnRenamed(dict_key, correct_column_names[dict_key])


# ## Save Results
# 
# We save all columns of the new dataframes and use these exports as the starting point for the remaining data engineering notebooks.

# In[18]:


for key in all_dfs:
    # https://forums.databricks.com/questions/21830/spark-how-to-simultaneously-read-from-and-write-to.html
    all_dfs[key].cache() # cache to avoid FileNotFoundException 
    print(key, all_dfs[key].count()) # light action
    all_dfs[key].write.parquet(os.path.join(HDFS_DATAFOLDER, "FE_"+key+".parquet"), mode = "overwrite")
    print(f"'FE_{key}.parquet' done and saved.")

