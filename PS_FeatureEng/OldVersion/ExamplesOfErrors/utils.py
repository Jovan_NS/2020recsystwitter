from pyspark.sql.functions import lit
from pyspark.sql.functions import split
from pyspark.sql.types import ArrayType, BooleanType, IntegerType, StructField, StructType, StringType


TRAIN_DATA_PATH = "/home/jovyan/Ex3/data/training_data_50mb.tsv"
VALIDATION_DATA_PATH = "/home/jovyan/Ex3/data/eval_data_50mb.tsv"
COLNAMES = ["text_tokens", "hashtags", "tweet_id", "present_media", "present_links", "present_domains",
			"tweet_type","language", "tweet_timestamp", "engaged_with_user_id", "engaged_with_user_follower_count",
			"engaged_with_user_following_count", "engaged_with_user_is_verified", "engaged_with_user_account_creation",
			"enaging_user_id", "enaging_user_follower_count", "enaging_user_following_count",
			"enaging_user_is_verified", "enaging_user_account_creation", "engagee_follows_engager",
			"reply_timestamp", "retweet_timestamp", "retweet_with_comment_timestamp", "like_timestamp"]
SCHEMA = StructType([
	StructField("text_tokens", ArrayType(StringType())),
	StructField("hashtags", ArrayType(StringType())),
	StructField("tweet_id", StringType()),
	StructField("present_media", ArrayType(StringType())),
	StructField("present_links", ArrayType(StringType())),
	StructField("present_domains", ArrayType(StringType())),
	StructField("tweet_type", StringType()),
	StructField("language", StringType()),
	StructField("tweet_timestamp", IntegerType()),
	StructField("engaged_with_user_id", StringType()),
	StructField("engaged_with_user_follower_count", IntegerType()),
	StructField("engaged_with_user_following_count", IntegerType()),
	StructField("engaged_with_user_is_verified", BooleanType()),
	StructField("engaged_with_user_account_creation", IntegerType()),
	StructField("enaging_user_id", StringType()),
	StructField("enaging_user_follower_count", IntegerType()),
	StructField("enaging_user_following_count", IntegerType()),
	StructField("enaging_user_is_verified", BooleanType()),
	StructField("enaging_user_account_creation", IntegerType()),
	StructField("engagee_follows_engager", BooleanType()),
	StructField("reply_timestamp", IntegerType()),
	StructField("retweet_timestamp", IntegerType()),
	StructField("retweet_with_comment_timestamp", IntegerType()),
	StructField("like_timestamp", IntegerType()),
])


def load_data(path, spark_session):
	"""Load the data (train or validation) and cast to the expected data type.

		Parameters
		----------
		path : string
			HDFS path to the tsv file containing the data
		spark_session : pyspark.sql.session.SparkSession
			SparkSession object

		Returns
		-------
		df : pyspark.sql.dataframe.DataFrame
			DataFrame object including all attributes
	"""
	df = spark_session.read.csv(path, sep="\x01")

	# Replace column names: the validation dataset does not have the last 4 columns as defined in COLNAMES
	df = df.toDF(*(COLNAMES if len(df.columns) == len(COLNAMES) else COLNAMES[:-4]))

	# Convert list features parsed as string to ArrayType
	colname_lists = [COLNAMES[i] for i in [0, 1, 3, 4, 5]]
	for col in colname_lists:
		df = df.withColumn(col, split(col, "\t"))

	# Cast datatype of different columns based on the defined schema
	for struct in SCHEMA if len(df.columns) == len(SCHEMA) else SCHEMA[:-4]:
		df = df.withColumn(struct.name, df[struct.name].cast(struct.dataType))

	return df


def add_dummy_values(df, value=1):
	"""Assign dummy value to the engagement features.

		Parameters
		----------
		df : pyspark.sql.dataframe.DataFrame
			DataFrame object
		value : int
			Dummy value to be assigned to the submission attributes

		Returns
		-------
		df : pyspark.sql.dataframe.DataFrame
			DataFrame object with dummy values assigned to target attributes
	"""
	for col in COLNAMES[-4:]:
		df = df.withColumn(col, lit(value))
	return df


def generate_submission_files(df, submission_name, compression=None):
	"""Generate submission files on the HDFS in the required format.

		Parameters
		----------
		df : pyspark.sql.dataframe.DataFrame
			DataFrame object
		submission_name : string
			Name of the submission. Submission files will be saved in HDFS in 4 folders having the following format:
			/user/<username>/<submission_name>/<submission_name>_<target_attribute_name>
		compression : string
			Compression type to be passed to "write.csv"
			https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.DataFrameWriter.csv
	"""
	# Check if target columns are in the dataframe
	if not set(df.columns).issuperset(set(COLNAMES[-4:])):
		raise Exception("Target attributes cannot be found!")

	# Generate submission files in the required format: (tweet_id, user_id, prediction)
	# https://recsys-twitter.com/code/snippets
	# TODO: Check the submission format once we get access to recsys-twitter.com
	for target_col in COLNAMES[-4:]:
		df.select(*["tweet_id", "enaging_user_id", target_col]).write.csv(
			"{}/{}_{}/".format(submission_name, submission_name, target_col), header=False, mode="overwrite",
			compression=compression)


def delete_hdfs_path(spark, path):
	"""Delete a specific path on HDFS

		Parameters
		----------
		spark : pyspark.sql.session.SparkSession
			SparkSession object
		path : string
			HDFS folder or file path to be deleted
	"""
	# Create a pyspark file system handler over HDFS API
	fs = spark._jvm.org.apache.hadoop.fs.FileSystem.get(spark._jsc.hadoopConfiguration())
	fs.delete(spark._jvm.org.apache.hadoop.fs.Path(path), True)
