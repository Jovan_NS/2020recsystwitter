Done with reading dataframes! Read 84 previous versions and 0 backups. 
Previous versions: ['GraphBased_train_random_sample_1pct.parquet', 'GraphBased_val_random_sample_1pct.parquet', 'GraphBased_test_random_sample_1pct.parquet', 'GraphBased_val+test_random_sample_1pct.parquet', 'GraphBased_train_EWU_sample_1pct.parquet', 'GraphBased_val_EWU_sample_1pct.parquet', 'GraphBased_test_EWU_sample_1pct.parquet', 'GraphBased_val+test_EWU_sample_1pct.parquet', 'GraphBased_train_EU_sample_1pct.parquet', 'GraphBased_val_EU_sample_1pct.parquet', 'GraphBased_test_EU_sample_1pct.parquet', 'GraphBased_val+test_EU_sample_1pct.parquet', 'GraphBased_train_inter_EWU+EU_sample_1pct.parquet', 'GraphBased_val_inter_EWU+EU_sample_1pct.parquet', 'GraphBased_test_inter_EWU+EU_sample_1pct.parquet', 'GraphBased_val+test_inter_EWU+EU_sample_1pct.parquet', 'GraphBased_train_tweet_sample_1pct.parquet', 'GraphBased_val_tweet_sample_1pct.parquet', 'GraphBased_test_tweet_sample_1pct.parquet', 'GraphBased_val+test_tweet_sample_1pct.parquet', 'GraphBased_train_random_sample_2pct.parquet', 'GraphBased_val_random_sample_2pct.parquet', 'GraphBased_test_random_sample_2pct.parquet', 'GraphBased_val+test_random_sample_2pct.parquet', 'GraphBased_train_EWU_sample_2pct.parquet', 'GraphBased_val_EWU_sample_2pct.parquet', 'GraphBased_test_EWU_sample_2pct.parquet', 'GraphBased_val+test_EWU_sample_2pct.parquet', 'GraphBased_train_EU_sample_2pct.parquet', 'GraphBased_val_EU_sample_2pct.parquet', 'GraphBased_test_EU_sample_2pct.parquet', 'GraphBased_val+test_EU_sample_2pct.parquet', 'GraphBased_train_inter_EWU+EU_sample_2pct.parquet', 'GraphBased_val_inter_EWU+EU_sample_2pct.parquet', 'GraphBased_test_inter_EWU+EU_sample_2pct.parquet', 'GraphBased_val+test_inter_EWU+EU_sample_2pct.parquet', 'GraphBased_train_tweet_sample_2pct.parquet', 'GraphBased_val_tweet_sample_2pct.parquet', 'GraphBased_test_tweet_sample_2pct.parquet', 'GraphBased_val+test_tweet_sample_2pct.parquet', 'GraphBased_train_random_sample_5pct.parquet', 'GraphBased_val_random_sample_5pct.parquet', 'GraphBased_test_random_sample_5pct.parquet', 'GraphBased_val+test_random_sample_5pct.parquet', 'GraphBased_train_EWU_sample_5pct.parquet', 'GraphBased_val_EWU_sample_5pct.parquet', 'GraphBased_test_EWU_sample_5pct.parquet', 'GraphBased_val+test_EWU_sample_5pct.parquet', 'GraphBased_train_EU_sample_5pct.parquet', 'GraphBased_val_EU_sample_5pct.parquet', 'GraphBased_test_EU_sample_5pct.parquet', 'GraphBased_val+test_EU_sample_5pct.parquet', 'GraphBased_train_inter_EWU+EU_sample_5pct.parquet', 'GraphBased_val_inter_EWU+EU_sample_5pct.parquet', 'GraphBased_test_inter_EWU+EU_sample_5pct.parquet', 'GraphBased_val+test_inter_EWU+EU_sample_5pct.parquet', 'GraphBased_train_tweet_sample_5pct.parquet', 'GraphBased_val_tweet_sample_5pct.parquet', 'GraphBased_test_tweet_sample_5pct.parquet', 'GraphBased_val+test_tweet_sample_5pct.parquet', 'GraphBased_train_random_sample_10pct.parquet', 'GraphBased_val_random_sample_10pct.parquet', 'GraphBased_test_random_sample_10pct.parquet', 'GraphBased_val+test_random_sample_10pct.parquet', 'GraphBased_train_EWU_sample_10pct.parquet', 'GraphBased_val_EWU_sample_10pct.parquet', 'GraphBased_test_EWU_sample_10pct.parquet', 'GraphBased_val+test_EWU_sample_10pct.parquet', 'GraphBased_train_EU_sample_10pct.parquet', 'GraphBased_val_EU_sample_10pct.parquet', 'GraphBased_test_EU_sample_10pct.parquet', 'GraphBased_val+test_EU_sample_10pct.parquet', 'GraphBased_train_inter_EWU+EU_sample_10pct.parquet', 'GraphBased_val_inter_EWU+EU_sample_10pct.parquet', 'GraphBased_test_inter_EWU+EU_sample_10pct.parquet', 'GraphBased_val+test_inter_EWU+EU_sample_10pct.parquet', 'GraphBased_train_tweet_sample_10pct.parquet', 'GraphBased_val_tweet_sample_10pct.parquet', 'GraphBased_test_tweet_sample_10pct.parquet', 'GraphBased_val+test_tweet_sample_10pct.parquet', 'GraphBased_train.parquet', 'GraphBased_val.parquet', 'GraphBased_test.parquet', 'GraphBased_val+test.parquet']; 
backups: [].
Columns from train missing! ['graph_engagee_follows_engager_2d', 'graph_engager_follows_engagee_2d', 'ratio_engaged_to_engaging_follower_counts', 'ratio_engaged_to_engaging_following_counts'] from train_random_sample_1pct not in train!
Columns from val missing! ['graph_engagee_follows_engager_2d', 'graph_engager_follows_engagee_2d', 'ratio_engaged_to_engaging_follower_counts', 'ratio_engaged_to_engaging_following_counts'] from train_random_sample_1pct not in val!
Columns from test missing! ['graph_engagee_follows_engager_2d', 'graph_engager_follows_engagee_2d', 'ratio_engaged_to_engaging_follower_counts', 'ratio_engaged_to_engaging_following_counts'] from train_random_sample_1pct not in test!
Columns from val+test missing! ['graph_engagee_follows_engager_2d', 'graph_engager_follows_engagee_2d', 'ratio_engaged_to_engaging_follower_counts', 'ratio_engaged_to_engaging_following_counts'] from train_random_sample_1pct not in val+test!
train_random_sample_1pct ['text_tokens', 'hashtags', 'tweet_id', 'present_media', 'present_links', 'present_domains', 'tweet_type', 'language', 'tweet_timestamp', 'engaged_with_user_id', 'engaged_with_user_follower_count', 'engaged_with_user_following_count', 'engaged_with_user_is_verified', 'engaged_with_user_account_creation', 'engaging_user_id', 'engaging_user_follower_count', 'engaging_user_following_count', 'engaging_user_is_verified', 'engaging_user_account_creation', 'engagee_follows_engager', 'reply_timestamp', 'retweet_timestamp', 'retweet_with_comment_timestamp', 'like_timestamp', 'like', 'reply', 'retweet', 'quote', 'react', 'text', 'photos_count', 'videos_count', 'gifs_count', 'media_count', 'hashtags_count', 'links_count', 'domains_count', 'tweet_time', 'tweet_hour', 'tweet_weekday', 'tweet_weekday_str', 'tweet_day_of_year', 'corr_engaged_with_user_account_creation', 'engaged_creation_month', 'engaged_creation_year', 'engaged_age', 'corr_engaging_user_account_creation', 'engaging_creation_month', 'engaging_creation_year', 'engaging_age', 'creation_age_difference', 'graph_engagee_follows_engager_2d', 'graph_engager_follows_engagee_2d', 'graph_engaging_flag_like_from_engaged_1d', 'graph_engaging_flag_reply_from_engaged_1d', 'graph_engaging_flag_retweet_from_engaged_1d', 'graph_engaging_flag_quote_from_engaged_1d', 'graph_engaging_flag_react_from_engaged_1d', 'graph_engaging_count_like_from_engaged_1d', 'graph_engaging_count_reply_from_engaged_1d', 'graph_engaging_count_retweet_from_engaged_1d', 'graph_engaging_count_quote_from_engaged_1d', 'graph_engaging_count_react_from_engaged_1d', 'graph_engaged_flag_like_from_engaging_1d', 'graph_engaged_flag_reply_from_engaging_1d', 'graph_engaged_flag_retweet_from_engaging_1d', 'graph_engaged_flag_quote_from_engaging_1d', 'graph_engaged_flag_react_from_engaging_1d', 'graph_engaged_count_like_from_engaging_1d', 'graph_engaged_count_reply_from_engaging_1d', 'graph_engaged_count_retweet_from_engaging_1d', 'graph_engaged_count_quote_from_engaging_1d', 'graph_engaged_count_react_from_engaging_1d', 'graph_engaging_flag_like_from_engaged_2d', 'graph_engaging_flag_reply_from_engaged_2d', 'graph_engaging_flag_retweet_from_engaged_2d', 'graph_engaging_flag_quote_from_engaged_2d', 'graph_engaging_flag_react_from_engaged_2d', 'graph_engaging_count_like_from_engaged_2d', 'graph_engaging_count_reply_from_engaged_2d', 'graph_engaging_count_retweet_from_engaged_2d', 'graph_engaging_count_quote_from_engaged_2d', 'graph_engaging_count_react_from_engaged_2d', 'graph_engaged_flag_like_from_engaging_2d', 'graph_engaged_flag_reply_from_engaging_2d', 'graph_engaged_flag_retweet_from_engaging_2d', 'graph_engaged_flag_quote_from_engaging_2d', 'graph_engaged_flag_react_from_engaging_2d', 'graph_engaged_count_like_from_engaging_2d', 'graph_engaged_count_reply_from_engaging_2d', 'graph_engaged_count_retweet_from_engaging_2d', 'graph_engaged_count_quote_from_engaging_2d', 'graph_engaged_count_react_from_engaging_2d', 'ratio_engaged_to_engaging_follower_counts', 'ratio_engaged_to_engaging_following_counts']
Old columns from train_random_sample_1pct are kept.
Old columns from val_random_sample_1pct are kept.
Old columns from test_random_sample_1pct are kept.
Old columns from val+test_random_sample_1pct are kept.
Old columns from train_EWU_sample_1pct are kept.
Old columns from val_EWU_sample_1pct are kept.
Old columns from test_EWU_sample_1pct are kept.
Old columns from val+test_EWU_sample_1pct are kept.
Old columns from train_EU_sample_1pct are kept.
Old columns from val_EU_sample_1pct are kept.
Old columns from test_EU_sample_1pct are kept.
Old columns from val+test_EU_sample_1pct are kept.
Old columns from train_inter_EWU+EU_sample_1pct are kept.
Old columns from val_inter_EWU+EU_sample_1pct are kept.
Old columns from test_inter_EWU+EU_sample_1pct are kept.
Old columns from val+test_inter_EWU+EU_sample_1pct are kept.
Old columns from train_tweet_sample_1pct are kept.
Old columns from val_tweet_sample_1pct are kept.
Old columns from test_tweet_sample_1pct are kept.
Old columns from val+test_tweet_sample_1pct are kept.
Old columns from train_random_sample_2pct are kept.
Old columns from val_random_sample_2pct are kept.
Old columns from test_random_sample_2pct are kept.
Old columns from val+test_random_sample_2pct are kept.
Old columns from train_EWU_sample_2pct are kept.
Old columns from val_EWU_sample_2pct are kept.
Old columns from test_EWU_sample_2pct are kept.
Old columns from val+test_EWU_sample_2pct are kept.
Old columns from train_EU_sample_2pct are kept.
Old columns from val_EU_sample_2pct are kept.
Old columns from test_EU_sample_2pct are kept.
Old columns from val+test_EU_sample_2pct are kept.
Old columns from train_inter_EWU+EU_sample_2pct are kept.
Old columns from val_inter_EWU+EU_sample_2pct are kept.
Old columns from test_inter_EWU+EU_sample_2pct are kept.
Old columns from val+test_inter_EWU+EU_sample_2pct are kept.
Old columns from train_tweet_sample_2pct are kept.
Old columns from val_tweet_sample_2pct are kept.
Old columns from test_tweet_sample_2pct are kept.
Old columns from val+test_tweet_sample_2pct are kept.
Old columns from train_random_sample_5pct are kept.
Old columns from val_random_sample_5pct are kept.
Old columns from test_random_sample_5pct are kept.
Old columns from val+test_random_sample_5pct are kept.
Old columns from train_EWU_sample_5pct are kept.
Old columns from val_EWU_sample_5pct are kept.
Old columns from test_EWU_sample_5pct are kept.
Old columns from val+test_EWU_sample_5pct are kept.
Old columns from train_EU_sample_5pct are kept.
Old columns from val_EU_sample_5pct are kept.
Old columns from test_EU_sample_5pct are kept.
Old columns from val+test_EU_sample_5pct are kept.
Old columns from train_inter_EWU+EU_sample_5pct are kept.
Old columns from val_inter_EWU+EU_sample_5pct are kept.
Old columns from test_inter_EWU+EU_sample_5pct are kept.
Old columns from val+test_inter_EWU+EU_sample_5pct are kept.
Old columns from train_tweet_sample_5pct are kept.
Old columns from val_tweet_sample_5pct are kept.
Old columns from test_tweet_sample_5pct are kept.
Old columns from val+test_tweet_sample_5pct are kept.
Old columns from train_random_sample_10pct are kept.
Old columns from val_random_sample_10pct are kept.
Old columns from test_random_sample_10pct are kept.
Old columns from val+test_random_sample_10pct are kept.
Old columns from train_EWU_sample_10pct are kept.
Old columns from val_EWU_sample_10pct are kept.
Old columns from test_EWU_sample_10pct are kept.
Old columns from val+test_EWU_sample_10pct are kept.
Old columns from train_EU_sample_10pct are kept.
Old columns from val_EU_sample_10pct are kept.
Old columns from test_EU_sample_10pct are kept.
Old columns from val+test_EU_sample_10pct are kept.
Old columns from train_inter_EWU+EU_sample_10pct are kept.
Old columns from val_inter_EWU+EU_sample_10pct are kept.
Old columns from test_inter_EWU+EU_sample_10pct are kept.
Old columns from val+test_inter_EWU+EU_sample_10pct are kept.
Old columns from train_tweet_sample_10pct are kept.
Old columns from val_tweet_sample_10pct are kept.
Old columns from test_tweet_sample_10pct are kept.
Old columns from val+test_tweet_sample_10pct are kept.
Recreated columns graph_engagee_follows_engager_2d and graph_engager_follows_engagee_2d for train!
Recreated columns graph_engagee_follows_engager_2d and graph_engager_follows_engagee_2d for val!
Recreated columns graph_engagee_follows_engager_2d and graph_engager_follows_engagee_2d for test!
Recreated columns graph_engagee_follows_engager_2d and graph_engager_follows_engagee_2d for val+test!
Old columns from train_random_sample_1pct are kept.
Old columns from val_random_sample_1pct are kept.
Old columns from test_random_sample_1pct are kept.
Old columns from val+test_random_sample_1pct are kept.
Old columns from train_EWU_sample_1pct are kept.
Old columns from val_EWU_sample_1pct are kept.
Old columns from test_EWU_sample_1pct are kept.
Old columns from val+test_EWU_sample_1pct are kept.
Old columns from train_EU_sample_1pct are kept.
Old columns from val_EU_sample_1pct are kept.
Old columns from test_EU_sample_1pct are kept.
Old columns from val+test_EU_sample_1pct are kept.
Old columns from train_inter_EWU+EU_sample_1pct are kept.
Old columns from val_inter_EWU+EU_sample_1pct are kept.
Old columns from test_inter_EWU+EU_sample_1pct are kept.
Old columns from val+test_inter_EWU+EU_sample_1pct are kept.
Old columns from train_tweet_sample_1pct are kept.
Old columns from val_tweet_sample_1pct are kept.
Old columns from test_tweet_sample_1pct are kept.
Old columns from val+test_tweet_sample_1pct are kept.
Old columns from train_random_sample_2pct are kept.
Old columns from val_random_sample_2pct are kept.
Old columns from test_random_sample_2pct are kept.
Old columns from val+test_random_sample_2pct are kept.
Old columns from train_EWU_sample_2pct are kept.
Old columns from val_EWU_sample_2pct are kept.
Old columns from test_EWU_sample_2pct are kept.
Old columns from val+test_EWU_sample_2pct are kept.
Old columns from train_EU_sample_2pct are kept.
Old columns from val_EU_sample_2pct are kept.
Old columns from test_EU_sample_2pct are kept.
Old columns from val+test_EU_sample_2pct are kept.
Old columns from train_inter_EWU+EU_sample_2pct are kept.
Old columns from val_inter_EWU+EU_sample_2pct are kept.
Old columns from test_inter_EWU+EU_sample_2pct are kept.
Old columns from val+test_inter_EWU+EU_sample_2pct are kept.
Old columns from train_tweet_sample_2pct are kept.
Old columns from val_tweet_sample_2pct are kept.
Old columns from test_tweet_sample_2pct are kept.
Old columns from val+test_tweet_sample_2pct are kept.
Old columns from train_random_sample_5pct are kept.
Old columns from val_random_sample_5pct are kept.
Old columns from test_random_sample_5pct are kept.
Old columns from val+test_random_sample_5pct are kept.
Old columns from train_EWU_sample_5pct are kept.
Old columns from val_EWU_sample_5pct are kept.
Old columns from test_EWU_sample_5pct are kept.
Old columns from val+test_EWU_sample_5pct are kept.
Old columns from train_EU_sample_5pct are kept.
Old columns from val_EU_sample_5pct are kept.
Old columns from test_EU_sample_5pct are kept.
Old columns from val+test_EU_sample_5pct are kept.
Old columns from train_inter_EWU+EU_sample_5pct are kept.
Old columns from val_inter_EWU+EU_sample_5pct are kept.
Old columns from test_inter_EWU+EU_sample_5pct are kept.
Old columns from val+test_inter_EWU+EU_sample_5pct are kept.
Old columns from train_tweet_sample_5pct are kept.
Old columns from val_tweet_sample_5pct are kept.
Old columns from test_tweet_sample_5pct are kept.
Old columns from val+test_tweet_sample_5pct are kept.
Old columns from train_random_sample_10pct are kept.
Old columns from val_random_sample_10pct are kept.
Old columns from test_random_sample_10pct are kept.
Old columns from val+test_random_sample_10pct are kept.
Old columns from train_EWU_sample_10pct are kept.
Old columns from val_EWU_sample_10pct are kept.
Old columns from test_EWU_sample_10pct are kept.
Old columns from val+test_EWU_sample_10pct are kept.
Old columns from train_EU_sample_10pct are kept.
Old columns from val_EU_sample_10pct are kept.
Old columns from test_EU_sample_10pct are kept.
Old columns from val+test_EU_sample_10pct are kept.
Old columns from train_inter_EWU+EU_sample_10pct are kept.
Old columns from val_inter_EWU+EU_sample_10pct are kept.
Old columns from test_inter_EWU+EU_sample_10pct are kept.
Old columns from val+test_inter_EWU+EU_sample_10pct are kept.
Old columns from train_tweet_sample_10pct are kept.
Old columns from val_tweet_sample_10pct are kept.
Old columns from test_tweet_sample_10pct are kept.
Old columns from val+test_tweet_sample_10pct are kept.
Old columns from train are kept.
Old columns from val are kept.
Old columns from test are kept.
Old columns from val+test are kept.
Old columns from train_random_sample_1pct are kept.
Old columns from val_random_sample_1pct are kept.
Old columns from test_random_sample_1pct are kept.
Old columns from val+test_random_sample_1pct are kept.
Old columns from train_EWU_sample_1pct are kept.
Old columns from val_EWU_sample_1pct are kept.
Old columns from test_EWU_sample_1pct are kept.
Old columns from val+test_EWU_sample_1pct are kept.
Old columns from train_EU_sample_1pct are kept.
Old columns from val_EU_sample_1pct are kept.
Old columns from test_EU_sample_1pct are kept.
Old columns from val+test_EU_sample_1pct are kept.
Old columns from train_inter_EWU+EU_sample_1pct are kept.
Old columns from val_inter_EWU+EU_sample_1pct are kept.
Old columns from test_inter_EWU+EU_sample_1pct are kept.
Old columns from val+test_inter_EWU+EU_sample_1pct are kept.
Old columns from train_tweet_sample_1pct are kept.
Old columns from val_tweet_sample_1pct are kept.
Old columns from test_tweet_sample_1pct are kept.
Old columns from val+test_tweet_sample_1pct are kept.
Old columns from train_random_sample_2pct are kept.
Old columns from val_random_sample_2pct are kept.
Old columns from test_random_sample_2pct are kept.
Old columns from val+test_random_sample_2pct are kept.
Old columns from train_EWU_sample_2pct are kept.
Old columns from val_EWU_sample_2pct are kept.
Old columns from test_EWU_sample_2pct are kept.
Old columns from val+test_EWU_sample_2pct are kept.
Old columns from train_EU_sample_2pct are kept.
Old columns from val_EU_sample_2pct are kept.
Old columns from test_EU_sample_2pct are kept.
Old columns from val+test_EU_sample_2pct are kept.
Old columns from train_inter_EWU+EU_sample_2pct are kept.
Old columns from val_inter_EWU+EU_sample_2pct are kept.
Old columns from test_inter_EWU+EU_sample_2pct are kept.
Old columns from val+test_inter_EWU+EU_sample_2pct are kept.
Old columns from train_tweet_sample_2pct are kept.
Old columns from val_tweet_sample_2pct are kept.
Old columns from test_tweet_sample_2pct are kept.
Old columns from val+test_tweet_sample_2pct are kept.
Old columns from train_random_sample_5pct are kept.
Old columns from val_random_sample_5pct are kept.
Old columns from test_random_sample_5pct are kept.
Old columns from val+test_random_sample_5pct are kept.
Old columns from train_EWU_sample_5pct are kept.
Old columns from val_EWU_sample_5pct are kept.
Old columns from test_EWU_sample_5pct are kept.
Old columns from val+test_EWU_sample_5pct are kept.
Old columns from train_EU_sample_5pct are kept.
Old columns from val_EU_sample_5pct are kept.
Old columns from test_EU_sample_5pct are kept.
Old columns from val+test_EU_sample_5pct are kept.
Old columns from train_inter_EWU+EU_sample_5pct are kept.
Old columns from val_inter_EWU+EU_sample_5pct are kept.
Old columns from test_inter_EWU+EU_sample_5pct are kept.
Old columns from val+test_inter_EWU+EU_sample_5pct are kept.
Old columns from train_tweet_sample_5pct are kept.
Old columns from val_tweet_sample_5pct are kept.
Old columns from test_tweet_sample_5pct are kept.
Old columns from val+test_tweet_sample_5pct are kept.
Old columns from train_random_sample_10pct are kept.
Old columns from val_random_sample_10pct are kept.
Old columns from test_random_sample_10pct are kept.
Old columns from val+test_random_sample_10pct are kept.
Old columns from train_EWU_sample_10pct are kept.
Old columns from val_EWU_sample_10pct are kept.
Old columns from test_EWU_sample_10pct are kept.
Old columns from val+test_EWU_sample_10pct are kept.
Old columns from train_EU_sample_10pct are kept.
Old columns from val_EU_sample_10pct are kept.
Old columns from test_EU_sample_10pct are kept.
Old columns from val+test_EU_sample_10pct are kept.
Old columns from train_inter_EWU+EU_sample_10pct are kept.
Old columns from val_inter_EWU+EU_sample_10pct are kept.
Old columns from test_inter_EWU+EU_sample_10pct are kept.
Old columns from val+test_inter_EWU+EU_sample_10pct are kept.
Old columns from train_tweet_sample_10pct are kept.
Old columns from val_tweet_sample_10pct are kept.
Old columns from test_tweet_sample_10pct are kept.
Old columns from val+test_tweet_sample_10pct are kept.
Now recreating the ratio_engaged_to_engaging_follower_counts for train.
Now recreating the ratio_engaged_to_engaging_follower_counts for val.
Now recreating the ratio_engaged_to_engaging_follower_counts for test.
Now recreating the ratio_engaged_to_engaging_follower_counts for val+test.
Old columns from train_random_sample_1pct are kept.
Old columns from val_random_sample_1pct are kept.
Old columns from test_random_sample_1pct are kept.
Old columns from val+test_random_sample_1pct are kept.
Old columns from train_EWU_sample_1pct are kept.
Old columns from val_EWU_sample_1pct are kept.
Old columns from test_EWU_sample_1pct are kept.
Old columns from val+test_EWU_sample_1pct are kept.
Old columns from train_EU_sample_1pct are kept.
Old columns from val_EU_sample_1pct are kept.
Old columns from test_EU_sample_1pct are kept.
Old columns from val+test_EU_sample_1pct are kept.
Old columns from train_inter_EWU+EU_sample_1pct are kept.
Old columns from val_inter_EWU+EU_sample_1pct are kept.
Old columns from test_inter_EWU+EU_sample_1pct are kept.
Old columns from val+test_inter_EWU+EU_sample_1pct are kept.
Old columns from train_tweet_sample_1pct are kept.
Old columns from val_tweet_sample_1pct are kept.
Old columns from test_tweet_sample_1pct are kept.
Old columns from val+test_tweet_sample_1pct are kept.
Old columns from train_random_sample_2pct are kept.
Old columns from val_random_sample_2pct are kept.
Old columns from test_random_sample_2pct are kept.
Old columns from val+test_random_sample_2pct are kept.
Old columns from train_EWU_sample_2pct are kept.
Old columns from val_EWU_sample_2pct are kept.
Old columns from test_EWU_sample_2pct are kept.
Old columns from val+test_EWU_sample_2pct are kept.
Old columns from train_EU_sample_2pct are kept.
Old columns from val_EU_sample_2pct are kept.
Old columns from test_EU_sample_2pct are kept.
Old columns from val+test_EU_sample_2pct are kept.
Old columns from train_inter_EWU+EU_sample_2pct are kept.
Old columns from val_inter_EWU+EU_sample_2pct are kept.
Old columns from test_inter_EWU+EU_sample_2pct are kept.
Old columns from val+test_inter_EWU+EU_sample_2pct are kept.
Old columns from train_tweet_sample_2pct are kept.
Old columns from val_tweet_sample_2pct are kept.
Old columns from test_tweet_sample_2pct are kept.
Old columns from val+test_tweet_sample_2pct are kept.
Old columns from train_random_sample_5pct are kept.
Old columns from val_random_sample_5pct are kept.
Old columns from test_random_sample_5pct are kept.
Old columns from val+test_random_sample_5pct are kept.
Old columns from train_EWU_sample_5pct are kept.
Old columns from val_EWU_sample_5pct are kept.
Old columns from test_EWU_sample_5pct are kept.
Old columns from val+test_EWU_sample_5pct are kept.
Old columns from train_EU_sample_5pct are kept.
Old columns from val_EU_sample_5pct are kept.
Old columns from test_EU_sample_5pct are kept.
Old columns from val+test_EU_sample_5pct are kept.
Old columns from train_inter_EWU+EU_sample_5pct are kept.
Old columns from val_inter_EWU+EU_sample_5pct are kept.
Old columns from test_inter_EWU+EU_sample_5pct are kept.
Old columns from val+test_inter_EWU+EU_sample_5pct are kept.
Old columns from train_tweet_sample_5pct are kept.
Old columns from val_tweet_sample_5pct are kept.
Old columns from test_tweet_sample_5pct are kept.
Old columns from val+test_tweet_sample_5pct are kept.
Old columns from train_random_sample_10pct are kept.
Old columns from val_random_sample_10pct are kept.
Old columns from test_random_sample_10pct are kept.
Old columns from val+test_random_sample_10pct are kept.
Old columns from train_EWU_sample_10pct are kept.
Old columns from val_EWU_sample_10pct are kept.
Old columns from test_EWU_sample_10pct are kept.
Old columns from val+test_EWU_sample_10pct are kept.
Old columns from train_EU_sample_10pct are kept.
Old columns from val_EU_sample_10pct are kept.
Old columns from test_EU_sample_10pct are kept.
Old columns from val+test_EU_sample_10pct are kept.
Old columns from train_inter_EWU+EU_sample_10pct are kept.
Old columns from val_inter_EWU+EU_sample_10pct are kept.
Old columns from test_inter_EWU+EU_sample_10pct are kept.
Old columns from val+test_inter_EWU+EU_sample_10pct are kept.
Old columns from train_tweet_sample_10pct are kept.
Old columns from val_tweet_sample_10pct are kept.
Old columns from test_tweet_sample_10pct are kept.
Old columns from val+test_tweet_sample_10pct are kept.
Now recreating the ratio_engaged_to_engaging_following_counts for train.
Now recreating the ratio_engaged_to_engaging_following_counts for val.
Now recreating the ratio_engaged_to_engaging_following_counts for test.
Now recreating the ratio_engaged_to_engaging_following_counts for val+test.
Traceback (most recent call last):
  File "/home/adbs20/e01528091/Master/2020recsystwitter/PS_FeatureEng/PySpark-FeatureEng-02-GraphBased.py", line 452, in <module>
    files_to_be_exported=changed_dfs)
  File "/home/adbs20/e01528091/Master/2020recsystwitter/PS_FeatureEng/Functions/export_dataframes.py", line 26, in export_dataframes
    print(f"Now at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')} exporting: {key}, {dfs[key].count()}")  # light action
  File "/opt/cloudera/parcels/CDH-6.3.2-1.cdh6.3.2.p0.1605554/lib/spark/python/lib/pyspark.zip/pyspark/sql/dataframe.py", line 522, in count
  File "/opt/cloudera/parcels/CDH-6.3.2-1.cdh6.3.2.p0.1605554/lib/spark/python/lib/py4j-0.10.7-src.zip/py4j/java_gateway.py", line 1257, in __call__
  File "/opt/cloudera/parcels/CDH-6.3.2-1.cdh6.3.2.p0.1605554/lib/spark/python/lib/pyspark.zip/pyspark/sql/utils.py", line 63, in deco
  File "/opt/cloudera/parcels/CDH-6.3.2-1.cdh6.3.2.p0.1605554/lib/spark/python/lib/py4j-0.10.7-src.zip/py4j/protocol.py", line 328, in get_return_value
py4j.protocol.Py4JJavaError: An error occurred while calling o20079.count.
: org.apache.spark.SparkException: Job aborted due to stage failure: Task 57 in stage 469.0 failed 4 times, most recent failure: Lost task 57.3 in stage 469.0 (TID 24805, c110.local, executor 7): org.apache.spark.memory.SparkOutOfMemoryError: Unable to acquire 7956323072 bytes of memory, got 0
	at org.apache.spark.memory.MemoryConsumer.throwOom(MemoryConsumer.java:157)
	at org.apache.spark.memory.MemoryConsumer.allocateArray(MemoryConsumer.java:98)
	at org.apache.spark.sql.execution.UnsafeKVExternalSorter.<init>(UnsafeKVExternalSorter.java:117)
	at org.apache.spark.sql.execution.UnsafeFixedWidthAggregationMap.destructAndCreateExternalSorter(UnsafeFixedWidthAggregationMap.java:248)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.agg_doConsume_0$(Unknown Source)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.agg_doAggregateWithKeys_0$(Unknown Source)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.processNext(Unknown Source)
	at org.apache.spark.sql.execution.BufferedRowIterator.hasNext(BufferedRowIterator.java:43)
	at org.apache.spark.sql.execution.WholeStageCodegenExec$$anonfun$13$$anon$2.hasNext(WholeStageCodegenExec.scala:643)
	at scala.collection.Iterator$$anon$11.hasNext(Iterator.scala:409)
	at org.apache.spark.shuffle.sort.BypassMergeSortShuffleWriter.write(BypassMergeSortShuffleWriter.java:125)
	at org.apache.spark.scheduler.ShuffleMapTask.runTask(ShuffleMapTask.scala:99)
	at org.apache.spark.scheduler.ShuffleMapTask.runTask(ShuffleMapTask.scala:55)
	at org.apache.spark.scheduler.Task.run(Task.scala:121)
	at org.apache.spark.executor.Executor$TaskRunner$$anonfun$11.apply(Executor.scala:407)
	at org.apache.spark.util.Utils$.tryWithSafeFinally(Utils.scala:1408)
	at org.apache.spark.executor.Executor$TaskRunner.run(Executor.scala:413)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

Driver stacktrace:
	at org.apache.spark.scheduler.DAGScheduler.org$apache$spark$scheduler$DAGScheduler$$failJobAndIndependentStages(DAGScheduler.scala:1890)
	at org.apache.spark.scheduler.DAGScheduler$$anonfun$abortStage$1.apply(DAGScheduler.scala:1878)
	at org.apache.spark.scheduler.DAGScheduler$$anonfun$abortStage$1.apply(DAGScheduler.scala:1877)
	at scala.collection.mutable.ResizableArray$class.foreach(ResizableArray.scala:59)
	at scala.collection.mutable.ArrayBuffer.foreach(ArrayBuffer.scala:48)
	at org.apache.spark.scheduler.DAGScheduler.abortStage(DAGScheduler.scala:1877)
	at org.apache.spark.scheduler.DAGScheduler$$anonfun$handleTaskSetFailed$1.apply(DAGScheduler.scala:929)
	at org.apache.spark.scheduler.DAGScheduler$$anonfun$handleTaskSetFailed$1.apply(DAGScheduler.scala:929)
	at scala.Option.foreach(Option.scala:257)
	at org.apache.spark.scheduler.DAGScheduler.handleTaskSetFailed(DAGScheduler.scala:929)
	at org.apache.spark.scheduler.DAGSchedulerEventProcessLoop.doOnReceive(DAGScheduler.scala:2111)
	at org.apache.spark.scheduler.DAGSchedulerEventProcessLoop.onReceive(DAGScheduler.scala:2060)
	at org.apache.spark.scheduler.DAGSchedulerEventProcessLoop.onReceive(DAGScheduler.scala:2049)
	at org.apache.spark.util.EventLoop$$anon$1.run(EventLoop.scala:49)
	at org.apache.spark.scheduler.DAGScheduler.runJob(DAGScheduler.scala:740)
	at org.apache.spark.SparkContext.runJob(SparkContext.scala:2081)
	at org.apache.spark.SparkContext.runJob(SparkContext.scala:2102)
	at org.apache.spark.SparkContext.runJob(SparkContext.scala:2121)
	at org.apache.spark.SparkContext.runJob(SparkContext.scala:2146)
	at org.apache.spark.rdd.RDD$$anonfun$collect$1.apply(RDD.scala:945)
	at org.apache.spark.rdd.RDDOperationScope$.withScope(RDDOperationScope.scala:151)
	at org.apache.spark.rdd.RDDOperationScope$.withScope(RDDOperationScope.scala:112)
	at org.apache.spark.rdd.RDD.withScope(RDD.scala:363)
	at org.apache.spark.rdd.RDD.collect(RDD.scala:944)
	at org.apache.spark.sql.execution.SparkPlan.executeCollect(SparkPlan.scala:299)
	at org.apache.spark.sql.Dataset$$anonfun$count$1.apply(Dataset.scala:2830)
	at org.apache.spark.sql.Dataset$$anonfun$count$1.apply(Dataset.scala:2829)
	at org.apache.spark.sql.Dataset$$anonfun$53.apply(Dataset.scala:3364)
	at org.apache.spark.sql.execution.SQLExecution$$anonfun$withNewExecutionId$1.apply(SQLExecution.scala:78)
	at org.apache.spark.sql.execution.SQLExecution$.withSQLConfPropagated(SQLExecution.scala:125)
	at org.apache.spark.sql.execution.SQLExecution$.withNewExecutionId(SQLExecution.scala:73)
	at org.apache.spark.sql.Dataset.withAction(Dataset.scala:3363)
	at org.apache.spark.sql.Dataset.count(Dataset.scala:2829)
	at sun.reflect.GeneratedMethodAccessor60.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at py4j.reflection.MethodInvoker.invoke(MethodInvoker.java:244)
	at py4j.reflection.ReflectionEngine.invoke(ReflectionEngine.java:357)
	at py4j.Gateway.invoke(Gateway.java:282)
	at py4j.commands.AbstractCommand.invokeMethod(AbstractCommand.java:132)
	at py4j.commands.CallCommand.execute(CallCommand.java:79)
	at py4j.GatewayConnection.run(GatewayConnection.java:238)
	at java.lang.Thread.run(Thread.java:748)
Caused by: org.apache.spark.memory.SparkOutOfMemoryError: Unable to acquire 7956323072 bytes of memory, got 0
	at org.apache.spark.memory.MemoryConsumer.throwOom(MemoryConsumer.java:157)
	at org.apache.spark.memory.MemoryConsumer.allocateArray(MemoryConsumer.java:98)
	at org.apache.spark.sql.execution.UnsafeKVExternalSorter.<init>(UnsafeKVExternalSorter.java:117)
	at org.apache.spark.sql.execution.UnsafeFixedWidthAggregationMap.destructAndCreateExternalSorter(UnsafeFixedWidthAggregationMap.java:248)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.agg_doConsume_0$(Unknown Source)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.agg_doAggregateWithKeys_0$(Unknown Source)
	at org.apache.spark.sql.catalyst.expressions.GeneratedClass$GeneratedIteratorForCodegenStage37.processNext(Unknown Source)
	at org.apache.spark.sql.execution.BufferedRowIterator.hasNext(BufferedRowIterator.java:43)
	at org.apache.spark.sql.execution.WholeStageCodegenExec$$anonfun$13$$anon$2.hasNext(WholeStageCodegenExec.scala:643)
	at scala.collection.Iterator$$anon$11.hasNext(Iterator.scala:409)
	at org.apache.spark.shuffle.sort.BypassMergeSortShuffleWriter.write(BypassMergeSortShuffleWriter.java:125)
	at org.apache.spark.scheduler.ShuffleMapTask.runTask(ShuffleMapTask.scala:99)
	at org.apache.spark.scheduler.ShuffleMapTask.runTask(ShuffleMapTask.scala:55)
	at org.apache.spark.scheduler.Task.run(Task.scala:121)
	at org.apache.spark.executor.Executor$TaskRunner$$anonfun$11.apply(Executor.scala:407)
	at org.apache.spark.util.Utils$.tryWithSafeFinally(Utils.scala:1408)
	at org.apache.spark.executor.Executor$TaskRunner.run(Executor.scala:413)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	... 1 more

