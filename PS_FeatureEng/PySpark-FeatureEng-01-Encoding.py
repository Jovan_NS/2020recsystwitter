#!/usr/bin/env python
# coding: utf-8

# # 01 Encoding
# 
# Note that in this notebook we also create val+test.

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
import pyspark.sql.types as t
from operator import add

from Functions.import_dataframes import import_dataframes
from Functions.export_dataframes import export_dataframes
from Functions.check_if_columns_are_missing import check_if_columns_are_missing
from Functions.prepare_for_new_columns import prepare_for_new_columns


# In[2]:


# Other imports
import os
import string
from collections import Counter
import pandas as pd
import copy
from datetime import datetime


# In[3]:


# If on the TU Wien's LBD cluster use yarn, otherwise use local
working_on_cluster = "adbs20" in os.path.abspath(os.getcwd())
master = "yarn" if working_on_cluster else "local"

# Initialise Spark #session 
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_01_Encoding")     .master(master)     .config("spark.executor.memory", "32G")     .config("spark.driver.memory", "32G")     .config("spark.driver.maxResultSize", "16G")     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", 8)     .config("spark.sql.broadcastTimeout", 900)     .config('spark.sql.session.timeZone', 'UTC')     .config('spark.driver.extraJavaOptions', '-Duser.timezone=UTC')     .config('spark.executor.extraJavaOptions', '-Duser.timezone=UTC')     .getOrCreate()
    #.jar("glint-fmpair/target/scala-2.11/glint-fmpair-assembly-1.0.jar", "pyspark-shell") \

# from glintfmpair import WeightHotEncoderEstimator, WeightHotEncoderModel, GlintFMPair, GlintFMPairModel, PopRank, SAGH, KNN, TfIdfKNN    


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


# General setings
DEV = False # set to True to use smaller datasets
RECREATE_EVEN_IF_ALREADY_EXIST = False # if True, the code will force recreation of steps
CALCULATE_STEPS = False # calculate inbetween steps and statistics - provides more transparent progress, but might slow down the run
FEATURESET_EXPORT_PREFIX = "Encoding_" # import-export filename prefix for features created here
BACKUP_PREFIX = "FE_"

HDFS_DATAFOLDER = "Data" if master == "yarn" else os.path.join("..", "Data")
SAMPLING_TECHNIQUES =  ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
SAMPLING_PERCENTAGES = ("1pct", "2pct", "5pct", "10pct")
TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"
DATASETS = (TRAIN_NAME, VAL_NAME, TEST_NAME)


# In[6]:


dfs, changed_dfs = import_dataframes(spark, datasets=DATASETS,
                                             sampling_techniques=SAMPLING_TECHNIQUES,
                                             sampling_percentages=SAMPLING_PERCENTAGES,
                                             featureset_export_prefix=FEATURESET_EXPORT_PREFIX,
                                             backup_featureset_prefixes=BACKUP_PREFIX,
                                             recreate_even_if_already_exist=RECREATE_EVEN_IF_ALREADY_EXIST,
                                             HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# In[7]:


if DEV:
    trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
    val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
else:
    trn_key = TRAIN_NAME
    val_key = VAL_NAME


# In[8]:


check_if_columns_are_missing(dfs, trn_key)

if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].limit(5).toPandas())
    
dfs[trn_key].columns


# ## Feature Engineering

# ### Media
# 
#  - photos\_count: number of photos in the tweet.
#  
#  - videos\_count: number of videos in the tweet.
#  
#  - gifs\_count: number of gifs in the tweet.
#  
#  - media\_count: sum of the three features above.

# In[9]:


from Functions.fe01_count_media import count_media

new_cols = ["photos_count", "videos_count", "gifs_count", "media_count"]


for key in dfs:
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=new_cols,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    if recreate_columns:
        dfs[key] = count_media(dfs[key])
        print(f"Recreated columns {new_cols} for {key}!")
        
    
if CALCULATE_STEPS:
    dfs[trn_key][["photos_count", "videos_count", "gifs_count", "media_count"]].show(40)


# ### Hastags and Links Counts
# 
#  - hashtags\_count: number of hashtags in the target tweet.
#  - links\_count: number of links in the target tweet.
#  - domains\_count: number of domains in the target tweet. 

# In[10]:


from Functions.fe01_count_tweet_elements import count_tweet_elements

input_cols = ['hashtags','present_links','present_domains',]
output_cols = ['hashtags_count','links_count','domains_count',]

for key in dfs:
    for inc, outc in zip(input_cols, output_cols):
        dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=output_cols,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
        if recreate_columns:
            dfs[key] = count_tweet_elements(dfs[key], inc=inc, outc=outc)
    print(f"Done with {key}.")
            
            
if CALCULATE_STEPS:
    dfs[trn_key][output_cols].show(40)


# ### Tweet Timestamp-based Features
# 
# We create the following two tweet timestamp-related features.
# 
#  - weekday: day of the week of the publication of the tweet.
#  - hour: hour of the day of the publication of the tweet (UTC).

# In[11]:


from Functions.fe01_extract_time_hour_and_weekday import extract_time_hour_and_weekday

new_cols = ["tweet_time", "tweet_hour", "tweet_weekday", "tweet_weekday_str", "tweet_day_of_year"]

for key in dfs:
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=new_cols,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    if recreate_columns:
        dfs[key] = extract_time_hour_and_weekday(dfs[key])

        
if CALCULATE_STEPS:
    display(dfs[trn_key].select(new_cols).show(4))
    display(dfs[trn_key].select(new_cols).describe())


# ### Account Age Timestamp-based Features
# 
# We then create these two timestamp-based features:
# 
#  - engaging\_age: the age of the engaging user as a number of months that passed between March 2006 and the month of the account of creation.
#  - engaged\_age: as binned\_engaging\_age but for the engaged-with user.

# In[12]:


from Functions.fe01_get_account_age import get_account_age

input_cols = ['engaged_with_user_account_creation','engaging_user_account_creation']
output_cols = ['engaged_age','engaging_age']
output_prefixes = ['engaged_','engaging_']

for key in dfs:
    for inc, outc, outpref in zip(input_cols, output_cols, output_prefixes):
        dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=output_cols,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
        if recreate_columns:
            dfs[key] = get_account_age(dfs[key], timestampcol = inc, corr_prefix = "corr_", final_prefix = outpref)
            
            
if CALCULATE_STEPS:
    cols = ['engaged_creation_year','engaged_age','engaging_creation_year','engaging_age']
    display(dfs[trn_key][cols].describe().toPandas())
    display(dfs[val_key][cols].describe().toPandas())
    display(dfs[trn_key].filter(f.col("engaging_age") < 0).select(["engaging_user_id", "engaging_user_account_creation", "engaging_creation_year", "engaging_creation_month", "engaging_age"]).show())
    display(dfs[val_key].filter(f.col("engaging_age") < 0).select(["engaging_user_id", "engaging_user_account_creation", "engaging_creation_year", "engaging_creation_month", "engaging_age"]).show())


# ## Relative Age
# 
# Here we add creation_age_difference as the simple difference of engaged_age and engaging_age.

# In[13]:


for key in dfs:
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name="creation_age_difference",
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    if recreate_columns:
        dfs[key] = dfs[key].withColumn("creation_age_difference", f.col("engaged_age") - f.col("engaging_age"))

        
if CALCULATE_STEPS:
    display(dfs[trn_key].select(["engaged_age", "engaging_age", "creation_age_difference"]).show(4))


# ## Export Files

# ## Export the Results

# In[14]:


export_dataframes(dfs=dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                  files_to_be_exported=changed_dfs)


# ## Create test+val Datasets

# In[15]:


vt_dfs = {}

for key in dfs:
    if ("test" in key):
        test_key = key
        val_key = test_key.replace("test", "val")
        new_vt_key = test_key.replace("test", "val+test")
        vt_dfs[new_vt_key] = dfs[val_key].unionByName(dfs[test_key])

        assert len(dfs[val_key].columns) == len(dfs[test_key].columns)
        assert len(vt_dfs[new_vt_key].columns) == len(dfs[test_key].columns)
        assert vt_dfs[new_vt_key].count() == (dfs[val_key].count() + dfs[test_key].count())


# Export the new datasets.

# In[16]:


export_dataframes(dfs=vt_dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, 
                  HDFS_datafolder=HDFS_DATAFOLDER, files_to_be_exported=vt_dfs.keys())


# In[17]:


print("done")

