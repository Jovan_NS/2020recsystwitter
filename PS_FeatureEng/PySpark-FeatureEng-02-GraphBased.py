#!/usr/bin/env python
# coding: utf-8

# # 02 Graph-Based Features
# 
# 

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
import pyspark.sql.types as t
from operator import add

from Functions.import_dataframes import import_dataframes
from Functions.export_dataframes import export_dataframes
from Functions.show_final_statistics import show_final_statistics
from Functions.check_if_columns_are_missing import check_if_columns_are_missing
from Functions.prepare_for_new_columns import prepare_for_new_columns
from Functions.pyspark_df_shape import pyspark_df_shape


# In[2]:


# Other imports
import os
import string
from collections import Counter
import pandas as pd
import copy


# In[3]:


# If on the TU Wien's LBD cluster use yarn, otherwise use local
working_on_cluster = "adbs20" in os.path.abspath(os.getcwd())
master = "yarn" if working_on_cluster else "local"
memory = "120G" if working_on_cluster else "8G"
cores = 16 if working_on_cluster else 8
timeout = 7200 if working_on_cluster else 900

# Initialise Spark 
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_02_GraphBased")     .master(master)     .config("spark.executor.memory", memory)     .config("spark.driver.memory", memory)     .config("spark.driver.maxResultSize", memory)     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", cores)     .config("spark.sql.broadcastTimeout", timeout)     .config('spark.sql.session.timeZone', 'UTC')     .config('spark.driver.extraJavaOptions', '-Duser.timezone=UTC')     .config('spark.executor.extraJavaOptions', '-Duser.timezone=UTC')     .getOrCreate()


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


# General setings
RECREATE_EVEN_IF_ALREADY_EXIST = False # if True, the code will force recreation of steps
CALCULATE_STEPS = False # calculate inbetween steps and statistics - provides more transparent progress, but might slow down the run
DEV = False # Set to true to use smaller datasets
FEATURESET_EXPORT_PREFIX = "GraphBased_" # import-export filename prefix for features created here
BACKUP_PREFIX = "Encoding_" # in case a file with this prefix was not generated yet, load it from files with this prefix


HDFS_DATAFOLDER = "Data" if working_on_cluster else os.path.join("..", "Data")
SAMPLING_TECHNIQUES = ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
SAMPLING_PERCENTAGES = ("1pct", "2pct", "5pct", "10pct")
TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"
VT_NAME = "val+test"

# note the distintion below in comparison to FE 00 and 01
IMPORT_DATASETS = (TRAIN_NAME, VAL_NAME, TEST_NAME, VT_NAME)


# In[6]:


dfs, changed_dfs = import_dataframes(spark, datasets=IMPORT_DATASETS,
       sampling_techniques=SAMPLING_TECHNIQUES,
       sampling_percentages=SAMPLING_PERCENTAGES,
       featureset_export_prefix=FEATURESET_EXPORT_PREFIX,
       backup_featureset_prefixes=BACKUP_PREFIX,
       recreate_even_if_already_exist=RECREATE_EVEN_IF_ALREADY_EXIST, 
       HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# In[7]:


'''if DEV:
    trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
    val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
else:
    trn_key = TRAIN_NAME
    val_key = VAL_NAME'''

trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]


# In[8]:


check_if_columns_are_missing(dfs, trn_key)

if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].limit(10).toPandas())

print(trn_key, dfs[trn_key].columns)


# ## Data Split
# 
# Since this type of features is modelled to represent more static aspects of the context (i.e. whether a relationship between two users even exists), we will use all information available. This is in particular:
# 
# * full train (train)
# * full train+val (for val)
# * full train+test+val (for test+val)
# 
# For a tweet from Friday in train dataset, we would therefore also include info from tweets from Saturday in the train dataset. In case of inconsistances, we let the feature be positive if there is at least one positive instance, for practical and sematical reasons.

# In[9]:


from Functions.create_cumulative_dfs_for_static_feature_creation import create_cumulative_dfs_for_static_feature_creation

cumulative_dfs = create_cumulative_dfs_for_static_feature_creation(dfs, column_mismatch_okay = not RECREATE_EVEN_IF_ALREADY_EXIST)
cumulative_dfs.keys()


# ## Feature Engineering
# 
# ### Follows
# 
# The original dataset contained a feature indicating whether the account of the author (also referred to as engagee, engaged user, and engaged-with user) followed the account that had viewed their tweet (i.e. the viewer, the engager or the engaging user). The opposite direction is not provided since, by design of dataset, the viewer always follows the author. This has been done in order not to reveal a non-public info regarding which tweets a user has seen but chosen not to engage with. So the authors created the pseudo-negative dataset by taking a subset of all tweets written by users the engaging users follows with which they did not interact (either because the engager actually has seen the tweet and chose not to interact with it or because the engager has not seen the tweet at all). 
# 
# Based on these provided first-degree connections, we added second-degree connections by, essentially, joining the list of following relations with itself (with the joining key being the followed users on one side and following users on the other) and then adding the new list's followed users as the old list's following users second degree connections.
# 
# #### First Degree Follows
# 
# We must first create for each dataset a comprehensive list of first-degree follows relations.

# In[10]:


from Functions.fe02_extract_follows import extract_follows

follows = {}

for key in dfs:
    # by using cumulative_dfs[key] instead of dfs[key] we also consider train instances for val and test
    # https://prnt.sc/26d8m9e ; https://prnt.sc/26d8mf5
    follows[key] = extract_follows(cumulative_dfs[key], flag_column = "follows")


# #### Second Degree Follows
# 
# Now let us get second degree follows and then extract the following features:
# 
#  - graph\_engagee\_follows\_engager\_2d: did the author of a tweet follow a third user who followed the viewer?
#  - graph\_engager\_follows\_engagee\_2d: did the user who has seen the author's tweet follow a third user who followed the author?
#  
# We start by creating a graph of second degree follows.

# In[11]:


from Functions.fe02_extract_follows_2d import extract_follows_2d

follows_2d = {}

for key in dfs:
    follows_2d[key] = extract_follows_2d(follows[key], flag_column = "follows")


# In[12]:


if CALCULATE_STEPS:
    print(follows[val_key].columns)
    print(follows_2d[val_key].columns)
    print(follows[val_key].count(), follows_2d[val_key].count())
    print(dfs[val_key].columns)
    print("graph_engagee_follows_engager_2d" in dfs[val_key].columns)


# #### Follows Features
# 
# Then we use the graph of second degree follows to create the new features -- ```graph_engagee_follows_engager_2d``` and ```graph_engager_follows_engagee_2d```.

# In[13]:


from Functions.fe02_extract_graph_relations import extract_graph_relations

for key in dfs:
    train_graph = None
    '''
    # From previous version, where train was not already included in train and val
    if ("test" in key) or ("val" in key):
        train_key = key.replace("test", "train").replace("val", "train")
        train_graph = follows_2d[train_key]
    '''
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=("graph_engagee_follows_engager_2d","graph_engager_follows_engagee_2d",),
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    if recreate_columns:
        dfs[key] = extract_graph_relations(df = dfs[key], graph = follows_2d[key], acting_user = "engaged_with_user_id", acted_upon_user = "engaging_user_id", 
                             new_feature_name = "graph_engagee_follows_engager_2d", null_as_false = False, graph_flag_column = "follows", train_graph = train_graph)
        dfs[key] = extract_graph_relations(df = dfs[key], graph = follows_2d[key], acting_user = "engaging_user_id", acted_upon_user = "engaged_with_user_id", 
                             new_feature_name = "graph_engager_follows_engagee_2d", null_as_false = False, graph_flag_column = "follows", train_graph = train_graph)
        print(f"Recreated columns graph_engagee_follows_engager_2d and graph_engager_follows_engagee_2d for {key}!")


if CALCULATE_STEPS:
    # http://prntscr.com/1v9or79
    # before cummulative: https://prnt.sc/26d8rgc ; after cummulative dfs for follows: 
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].limit(4).toPandas())
        display(dfs[trn_key].groupBy('graph_engager_follows_engagee_2d').count().toPandas())
        display(dfs[trn_key].groupBy('graph_engagee_follows_engager_2d').count().toPandas())


# ## Engagements
# 
# We also want to create first and second degree connections for all $4+1$ engagement types. 
# 
# 
# Since val and test originally did not have engagements, we will use training data for these two. For training, we only use the first three days (out of seven!). This is saved in ```restricted_dfs```.

# In[14]:


if CALCULATE_STEPS:
    dfs[trn_key].select("tweet_day_of_year").describe().show()
    dfs[trn_key].select("tweet_day_of_year").printSchema()


# In[15]:


from Functions.create_restricted_dfs_for_static_feature_creation import create_restricted_dfs_for_static_feature_creation

restricted_dfs = create_restricted_dfs_for_static_feature_creation(dfs, col_with_first_day_identifier="tweet_day_of_year", first_train_days=[37,38,39,])


# ### First Degree Engs
# 
# Let us now get the counts and flags for each engagement type (flag is true when count is bigger than one).

# In[16]:


from Functions.fe02_extract_engagements import extract_engagements

engs_1d = {}

for key in dfs:
    engs_1d[key] = extract_engagements(restricted_dfs[key])

if CALCULATE_STEPS:
    display(engs_1d[trn_key].limit(20).toPandas())
    display(engs_1d[trn_key].describe().toPandas())
    engs_1d[trn_key].columns


# ### Second Degree Engs
# 
# Let us not extract the second degree engagements, similarly as we have done foe the follows.

# In[17]:


from Functions.fe02_extract_engagements_2d import extract_engagements_2d

engs_2d = {}

for key in dfs:
    engs_2d[key] = extract_engagements_2d(engs_1d[key])
    
if CALCULATE_STEPS:
    display(engs_2d[trn_key].describe().toPandas())
engs_2d[trn_key].columns


# #### Engagement features Features
# 
# Then we use the graph of second degree follows to create the new features -- ```graph_engagee_<eng>_engager_2d``` and ```graph_engager_<eng>_engagee_2d``` where ```<eng>``` corresponds to all engagement types.

# In[18]:


from Functions.fe02_extract_graph_relations import extract_graph_relations

expected_new_features = {'graph_engaging_flag_like_from_engaged_1d', 'graph_engaging_flag_reply_from_engaged_1d', 'graph_engaging_flag_retweet_from_engaged_1d', 'graph_engaging_flag_quote_from_engaged_1d', 'graph_engaging_flag_react_from_engaged_1d', 'graph_engaging_count_like_from_engaged_1d', 'graph_engaging_count_reply_from_engaged_1d', 'graph_engaging_count_retweet_from_engaged_1d', 'graph_engaging_count_quote_from_engaged_1d', 'graph_engaging_count_react_from_engaged_1d', 'graph_engaged_flag_like_from_engaging_1d', 'graph_engaged_flag_reply_from_engaging_1d', 'graph_engaged_flag_retweet_from_engaging_1d', 'graph_engaged_flag_quote_from_engaging_1d', 'graph_engaged_flag_react_from_engaging_1d', 'graph_engaged_count_like_from_engaging_1d', 'graph_engaged_count_reply_from_engaging_1d', 'graph_engaged_count_retweet_from_engaging_1d', 'graph_engaged_count_quote_from_engaging_1d', 'graph_engaged_count_react_from_engaging_1d', 'graph_engaging_flag_like_from_engaged_2d', 'graph_engaging_flag_reply_from_engaged_2d', 'graph_engaging_flag_retweet_from_engaged_2d', 'graph_engaging_flag_quote_from_engaged_2d', 'graph_engaging_flag_react_from_engaged_2d', 'graph_engaging_count_like_from_engaged_2d', 'graph_engaging_count_reply_from_engaged_2d', 'graph_engaging_count_retweet_from_engaged_2d', 'graph_engaging_count_quote_from_engaged_2d', 'graph_engaging_count_react_from_engaged_2d', 'graph_engaged_flag_like_from_engaging_2d', 'graph_engaged_flag_reply_from_engaging_2d', 'graph_engaged_flag_retweet_from_engaging_2d', 'graph_engaged_flag_quote_from_engaging_2d', 'graph_engaged_flag_react_from_engaging_2d', 'graph_engaged_count_like_from_engaging_2d', 'graph_engaged_count_reply_from_engaging_2d', 'graph_engaged_count_retweet_from_engaging_2d', 'graph_engaged_count_quote_from_engaging_2d', 'graph_engaged_count_react_from_engaging_2d',}
eng_suffixes = ['like', 'reply', 'retweet', 'quote', 'react']
new_features = set()

for key in dfs:
    train_graph = None
    '''
    # From previous version, where train was not already included in train and val
    if ("test" in key) or ("val" in key):
        train_key = key.replace("test", "train").replace("val", "train")
        train_graph = graph[train_key]
    '''
    
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=expected_new_features,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
     
    for graph, dimension_name in zip([engs_1d, engs_2d], ["_1d", "_2d"]):
        for acting_user, acted_upon_user in zip(["engaging_user_id", "engaged_with_user_id"],
                                                    ["engaged_with_user_id", "engaging_user_id"]):
            graph_flag_columns = []
            new_feature_names = []
            
            # note that only flag_ will be used in the actual experiments, count_ is was implement when an
            # old approach was designed
            for flag_prefix in ["flag_", "count_"]:
                for eng_suf in eng_suffixes:
                    graph_flag_columns.append(flag_prefix + eng_suf)
                    new_feature = "graph_" + acting_user.split("_")[0] + "_" + flag_prefix + eng_suf +                         "_from_" + acted_upon_user.split("_")[0] + dimension_name
                    new_feature_names.append(new_feature)
                    new_features.add(new_feature)
                    
            if recreate_columns:
                dfs[key] = extract_graph_relations(df=dfs[key], graph=graph[key],
                                                           acting_user=acting_user,
                                                           acted_upon_user=acted_upon_user,
                                                           graph_acting_column="graph_engaging_user_id",
                                                           graph_acted_upon_column="graph_engaged_with_user_id",
                                                           new_feature_name=new_feature_names,
                                                           null_as_false=False,
                                                           graph_flag_column=graph_flag_columns,
                                                           train_graph=train_graph)


#assert expected_new_features == new_features         


if CALCULATE_STEPS:
    print("All columns: ", dfs[val_key].columns)
    print("New features: ", new_features)
    
    # http://prntscr.com/1v9or79
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[val_key].limit(5).toPandas())
        display(dfs[val_key].select(*new_features).describe().toPandas())


# ## Ratios
# 
# ### Ratio of Followers for Enaged vs Engaging
# 
# Here we implement the ratio of numbers of followers between the author and the engager (based on given features ```engaged_with_user_follower_count``` and ```engaging_user_follower_count```).

# In[19]:


from Functions.fe0X_ratio_of_two_features import ratio_of_two_features

ratio_feature_name = "ratio_engaged_to_engaging_follower_counts"

for key in dfs:
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=ratio_feature_name,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
     
    if recreate_columns:
        print(f"Now recreating the {ratio_feature_name} for {key}.")
        dfs[key] = ratio_of_two_features(df=dfs[key], col1="engaged_with_user_follower_count", 
                                         col2="engaging_user_follower_count", 
                                         ratio_name=ratio_feature_name, add_1=True) 


if CALCULATE_STEPS:
    # https://prnt.sc/1vooL4wuV5xD
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[val_key].select(ratio_feature_name).describe().toPandas())


# ### Ratio of Followings for Enaged vs Engaging
# 
# Here we implement the ratio of numbers of users the author and the engager follow (based on given features ```engaged_with_user_following_count``` and ```engaging_user_following_count```).

# In[20]:


from Functions.fe0X_ratio_of_two_features import ratio_of_two_features

ratio_feature_name = "ratio_engaged_to_engaging_following_counts"
col1="engaged_with_user_following_count"
col2="engaging_user_following_count"

for key in dfs:
    dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=ratio_feature_name,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
     
    if recreate_columns:
        print(f"Now recreating the {ratio_feature_name} for {key}.")
        dfs[key] = ratio_of_two_features(df=dfs[key], col1=col1, col2=col2, 
                                         ratio_name=ratio_feature_name, add_1=True) 


if CALCULATE_STEPS:
    # https://prnt.sc/POXUnH3cPQzG
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].select(ratio_feature_name).describe().toPandas())


# ## Export Files

# In[21]:


show_final_statistics(dfs=dfs, pandas_ref=pd, calculate_steps=CALCULATE_STEPS, 
                      keys_for_printing_statistics=(trn_key, val_key,), working_on_cluster=working_on_cluster)


# In[22]:


export_dataframes(dfs=dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                  files_to_be_exported=changed_dfs)


# In[23]:


print("Done!")

