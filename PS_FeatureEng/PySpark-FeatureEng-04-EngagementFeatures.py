#!/usr/bin/env python
# coding: utf-8

# # 04 Engagement History
# 
# Language of the tweet was one of the original features provided contained in the dataset. Partially inspired by the work of [FDC+20], we counted the number of past reactions (how many times the user had engaged and how many times they had not) of both the engaging user and the engaged user. We have these counts both for all tweets as well as for the specific language the target tweet was written in. Thus we created very many new features (number of languages in the dataset·2, for positive and negativeinteractions +4 for the language-independent counts), of which for each tweet prediction, six features would be selected: overall positive and negative engagement counts for both the author (suffix engaged) and the viewer (suffix engaging) as well as positive andnegative language-specific engagement counts for the viewer.

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
import pyspark.sql.types as t
from operator import add

from Functions.import_dataframes import import_dataframes
from Functions.export_dataframes import export_dataframes
from Functions.show_final_statistics import show_final_statistics
from Functions.check_if_columns_are_missing import check_if_columns_are_missing
from Functions.prepare_for_new_columns import prepare_for_new_columns


# In[2]:


# Other imports
import os
import string
from collections import Counter
import pandas as pd
import copy


# In[3]:


# If on the TU Wien's LBD cluster use yarn, otherwise use local
working_on_cluster = "adbs20" in os.path.abspath(os.getcwd())
master = "yarn" if working_on_cluster else "local"
memory = "120G" if working_on_cluster else "8G"
cores = 16 if working_on_cluster else 8
timeout = 7200 if working_on_cluster else 900

# Initialise Spark session
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_04_EngagementFeatures")     .master(master)     .config("spark.executor.memory", memory)     .config("spark.driver.memory", memory)     .config("spark.driver.maxResultSize", memory)     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", cores)     .config("spark.sql.broadcastTimeout", timeout)     .config('spark.sql.session.timeZone', 'UTC')     .config('spark.driver.extraJavaOptions', '-Duser.timezone=UTC')     .config('spark.executor.extraJavaOptions', '-Duser.timezone=UTC')     .getOrCreate()


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


# General setings
RECREATE_EVEN_IF_ALREADY_EXIST = False # if True, the code will force recreation of steps
CALCULATE_STEPS = False # calculate inbetween steps - provides more transparent progress, but might slow down the run
DEV = False # set to true to use smaller datasets
FEATURESET_EXPORT_PREFIX = "Engagement_" # import-export filename prefix for features created here
BACKUP_PREFIX = "Encoding_" # in case a file with this prefix was not generated yet, load it from files with this prefix

HDFS_DATAFOLDER = "Data" if working_on_cluster else os.path.join("..", "Data")
SAMPLING_TECHNIQUES = ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
SAMPLING_PERCENTAGES = ("1pct", "2pct", "5pct", "10pct")
TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"
VT_NAME = "val+test"

# note the distintion below in comparison to FE 00 and 01
IMPORT_DATASETS = (TRAIN_NAME, VAL_NAME, TEST_NAME, VT_NAME)


# In[6]:


dfs, changed_dfs = import_dataframes(spark, datasets=IMPORT_DATASETS,
       sampling_techniques=SAMPLING_TECHNIQUES,
       sampling_percentages=SAMPLING_PERCENTAGES,
       featureset_export_prefix=FEATURESET_EXPORT_PREFIX,
       backup_featureset_prefixes=BACKUP_PREFIX,
       recreate_even_if_already_exist=RECREATE_EVEN_IF_ALREADY_EXIST, 
       HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# Note that in the original challenge, test/val datasets did not contain target labels. Thus counting engagement labels for them would not have been possible. Therefore, we want to create proxies as follows:
# 
# * For val/test subsets we want to use the corresponding train subset for the training and the val/test subsets as they are for testing.
# * For training subsets, we want to use the first three days (or as specified in the argument
#     <calendar_days_to_be_used_for_train_training_sbs>) of the training subsets for the training and the later four days
#     (or as in <calendar_days_to_be_used_for_train_testing_sbs>) for testing (i.e. Thu, Fri, Sat -> Training; Sun, Mon,
#     Tue, Wed -> Testing).
#     
# Thus for val/test, we thus need to import the Encoding_ version of train dataset. 
# For train, we use the function below to split the train daset according to clendar days as described.

# In[7]:


training_for_test_and_val, _ = import_dataframes(spark, datasets=(TRAIN_NAME,),
       sampling_techniques=SAMPLING_TECHNIQUES,
       sampling_percentages=SAMPLING_PERCENTAGES,
       featureset_export_prefix="Encoding_", # featureset_export_prefix must be "Encoding_", cf. above
       backup_featureset_prefixes=None,
       recreate_even_if_already_exist=False, 
       HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# In[8]:


changed_dfs


# In[9]:


from Functions.create_corresponding_train_subset_mappings import create_corresponding_train_subset_mappings
from Functions.pyspark_df_shape import pyspark_df_shape

mapping_dfs, resulting_dfs = create_corresponding_train_subset_mappings(dfs=dfs,
                                                                      changed_dfs=changed_dfs,
                                                                      training_for_test_and_val=training_for_test_and_val,
                                                                      train_prefix=TRAIN_NAME,
                                                                      other_prefixes=(VT_NAME, VAL_NAME, TEST_NAME,),
                                                                      calendar_days_to_be_used_for_train_training_sbs=[37, 38, 39,  ],
                                                                      calendar_days_to_be_used_for_train_testing_sbs=[40, 41, 42, 43, ],
                                                                      calendar_days_col="tweet_day_of_year",
                                                                      fix_different_ratio=False)

if CALCULATE_STEPS:
    for key in dfs:
        print(f"{key}: {pyspark_df_shape(dfs[key], tpl=False)} mapped into {pyspark_df_shape(mapping_dfs[key],tpl=False)} and {pyspark_df_shape(resulting_dfs[key], tpl=False)}")


# In[10]:


if DEV:
    trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
    val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
else:
    trn_key = TRAIN_NAME
    val_key = VAL_NAME


# In[11]:


check_if_columns_are_missing(resulting_dfs, trn_key)

if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(resulting_dfs[trn_key].limit(4).toPandas())

print(trn_key, resulting_dfs[trn_key].columns)
resulting_dfs[trn_key].printSchema()


# ## Feature Engineering -- Engaging Features
# 
# Count the number of positive and negative engagements and authored tweets for all users. 

# In[12]:


from Functions.fe04_count_positive_and_negative_engagements import count_positive_and_negative_engagements
from Functions.fe0X_add_new_user_features import add_new_user_features

engagements = ['like', 'reply', 'retweet', 'quote', 'react']
engaging_posneg_counts = {}
engaged_posneg_counts = {}
trn_key_changed = False
val_key_changed = False
col_names_list = [] # list of new features
for user in ["engaging_", "engaged_with_"]:
    for posneg in ["all_", "positive_", "negative_"]:
        if posneg=="all_":
            col_names_list.append(user+"count_"+posneg+"tweets")
        else:
            for eng in engagements:
                col_names_list.append(user+"count_"+posneg+"tweets_"+eng)

print(col_names_list)
                
for key in dfs:
    #print(f"Checking whether columns for {key} need to be recreated.")
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    if recreate_columns:
        # also drop the corresponding columns for the mapping dfs
        mapping_dfs[key], _, _ = prepare_for_new_columns(df=mapping_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
        trn_key_changed = trn_key_changed or (key == trn_key)
        val_key_changed = val_key_changed or (key == val_key)
        engaging_posneg_counts[key] = count_positive_and_negative_engagements(mapping_dfs[key],
                                        group_column = 'engaging_user_id', 
                                        engagements = engagements,
                                        prefix = "engaging_") 
        engaged_posneg_counts[key] = count_positive_and_negative_engagements(mapping_dfs[key],
                                        group_column = 'engaged_with_user_id', 
                                        engagements = engagements,
                                        prefix = "engaged_with_")  
        
        resulting_dfs[key] = add_new_user_features(resulting_dfs[key], engaging_posneg_counts[key], 
                                                   df_user_id = "engaging_user_id", 
                                                   new_features_id = "engaging_user_id", 
                                                   default_value = 0) 
        resulting_dfs[key] = add_new_user_features(resulting_dfs[key], engaged_posneg_counts[key], 
                                                   df_user_id = "engaged_with_user_id", 
                                                   new_features_id = "engaged_with_user_id", 
                                                   default_value = 0)
        export_dataframes(dfs={key:resulting_dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, 
                          HDFS_datafolder=HDFS_DATAFOLDER, files_to_be_exported={key})
        
        
if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        if trn_key_changed:
            print(f"engaging_posneg_counts for {trn_key}")
            display(engaging_posneg_counts[trn_key].limit(5).toPandas())
            display(engaging_posneg_counts[trn_key].describe().toPandas())
        
        print(f"resulting_dfs for {trn_key}")
        display(resulting_dfs[trn_key].select(["tweet_id" , "engaged_with_user_id"]+col_names_list).limit(5).toPandas())
        display(resulting_dfs[trn_key].select(["tweet_id" , "engaged_with_user_id"]+col_names_list).describe().toPandas())
        
        if val_key_changed:
            print(f"engaging_posneg_counts for {val_key}")
            display(engaging_posneg_counts[val_key].limit(5).toPandas())
            display(engaging_posneg_counts[val_key].describe().toPandas())
        
        print(f"resulting_dfs for {val_key}")
        display(resulting_dfs[val_key].select(["tweet_id" , "engaged_with_user_id"]+col_names_list).limit(5).toPandas())
        display(resulting_dfs[val_key].select(["tweet_id" , "engaged_with_user_id"]+col_names_list).describe().toPandas())


# ### Hashtag, Domain, and Link Engagments
# 
# Unlike in fe03, where we counted how many hastags, domains, and links a user has seen in the previous hours, here we want to see what ratio of the specific tweets, hastags, links were engaged with in the entire train dataset. Whereas the features in 03 represent interest, this represents a proxy for general tweet element popularity.

# In[13]:


from Functions.fe04_count_positive_and_negative_tweet_elements import count_positive_and_negative_tweet_elements

elements_counts = {}
base_features = ["hashtags", "present_links", "present_domains"]
trn_key_changed = False
val_key_changed = False
elements_col_names_list = [] # list of new features
for base in base_features:
    prefix = base.replace("present_", "")
    for posneg in ["all_", "positive_", "negative_"]:
        if posneg=="all_":
            elements_col_names_list.append(prefix+"_count_"+posneg+"tweets")
        else:
            for eng in engagements:
                elements_col_names_list.append(prefix+"_count_"+posneg+"tweets_"+eng)
                
print("Now creating:", elements_col_names_list)
                
for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=elements_col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        # also drop the corresponding columns for the mapping dfs, thus recreate_even_if_already_exists=True
        mapping_dfs[key], _, _ = prepare_for_new_columns(df=mapping_dfs[key],
                                                                      recreate_even_if_already_exists=True,
                                                                      col_name=None,
                                                                      col_names_list=elements_col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
        trn_key_changed = trn_key_changed or (key == trn_key)
        val_key_changed = val_key_changed or (key == val_key)
        
        elements_counts[key] = {}
        
        for bf in base_features:
            elements_counts[key][bf] = count_positive_and_negative_tweet_elements(mapping_dfs[key], column_name = bf)
            resulting_dfs[key] = add_new_user_features(resulting_dfs[key], elements_counts[key][bf], 
                                                   df_user_id = "tweet_id", 
                                                   new_features_id = "tweet_id", 
                                                   default_value = 0)
            export_dataframes(dfs={key:resulting_dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, 
                          HDFS_datafolder=HDFS_DATAFOLDER, files_to_be_exported={key})


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        if trn_key_changed:
            print(f"elements_counts for {trn_key} and hashtags.")
            display(elements_counts[trn_key]["hashtags"].limit(5).toPandas())
            display(elements_counts[trn_key]["hashtags"].describe().toPandas())
        
        print(f"resulting_dfs for {trn_key}.")
        display(resulting_dfs[trn_key].select(["tweet_id"]+elements_col_names_list).limit(5).toPandas())
        display(resulting_dfs[trn_key].select(["tweet_id"]+elements_col_names_list).describe().toPandas())
        
        if val_key_changed:
            print(f"elements_counts for {val_key} and hashtags.")
            display(elements_counts[val_key]["hashtags"].limit(5).toPandas())
            display(elements_counts[val_key]["hashtags"].describe().toPandas())
        
        print(f"resulting_dfs for {val_key}.")
        display(resulting_dfs[val_key].select(["tweet_id"]+elements_col_names_list).limit(5).toPandas())
        display(resulting_dfs[val_key].select(["tweet_id"]+elements_col_names_list).describe().toPandas())


# ### User-Proxy Engagement
# 
# The reason why mean for all element counts above is zero is because it seems like no tweet from the train dataset is in the val dataset, so working with the last days of train to get the counts for val does not work. So, instead, we will join on engaged_with_user_id instead of tweet_id (the assumption here is that if someone used popular hashtags/links in the past, they will keep on doing so in the future.

# In[14]:


from Functions.fe04_user_proxy_count_positive_and_negative_tweet_elements import user_proxy_count_positive_and_negative_tweet_elements

elements_user_proxy_counts = {}
base_features = ["hashtags", "present_links", "present_domains"]
trn_key_changed = False
val_key_changed = False
user_proxy_col_names_list = [] # list of new features
for base in base_features:
    prefix = base.replace("present_", "")
    for posneg in ["all_", "positive_", "negative_"]:
        if posneg=="all_":
            user_proxy_col_names_list.append(prefix+"_user_proxy_count_"+posneg+"tweets")
        else:
            for eng in engagements:
                user_proxy_col_names_list.append(prefix+"_user_proxy_count_"+posneg+"tweets_"+eng)
                
print("Now creating:", user_proxy_col_names_list)
                
for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=user_proxy_col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        # also drop the corresponding columns for the mapping dfs, thus recreate_even_if_already_exists=True
        mapping_dfs[key], _, _ = prepare_for_new_columns(df=mapping_dfs[key],
                                                                      recreate_even_if_already_exists=True,
                                                                      col_name=None,
                                                                      col_names_list=user_proxy_col_names_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
        trn_key_changed = trn_key_changed or (key == trn_key)
        val_key_changed = val_key_changed or (key == val_key)
        
        elements_user_proxy_counts[key] = {}
        
        for bf in base_features:
            elements_user_proxy_counts[key][bf] = user_proxy_count_positive_and_negative_tweet_elements(mapping_dfs[key], column_name = bf)
            resulting_dfs[key] = add_new_user_features(resulting_dfs[key], elements_user_proxy_counts[key][bf], 
                                                   df_user_id = "engaged_with_user_id", 
                                                   new_features_id = "engaged_with_user_id", 
                                                   default_value = 0)
            export_dataframes(dfs={key:resulting_dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, 
                          HDFS_datafolder=HDFS_DATAFOLDER, files_to_be_exported={key})


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        if trn_key_changed:
            print(f"elements_user_proxy_counts for {trn_key} and hashtags.")
            display(elements_user_proxy_counts[trn_key]["hashtags"].limit(5).toPandas())
            display(elements_user_proxy_counts[trn_key]["hashtags"].describe().toPandas())
        
        print(f"resulting_dfs for {trn_key}.")
        display(resulting_dfs[trn_key].select(["engaged_with_user_id"]+user_proxy_col_names_list).limit(5).toPandas())
        display(resulting_dfs[trn_key].select(["engaged_with_user_id"]+user_proxy_col_names_list).describe().toPandas())
        
        if val_key_changed:
            print(f"elements_user_proxy_counts for {val_key} and hashtags.")
            display(elements_user_proxy_counts[val_key]["hashtags"].limit(5).toPandas())
            display(elements_user_proxy_counts[val_key]["hashtags"].describe().toPandas())
        
        print(f"resulting_dfs for {val_key}.")
        display(resulting_dfs[val_key].select(["engaged_with_user_id"]+user_proxy_col_names_list).limit(5).toPandas())
        display(resulting_dfs[val_key].select(["engaged_with_user_id"]+user_proxy_col_names_list).describe().toPandas())


# ### Engagement Ratios
# 
# Let us now calculate ratios based on counts calculated above.
# 
# #### Positive and Negative Interactions Ratios:
# 
# We calculate positive and negative interaction reation both for the viewer:
# ```
# 'engaging_count_positive_tweets_like', 
# 'engaging_count_positive_tweets_reply', 
# 'engaging_count_positive_tweets_retweet', 
# 'engaging_count_positive_tweets_quote', 
# 'engaging_count_positive_tweets_react', 
# 'engaging_count_negative_tweets_like', 
# 'engaging_count_negative_tweets_reply', 
# 'engaging_count_negative_tweets_retweet', 
# 'engaging_count_negative_tweets_quote', 
# 'engaging_count_negative_tweets_react', 
# 	/ 'engaging_count_all_tweets'
# ```
# 
# and for the author:
# 
# ```
# 'engaged_with_count_positive_tweets_like', 
# 'engaged_with_count_positive_tweets_reply', 
# 'engaged_with_count_positive_tweets_retweet', 
# 'engaged_with_count_positive_tweets_quote', 
# 'engaged_with_count_positive_tweets_react', 
# 'engaged_with_count_negative_tweets_like', 
# 'engaged_with_count_negative_tweets_reply', 
# 'engaged_with_count_negative_tweets_retweet', 
# 'engaged_with_count_negative_tweets_quote', 
# 'engaged_with_count_negative_tweets_react'
# 	/ 'engaged_with_count_all_tweets'
# ```
# 
# In addition to that, we also want to calculate the ratios for all of the observered tweet elements (hashtags, links, domains):
# 
# ```
# 'hashtags_count_positive_tweets_like', 
# 'hashtags_count_positive_tweets_reply', 
# 'hashtags_count_positive_tweets_retweet', 
# 'hashtags_count_positive_tweets_quote', 
# 'hashtags_count_positive_tweets_react', 
# 'hashtags_count_negative_tweets_like', 
# 'hashtags_count_negative_tweets_reply', 
# 'hashtags_count_negative_tweets_retweet', 
# 'hashtags_count_negative_tweets_quote', 
# 'hashtags_count_negative_tweets_react', 
# 	/ 'hashtags_count_all_tweets', 
# 'links_count_positive_tweets_like', 
# 'links_count_positive_tweets_reply', 
# 'links_count_positive_tweets_retweet', 
# 'links_count_positive_tweets_quote', 
# 'links_count_positive_tweets_react', 
# 'links_count_negative_tweets_like', 
# 'links_count_negative_tweets_reply', 
# 'links_count_negative_tweets_retweet', 
# 'links_count_negative_tweets_quote', 
# 'links_count_negative_tweets_react', 
# 	/ 'links_count_all_tweets'
# 'domains_count_positive_tweets_like', 
# 'domains_count_positive_tweets_reply', 
# 'domains_count_positive_tweets_retweet', 
# 'domains_count_positive_tweets_quote', 
# 'domains_count_positive_tweets_react', 
# 'domains_count_negative_tweets_like', 
# 'domains_count_negative_tweets_reply', 
# 'domains_count_negative_tweets_retweet', 
# 'domains_count_negative_tweets_quote', 
# 'domains_count_negative_tweets_react', 
# 	/ 'domains_count_all_tweets'
# ```
# 
# All of this must be done both for ```_count_``` as well as for ```_user_proxy_count_```!

# In[15]:


from Functions.fe0X_ratio_of_two_features import ratio_of_two_features


prefix = ["engaging", "engaged_with", "hashtags", "links", "domains"]
count_part = ["count", "user_proxy_count"]
posneg_part = ["positive", "negative"]
tweets_part = ["tweets"]
eng_part = ["like", "reply", "retweet", "quote", "react"]
divisor_suffix = "_all_tweets"

eng_ratios_list = [] # list of new features
dividends_list = []
divisors_list = []

for us in prefix:
    for co in count_part:
        if ("engag" in us) and ("user_proxy" in co):
            continue  # user_proxy_count only for tweet elements features, not for tweet engagement features
        for pn in posneg_part:
            for tw in tweets_part:
                for en in eng_part:
                    dividend = us + "_" + co + "_" + pn + "_" + tw + "_" + en
                    dividends_list.append(dividend)
                    divisor = us + "_" + co + "_all_tweets" # the name of the divisor depends only on the first two parts
                    divisors_list.append(divisor)
                    eng_ratio_feature = "ratio_all_to_"+dividend
                    eng_ratios_list.append(eng_ratio_feature)
                    
                
print("Now creating:", eng_ratios_list)

for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=eng_ratios_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        # also drop the corresponding columns for the mapping dfs, thus recreate_even_if_already_exists=True
        mapping_dfs[key], _, _ = prepare_for_new_columns(df=mapping_dfs[key],
                                                                      recreate_even_if_already_exists=True,
                                                                      col_name=None,
                                                                      col_names_list=eng_ratios_list,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
        for feature_name, col1, col2 in zip(eng_ratios_list, dividends_list, divisors_list):
            if ("user_proxy_count" in col2) and ("engag" in col2):
                continue  # no proxy_user_counts for the prefixes engaged_ and engaging_ -- they are already user-based!
                
            resulting_dfs[key] = ratio_of_two_features(df=resulting_dfs[key], col1=col1, col2=col2, 
                                                       ratio_name=feature_name, add_1=True)


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(f"resulting_dfs for {trn_key}")
        display(resulting_dfs[trn_key].select(["tweet_id", "engaged_with_user_id"]+eng_ratios_list).limit(5).toPandas())
        display(resulting_dfs[trn_key].select(["tweet_id", "engaged_with_user_id"]+eng_ratios_list).describe().toPandas())
        print(f"resulting_dfs for {val_key}")
        display(resulting_dfs[val_key].select(["tweet_id", "engaged_with_user_id"]+eng_ratios_list).limit(5).toPandas())
        display(resulting_dfs[val_key].select(["tweet_id", "engaged_with_user_id"]+eng_ratios_list).describe().toPandas())


# ## Language
# 
# We now want to see the preferred languages of users.

# ### Identify Language
# 
# First we want to identify languages of each of the tweets.

# In[16]:


if not working_on_cluster:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            display(dfs[trn_key].select(["text", "language"]).limit(500).toPandas())


# In[17]:


if not working_on_cluster:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].groupBy(["language"]).count().sort("count", ascending = False).toPandas())


# There are too many languages for us to decode which languge names. Moreover, since we focus in context-based predictions, this is also not decisive for us. So we proceed with these codes. We see that top five most represented languages in train dataset are:
# 
#  1. English ECED8A16BE2A5E8871FD55F4842F16B1
#  1. Japanese 22C448FF81263D4BAF2A176145EE9EAD
#  1. Spanish 06D61DCBBE938971E1EA0C38BD9B5446
#  1. Portuguese ECED8A16BE2A5E8871FD55F4842F16B
#  1. **Unknown B9175601E87101A984A50F8A62A1C374**
#  1. Turkish 4DC22C3F31C5C43721E6B5815A595ED6
#  1. Arabic 167115458A0DBDFF7E9C0C53A83BAC9B
#  1. Korean 125C57F4FA6D4E110983FB11B52EFD4E
#  1. Thai 022EC308651FACB02794A8147AEE1B78
#  1. French FA3F382BC409C271E3D6EAF8BE4648DD
#  1. Indonesian 975B38F44D65EE42A547283787FF5A21

# ### Look Whether the Language Is Unknown
# 
# We want to add a boolean column which states whether the language of the tweet is B9175601E87101A984A50F8A62A1C374, i.e. whether it could not be determined.

# In[18]:


for key in dfs: 
    resulting_dfs[key] = resulting_dfs[key].withColumn("language_unknown", f.when(f.col("language") == "B9175601E87101A984A50F8A62A1C374", True).otherwise(False))
    mapping_dfs[key] = mapping_dfs[key].withColumn("language_unknown", f.when(f.col("language") == "B9175601E87101A984A50F8A62A1C374", True).otherwise(False))

    
if CALCULATE_STEPS:
    print("unknown language tweets in train", resulting_dfs[trn_key].filter(f.col("language_unknown")).count())
    print("other tweets in train", resulting_dfs[trn_key].filter(f.col("language_unknown") == False).count())
    print("unknown language tweets in val", resulting_dfs[val_key].filter(f.col("language_unknown")).count())
    print("other tweets in val", resulting_dfs[val_key].filter(f.col("language_unknown") == False).count())


# ## User Language Preferences
# 
# 

# ### Boolean features
# 
# First we want to create boolean features, corresponding to more static variables: has the user ever interacted with some language. Originally we wanted to look at this as follows:
# 
#  * full train (train)
#  * full train+val (for test+val)
#  * full train+test+val (for test+val)
#  
#  For that purose, we used the following code:

# In[19]:


''' 
from Functions.create_cumulative_dfs_for_static_feature_creation import create_cumulative_dfs_for_static_feature_creation

cumulative_dfs = create_cumulative_dfs_for_static_feature_creation(dfs)
cumulative_dfs.keys()
'''


# However, after later consultation, we decided not to do this and to use mapped_dfs instead of cumulative_dfs.

# ### Language Flags
# 
# Now we want to see whether the user has in the past seen tweets in the given language. We first add a column with all languages the user has seen, and then we add a boolean column which is true if the language of the tweet is in the list of languages the user has seen.

# In[20]:


seen_languages = {}

# Remember that cumulative_dfs.keys() == dfs.keys()
for key in mapping_dfs:
    seen_languages[key] = mapping_dfs[key].groupBy("engaging_user_id").agg(f.collect_set("language").alias("seen_languages"))
    
if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(seen_languages[val_key].limit(4).toPandas())


# Add the seen_languages column to the dataframe.

# In[21]:


for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name="seen_languages",
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        old_count = resulting_dfs[key].count()
        old_cols_no = len(resulting_dfs[key].columns)
        resulting_dfs[key] = resulting_dfs[key].join(seen_languages[key][["engaging_user_id", "seen_languages"]], on = "engaging_user_id", how="left")
        assert resulting_dfs[key].count() == old_count
        assert len(resulting_dfs[key].columns) == old_cols_no + 1


# ### Identify Which Language the User Has Written and Reacted In
# 
# The columns <seen_language> and <authored_langauge> contain for each language (in <language>) a dict with users as keys and values as counts of times a tweet in that language was seen or authored.

# In[22]:


from Functions.fe0X_get_column_value_counts import get_column_value_counts

seen_languages = {}
authored_languages = {}

for key in dfs:
    seen_languages[key] = get_column_value_counts(mapping_dfs[key], 
                                                  column_to_count_values = 'engaging_user_id', 
                                                  group_column = "language", 
                                                  count_alias = "seen_languages_dict")
    authored_languages[key] = get_column_value_counts(mapping_dfs[key], 
                                                      column_to_count_values = 'engaged_with_user_id', 
                                                      group_column = "language", 
                                                      count_alias = "authored_languages_dict") 


if CALCULATE_STEPS:
    # https://prnt.sc/CIfWIFUttCKn
    display(seen_languages[trn_key].limit(5).toPandas())
    display(authored_languages[trn_key].limit(5).toPandas())


# Now, we join back the languages.

# In[23]:


from Functions.fe0X_add_new_user_features import add_new_user_features  


for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=["seen_languages_dict", "authored_languages_dict"],
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        # the bug causing the wrong number of instances was here - I had mapping_dfs as the first argument by mistake
        resulting_dfs[key] = add_new_user_features(resulting_dfs[key], seen_languages[key], 
                                               df_user_id = "engaging_user_id", 
                                               new_features_id = "engaging_user_id", default_value = dict()) 
        resulting_dfs[key] = add_new_user_features(resulting_dfs[key], authored_languages[key], 
                                               df_user_id = "engaged_with_user_id", 
                                               new_features_id = "engaged_with_user_id", default_value = dict())    
        
if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(resulting_dfs[trn_key][["seen_languages_dict", "authored_languages_dict"]].limit(5).toPandas())
        print(trn_key, resulting_dfs[trn_key].filter(f.col("seen_languages_dict").isNotNull()).count(), 
              resulting_dfs[trn_key].filter(f.col("authored_languages_dict").isNotNull()).count(),
              pyspark_df_shape(resulting_dfs[trn_key]))


# Now let us create columns with just the counts for the given language from these dicts.

# In[24]:


from Functions.fe04_tweets_in_this_language_count import tweets_in_this_language_count  


for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=["this_language_seen_count", "this_language_authored_count"],
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
    
    if recreate_columns:
        resulting_dfs[key] = tweets_in_this_language_count(df=resulting_dfs[key], 
                                                           new_col_name="this_language_seen_count", 
                                                           language_dict_col="seen_languages_dict")
        resulting_dfs[key] = tweets_in_this_language_count(df=resulting_dfs[key], 
                                                           new_col_name="this_language_authored_count", 
                                                           language_dict_col="authored_languages_dict")
        
if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(resulting_dfs[trn_key][["this_language_seen_count", "this_language_authored_count"]].limit(5).toPandas())
        print(trn_key, pyspark_df_shape(resulting_dfs[trn_key]))


# #### Ratio of Seen Tweets in the Given Language to all Seen Tweets.
# 
# Here we implement the ratio of the number all of tweets the engager has seen to the number of tweets they saw just in the relevant language.

# In[25]:


from Functions.fe0X_ratio_of_two_features import ratio_of_two_features

ratio_feature_name = "ratio_seen_tweets_in_this_langauge_to_total_seen_tweets"
col1="this_language_seen_count"
col2="engaging_count_all_tweets"

for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=ratio_feature_name,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
     
    if recreate_columns:
        print(f"Now recreating the {ratio_feature_name} for {key}.")
        resulting_dfs[key] = ratio_of_two_features(df=resulting_dfs[key], col1=col1, col2=col2, 
                                         ratio_name=ratio_feature_name, add_1=True) 


if CALCULATE_STEPS:
    # TODO: screenshot link
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(resulting_dfs[trn_key].select(ratio_feature_name).describe().toPandas())


# #### Ratio of Authored Tweets in the Given Language to all Authored Tweets.
# 
# Here we implement the ratio of the number all of tweets the engaged-with user has authroed to the number of tweets they  authored just in the relevant language.

# In[26]:


from Functions.fe0X_ratio_of_two_features import ratio_of_two_features

ratio_feature_name = "ratio_authored_tweets_in_this_langauge_to_total_authored_tweets"
col1="this_language_authored_count"
col2="engaged_with_count_all_tweets"

for key in dfs:
    resulting_dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=resulting_dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=ratio_feature_name,
                                                                      col_names_list=None,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=True)
     
    if recreate_columns:
        print(f"Now recreating the {ratio_feature_name} for {key}.")
        resulting_dfs[key] = ratio_of_two_features(df=resulting_dfs[key], col1=col1, col2=col2, 
                                         ratio_name=ratio_feature_name, add_1=True) 


if CALCULATE_STEPS:
    # TODO: screenshot link
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(resulting_dfs[trn_key].select(ratio_feature_name).describe().toPandas())


# # Export Dataframes

# In[27]:


show_final_statistics(dfs=resulting_dfs, pandas_ref=pd, calculate_steps=CALCULATE_STEPS, 
                      keys_for_printing_statistics=(trn_key, val_key,), working_on_cluster=working_on_cluster)


# In[28]:


show_final_statistics(dfs=mapping_dfs, pandas_ref=pd, calculate_steps=CALCULATE_STEPS, 
                      keys_for_printing_statistics=(trn_key, val_key,), working_on_cluster=working_on_cluster)


# In[29]:


export_dataframes(dfs=resulting_dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                  files_to_be_exported=changed_dfs)


# In[30]:


print("Done!")

