#!/usr/bin/env python
# coding: utf-8

# # 03 Time Features
# 
# The main approach to working with time-based column is to use the window approach as described here: https://stackoverflow.com/a/33226511.

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
import pyspark.sql.types as t
from operator import add

from Functions.import_dataframes import import_dataframes
from Functions.export_dataframes import export_dataframes
from Functions.show_final_statistics import show_final_statistics
from Functions.check_if_columns_are_missing import check_if_columns_are_missing
from Functions.prepare_for_new_columns import prepare_for_new_columns


# In[2]:


# Other imports
import os
import string
from collections import Counter
import pandas as pd
import copy
from datetime import datetime


# In[3]:


# If on the TU Wien's LBD cluster use yarn, otherwise use local
working_on_cluster = "adbs20" in os.path.abspath(os.getcwd())
master = "yarn" if working_on_cluster else "local"
memory = "120G" if working_on_cluster else "8G"
cores = 16 if working_on_cluster else 8
timeout = 7200 if working_on_cluster else 900


# Initialise Spark session
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_03_TimeFeatures")     .master(master)     .config("spark.executor.memory", memory)     .config("spark.driver.memory", memory)     .config("spark.driver.maxResultSize", memory)     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", cores)     .config("spark.sql.broadcastTimeout", timeout)     .config('spark.sql.session.timeZone', 'UTC')     .config('spark.driver.extraJavaOptions', '-Duser.timezone=UTC')     .config('spark.executor.extraJavaOptions', '-Duser.timezone=UTC')     .getOrCreate()


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


# General setings
RECREATE_EVEN_IF_ALREADY_EXIST = False # if True, the code will force recreation of steps
CALCULATE_STEPS = False # calculate inbetween steps - provides more transparent progress, but might slow down the run
DEV = False # set to true to use smaller datasets
FEATURESET_EXPORT_PREFIX = "Time_" # import-export filename prefix for features created here
BACKUP_PREFIX = "Encoding_" # in case a file with this prefix was not generated yet, load it from files with this prefix

HDFS_DATAFOLDER = "Data" if working_on_cluster else os.path.join("..", "Data")
SAMPLING_TECHNIQUES = ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
SAMPLING_PERCENTAGES = ("1pct", "2pct", "5pct", "10pct")
TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"
VT_NAME = "val+test"
# note the distintion below in comparison to FE 00 and 01
IMPORT_DATASETS = (TRAIN_NAME, VAL_NAME, TEST_NAME, VT_NAME)


# In[6]:


dfs, changed_dfs = import_dataframes(spark, datasets=IMPORT_DATASETS,
       sampling_techniques=SAMPLING_TECHNIQUES,
       sampling_percentages=SAMPLING_PERCENTAGES,
       featureset_export_prefix=FEATURESET_EXPORT_PREFIX,
       backup_featureset_prefixes=BACKUP_PREFIX,
       recreate_even_if_already_exist=RECREATE_EVEN_IF_ALREADY_EXIST, 
       HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# In[7]:


if DEV:
    trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
    val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
else:
    trn_key = TRAIN_NAME
    val_key = VAL_NAME


# In[8]:


check_if_columns_are_missing(dfs, trn_key)

if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].limit(4).toPandas())

print(trn_key, dfs[trn_key].columns)


# ## Dataset Preparation
# 
# ### Appended DFs
# 
# We want to look in the past 30 minutes, 60 minutes, and 120 minutes, as well as 12 hours, 24 hours, and 48 hours. Therefore, for test and val we want to include the last 48h of train, as it precedes the beginning time of test and val.

# In[9]:


from Functions.create_test_val_ds_with_days_from_train import create_test_val_ds_with_days_from_train
from Functions.pyspark_df_shape import pyspark_df_shape

appended_dfs = create_test_val_ds_with_days_from_train(spark=spark, dfs=dfs,
                                                       changed_dfs=changed_dfs,
                                                       export_prefix=FEATURESET_EXPORT_PREFIX,
                                                       hdfs_datafolder=HDFS_DATAFOLDER)

if CALCULATE_STEPS:
    for key in changed_dfs:
        print(f"{key}: {pyspark_df_shape(dfs[key])} -> {pyspark_df_shape(appended_dfs[key])}")


# In[10]:


# we simply export the whole appended_dfs and then filter out the train days later

for key in dfs:
    dfs[key] = appended_dfs[key].sort("tweet_timestamp")


# ## Feature Engineering -- Non User-Specific Features
# 
# Note that of all featuresets, this one is most time- and memory-intensive. In fact, the speed at which a task is performed decreases with every iteration, as seen below:
# 
# ```
# Now at, 03.09.2022 01:21:53, creating ['hashtags_frequency_24h', 'links_frequency_24h', 'domains_frequency_24h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now at, 03.09.2022 01:35:17, creating ['hashtags_frequency_48h', 'links_frequency_48h', 'domains_frequency_48h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now at, 03.09.2022 02:27:41, creating ['hashtags_frequency', 'links_frequency', 'domains_frequency'] for val+test_random_sample_1pct.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now at, 03.09.2022 02:48:13, creating ['hashtags_frequency_05h', 'links_frequency_05h', 'domains_frequency_05h'] for val+test_random_sample_1pct.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now at, 03.09.2022 03:58:03, creating ['hashtags_frequency_1h', 'links_frequency_1h', 'domains_frequency_1h'] for val+test_random_sample_1pct.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now exporting: val+test_random_sample_1pct 261263
# Time_val+test_random_sample_1pct.parquet done and saved.
# Now at, 03.09.2022 06:42:47, creating ['hashtags_frequency_2h', 'links_frequency_2h', 'domains_frequency_2h'] for val+test_random_sample_1pct.
# Now exporting: val+test_random_sample_1pct 261263
# ```
# 
# and 
# 
# ```
# Now at, 04.09.2022 00:56:49, creating ['user_hashtags_frequency_05h', 'user_links_frequency_05h', 'user_domains_frequency_05h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now at, 04.09.2022 01:09:30, creating ['user_hashtags_frequency_1h', 'user_links_frequency_1h', 'user_domains_frequency_1h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now at, 04.09.2022 01:59:42, creating ['user_hashtags_frequency_2h', 'user_links_frequency_2h', 'user_domains_frequency_2h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now exporting: test_random_sample_1pct 131207
# Time_test_random_sample_1pct.parquet done and saved.
# Now at, 04.09.2022 04:05:34, creating ['user_hashtags_frequency_12h', 'user_links_frequency_12h', 'user_domains_frequency_12h'] for test_random_sample_1pct.
# Now exporting: test_random_sample_1pct 131207
# ```
# 
# For this reasn, it is strongly recommended to convert this notebook into a Python scrip and then to use a task scheduler to repeatedly run it over and over again, as elaborated here:
#  * https://towardsdatascience.com/how-to-schedule-python-scripts-with-cron-the-only-guide-youll-ever-need-deea2df63b4e
#  * https://crontab.guru/every-2-hours
#  * https://stackoverflow.com/questions/65971461/jupyter-notebook-schedule-automatically
#  * https://stackoverflow.com/questions/7195503/setting-up-a-cron-job-in-windows
# 
# 
# ### Hastags, Domains, and Links Frequencies

# #### General Frequency
# 
#  - hashtags\_frequency: sum of numbers of appearances for each of the hashtags in the last 0.5, 1, 2, 12, 24, and 48 hours.
#  - links\_frequency: sum of numbers of appearances for each of the links in the tweet in the last 0.5, 1, 2, 12, 24, and 48 hours.
#  - domains\_frequency: sum of numbers of appearances for each of the domains in the tweet in the last 0.5, 1, 2, 12, 24, and 48 hours.

# In[11]:


from Functions.remove_duplicate_columns import remove_duplicate_columns

dfs[trn_key] = remove_duplicate_columns(dfs[trn_key])


# In[12]:


from Functions.fe03_get_frequency import get_frequency

base_features = ["hashtags", "present_links", "present_domains"]
new_features = ["hashtags_frequency", "links_frequency", "domains_frequency"]
hours_values = [None, 0.5, 1, 2, 12, 24, 48]
hours_names = ["", "_05h", "_1h", "_2h", "_12h", "_24h", "_48h"]

for key in dfs:
    for hv, hn in zip(hours_values, hours_names):
        next_features = [base+hn for base in new_features]
        # print(f"Checking whether columns {next_features} for {key} need to be recreated.")
        dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=next_features,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
    
        if recreate_columns:
            print(f"Now at, {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}, creating {next_features} for {key}.")
            for c_key in base_features:
                dfs[key] = get_frequency(dfs[key], column_name = c_key, group_by_cols=["item"], prior_hours=hv, new_col_prefix="", new_col_suffix=hn)
                export_dataframes(dfs={key:dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                      files_to_be_exported={key})




if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key][["tweet_id", "engaging_user_id", "hashtags", "hashtags_frequency", "hashtags_frequency_48h", "present_links", "links_frequency", "links_frequency_48h", "present_domains", "domains_frequency", "domains_frequency_48h"]].limit(8).toPandas())
        display(dfs[trn_key][["tweet_id", "engaging_user_id", "hashtags", "hashtags_frequency", "hashtags_frequency_48h", "present_links", "links_frequency", "links_frequency_48h", "present_domains", "domains_frequency", "domains_frequency_48h"]].describe().toPandas())


# #### User-Based Frequency
# 
# 
# Now we want to see the frequencies just for the engaging user in question, to see whether they were interested in a particular topic.
#  - user\_hashtags\_frequency: sum of numbers of appearances for each of the hashtags in the last 0.5, 1, 2, 12, 24, and 48 hours.
#  - user\_links\_frequency: sum of numbers of appearances for each of the links in the tweet in the last 0.5, 1, 2, 12, 24, and 48 hours.
#  - user\_domains\_frequency: sum of numbers of appearances for each of the domains in the tweet in the last 0.5, 1, 2, 12, 24, and 48 hours.

# In[ ]:


from Functions.fe03_get_frequency import get_frequency

base_features = ["hashtags", "present_links", "present_domains"]
new_features = ["user_hashtags_frequency", "user_links_frequency", "user_domains_frequency"]
hours_values = [None, 0.5, 1, 2, 12, 24, 48]
hours_names = ["", "_05h", "_1h", "_2h", "_12h", "_24h", "_48h"]

for key in dfs:
    for hv, hn in zip(hours_values, hours_names):
        next_features = [base+hn for base in new_features]
        #print(f"Checking whether columns {next_features} for {key} need to be recreated.")
        dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                      recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                      col_name=None,
                                                                      col_names_list=next_features,
                                                                      changed_dfs_set=changed_dfs,
                                                                      key=key,
                                                                      print_resulting_decision=False)
    
        if recreate_columns:
            print(f"Now at, {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}, creating {next_features} for {key}.")
            for c_key in base_features:
                dfs[key] = get_frequency(dfs[key], column_name = c_key, group_by_cols=["engaging_user_id", "item"], prior_hours=hv, new_col_prefix="user_", new_col_suffix=hn)
                export_dataframes(dfs={key:dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                      files_to_be_exported={key})




if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key][["tweet_id", "engaging_user_id", "hashtags", "hashtags_frequency", "hashtags_frequency_48h", "present_links", "links_frequency", "links_frequency_48h", "present_domains", "domains_frequency", "domains_frequency_48h"]].limit(8).toPandas())
        display(dfs[trn_key][["tweet_id", "engaging_user_id", "hashtags", "hashtags_frequency", "hashtags_frequency_48h", "present_links", "links_frequency", "links_frequency_48h", "present_domains", "domains_frequency", "domains_frequency_48h"]].describe().toPandas())


# In[ ]:


all_new_columns = ['hashtags_frequency',
 'links_frequency',
 'domains_frequency',
 'hashtags_frequency_05h',
 'links_frequency_05h',
 'domains_frequency_05h',
 'hashtags_frequency_1h',
 'links_frequency_1h',
 'domains_frequency_1h',
 'hashtags_frequency_2h',
 'links_frequency_2h',
 'domains_frequency_2h',
 'hashtags_frequency_12h',
 'links_frequency_12h',
 'domains_frequency_12h',
 'hashtags_frequency_24h',
 'links_frequency_24h',
 'domains_frequency_24h',
 'hashtags_frequency_48h',
 'links_frequency_48h',
 'domains_frequency_48h',
 'user_hashtags_frequency',
 'user_links_frequency',
 'user_domains_frequency',
 'user_hashtags_frequency_05h',
 'user_links_frequency_05h',
 'user_domains_frequency_05h',
 'user_hashtags_frequency_1h',
 'user_links_frequency_1h',
 'user_domains_frequency_1h',
 'user_hashtags_frequency_2h',
 'user_links_frequency_2h',
 'user_domains_frequency_2h',
 'user_hashtags_frequency_12h',
 'user_links_frequency_12h',
 'user_domains_frequency_12h',
 'user_hashtags_frequency_24h',
 'user_links_frequency_24h',
 'user_domains_frequency_24h',
 'user_hashtags_frequency_48h',
 'user_links_frequency_48h',
 'user_domains_frequency_48h']

hashtag_new_columns = ['hashtags_frequency_05h',
 'hashtags_frequency_1h',
 'hashtags_frequency_2h',
 'hashtags_frequency_12h',
 'hashtags_frequency_24h',
 'hashtags_frequency_48h',
 'hashtags_frequency',
 'user_hashtags_frequency_05h',
 'user_hashtags_frequency_1h',
 'user_hashtags_frequency_2h',
 'user_hashtags_frequency_12h',
 'user_hashtags_frequency_24h',
 'user_hashtags_frequency_48h',
 'user_hashtags_frequency']

if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(trn_key)
        display(dfs[trn_key][all_new_columns].limit(8).toPandas())
        display(dfs[trn_key][all_new_columns].describe().toPandas())
        display(dfs[trn_key][hashtag_new_columns].limit(8).toPandas())
        display(dfs[trn_key][hashtag_new_columns].describe().toPandas())
        print(val_key)
        display(dfs[val_key][all_new_columns].limit(8).toPandas())
        display(dfs[val_key][all_new_columns].describe().toPandas())
        display(dfs[val_key][hashtag_new_columns].limit(8).toPandas())
        display(dfs[val_key][hashtag_new_columns].describe().toPandas())


# ### Tweet Views
# 
# Now we want to see how many tweets the engaging user has seen/the engaged-with user has authored user have seen in the last 0.5, 1, 2, 12, 24, and 48 hours as well as overall. These features are called ```engaging_saw_tweets_count``` and ```engageds_tweets_views_count```, respectively. The limit in last hours is given as a prefix, similarly as for the features above.

# In[ ]:


from Functions.fe03_get_tweets_views_count import get_tweets_views_count

user_features = ["engaging_user_id", "engaged_with_user_id"]
user_prefixes = ["engaging_saw_tweets_count", "engageds_tweets_views_count"]
hours_values = [None, 0.5, 1, 2, 12, 24, 48]
hours_names = ["", "_05h", "_1h", "_2h", "_12h", "_24h", "_48h"]
views_counts_columns = ["engaging_saw_tweets_count_05h", "engaging_saw_tweets_count_1h", "engaging_saw_tweets_count_24h", "engaging_saw_tweets_count_48h", "engaging_saw_tweets_count", "engageds_tweets_views_count_05h", "engageds_tweets_views_count_1h", "engageds_tweets_views_count_24h", "engageds_tweets_views_count_48h", "engageds_tweets_views_count"]

for key in dfs:
    for uf, up in zip(user_features, user_prefixes):
        for hv, hn in zip(hours_values, hours_names):
            next_feature = up+hn 
            dfs[key], recreate_columns, changed_dfs = prepare_for_new_columns(df=dfs[key],
                                                                          recreate_even_if_already_exists=RECREATE_EVEN_IF_ALREADY_EXIST,
                                                                          col_name=next_feature,
                                                                          col_names_list=None,
                                                                          changed_dfs_set=changed_dfs,
                                                                          key=key,
                                                                          print_resulting_decision=False)

            if recreate_columns:
                print(f"Now at, {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}, creating {next_feature} for {key}.")
                dfs[key] = get_tweets_views_count(dfs[key], target_user=uf, prior_hours=hv, new_feature=next_feature)
                export_dataframes(dfs={key:dfs[key]}, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                          files_to_be_exported={key})




if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key][views_counts_columns].limit(8).toPandas())
        display(dfs[trn_key][views_counts_columns].describe().toPandas())


# ### Creation Age Ratio
# 
# Originally, we wanted to calculate the ratio of account creatio ages between the engager and the engeged.

# In[ ]:

if not working_on_cluster:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
           display(dfs[trn_key].select(["engaged_with_user_account_creation", "engaging_user_account_creation", "corr_engaged_with_user_account_creation", "engaged_creation_year", "corr_engaging_user_account_creation", "engaging_creation_year", "creation_age_difference", "engaged_age", "engaging_age"]).limit(10).toPandas())
           display(dfs[trn_key].select(["engaged_with_user_account_creation", "engaging_user_account_creation", "corr_engaged_with_user_account_creation", "engaged_creation_year", "corr_engaging_user_account_creation", "engaging_creation_year", "creation_age_difference", "engaged_age", "engaging_age"]).describe().toPandas())


# However, as we can see above, this information is already modelled as a difference in relative months of creation in column ```creation_age_difference``` from March 2006. Given that this is an ordinal column, difference is likely more meaningful than the pure ratio, especially since it can be scaled if necessary.

# ## Export Dataframes

# In[ ]:


show_final_statistics(dfs=dfs, pandas_ref=pd, calculate_steps=CALCULATE_STEPS, 
                      keys_for_printing_statistics=(trn_key, val_key,), working_on_cluster=working_on_cluster)


# In[ ]:


export_dataframes(dfs=dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                  files_to_be_exported=changed_dfs)


# In[ ]:


print("Done!")

