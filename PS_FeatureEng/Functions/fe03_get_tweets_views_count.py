from pyspark.sql import functions as f
from pyspark.sql import Window
import sys


hours = lambda i: int(i * 3600)


def get_tweets_views_count(df, target_user="engaging_user_id", prior_hours=None, new_feature="engaging_saw_tweets_count"):
    """
    Counts the number of tweets the user saw, optionally for the given timeframe.

    df -- dataframe from which counts are extracted
    target_user -- target user, i.e. "engaging_user_id" or "engaged_with_user_id"
    prior_hours -- None to consider the dataframe or the number of hours, cf. https://stackoverflow.com/a/33226511
    new_feature -- new feature name, e.g. "engaging_saw_tweets_count", "engageds_tweets_views_count",
        "engaging_saw_tweets_count_2h", etc.
    """

    if prior_hours is not None:
        w = (Window().partitionBy(target_user).orderBy(f.col("tweet_timestamp").cast("long")).
             rangeBetween(-hours(prior_hours), 0))
    else:
        w = (Window().partitionBy(target_user).orderBy(f.col("tweet_timestamp").cast("long")).
             rangeBetween(-sys.maxsize, 0))

    return df.select(f.col("*"), f.count("tweet_id").over(w).alias(new_feature))
