import pyspark.sql.functions as f
import pyspark.sql.types as t
from math import ceil


def replace_small_values_with_floor_mean(df, col_name = "engaged_with_user_account_creation", corr_prefix = "corr_", lowest_allowed_value=1141171200, fillna = True):
    col_mean = ceil(df.select(f.mean(f.col(col_name))).collect()[0][0])
    # https://stackoverflow.com/questions/43988801/pyspark-modify-column-values-when-another-column-value-satisfies-a-condition
    df = df.withColumn(corr_prefix+col_name, f.when(f.col(col_name) < lowest_allowed_value, col_mean).otherwise(f.col(col_name)))
    if fillna:
        df = df.fillna(col_mean, corr_prefix+col_name)
    return df


def get_year_and_month(df, timestampcol = "engaged_with_user_account_creation", prefix = "engaged_"):
    df = df.withColumn(prefix+"creation_month", f.from_unixtime(timestampcol, format="M").cast(t.IntegerType()))
    df = df.withColumn(prefix+"creation_year", f.from_unixtime(timestampcol, format="yyyy").cast(t.IntegerType()))
    return df


calculate_account_age = f.udf(lambda month, year: (12 * (year-2006)) + month - 3, t.IntegerType())


def get_account_age(df, timestampcol = "engaged_with_user_account_creation", corr_prefix = "corr_", fillna = True, final_prefix = "engaged_", beginning_month = 3, beginning_year = 2006):
    df = replace_small_values_with_floor_mean(df, col_name=timestampcol, corr_prefix=corr_prefix, fillna=fillna)
    timestampcol = corr_prefix + timestampcol
    df = get_year_and_month(df, timestampcol=timestampcol, prefix=final_prefix)
    df = df.withColumn(final_prefix+"age", calculate_account_age(final_prefix+"creation_month", final_prefix+"creation_year"))
    return df
