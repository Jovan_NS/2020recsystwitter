import pyspark.sql.functions as f
import pyspark.sql.types as t


def extract_time_hour_and_weekday(df, column_name = "tweet_timestamp", result_prefix = "tweet_"):
    df = df.withColumn(result_prefix + "time", f.from_unixtime(column_name, format="E HH:MM Z"))
    df = df.withColumn(result_prefix + "hour", f.from_unixtime(column_name, format="H").cast(t.IntegerType()))
    df = df.withColumn(result_prefix + "weekday", f.from_unixtime(column_name, format="u").cast(t.IntegerType()))
    df = df.withColumn(result_prefix + "weekday_str", f.from_unixtime(column_name, format="E"))
    df = df.withColumn(result_prefix + "day_of_year", f.from_unixtime(column_name, format="D").cast(t.IntegerType()))
    
    return df