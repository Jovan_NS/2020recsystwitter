import pyspark.sql.functions as f


def extract_engagements_2d(graph_1d, engaging='graph_engaging_user_id', engaged='graph_engaged_with_user_id',
                           engagements=['like', 'reply', 'retweet', 'quote', 'react']):
    """
    Creates a dataframe of the form <graph_id_prefix+engaging, graph_id_prefix+engaged, count_<eng>, flag_<eng>>. The
    count_<eng> counts the number each engagement <eng> in <engagements> of the second degree. Moreover,
    flag is boolean which is true when the count is greater than zero for the second degree (if the first user has
    authored a tweet that some other user(s) has engaged with whose tweet in turn a third user has engaged with -- in
    this case the flag between the first and the third user would be set to true.

    Parameters
    ----------
    graph_1d: a first-degree engagements graph, as created by function extract_engagements
    engaging: the name of the column with the engaging user
    engaged: the name of the column with the engaged-with user
    engagements: engagements to be considered

    Returns the resulting dataframe with four columns
    -------

    """

    left = graph_1d.withColumnRenamed(engaging, "left_" + engaging).withColumnRenamed(engaged, "left_" + engaged)
    right = graph_1d.withColumnRenamed(engaging, "right" + engaging).withColumnRenamed(engaged, "right_" + engaged)

    for eng in engagements:
        left = left.withColumnRenamed("count_" + eng, "left_count_" + eng).drop("flag(" + eng + ")")
        right = right.withColumnRenamed("count_" + eng, "right_count_" + eng).drop("flag(" + eng + ")")

    # join on the second (inner) user
    graph_2d = left.join(right, left["left_" + engaged] == right["right" + engaging], how="inner")
    graph_2d = graph_2d.drop("left_" + engaged).drop("right" + engaging) \
        .withColumnRenamed("left_" + engaging, engaging).withColumnRenamed("right_" + engaged, engaged)

    for eng in engagements:
        graph_2d = graph_2d.withColumn(eng, f.col("left_count_" + eng) + f.col("right_count_" + eng))
        graph_2d = graph_2d.drop("left_count_" + eng).drop("right_count" + eng)

    # get the final sum (two users may have multiple second-degree connections
    sums = {eng: "sum" for eng in engagements}
    graph_2d = graph_2d.groupBy(engaging, engaged).agg(sums)

    # remove the unneeded columns
    for eng in engagements:
        graph_2d = graph_2d.drop(eng)
        graph_2d = graph_2d.withColumnRenamed("sum(" + eng + ")", "count_" + eng)
        graph_2d = graph_2d.withColumn("flag_" + eng, f.when((f.col("count_" + eng) > 0), True).otherwise(False))

    return graph_2d
