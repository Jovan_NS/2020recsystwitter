from py4j.protocol import Py4JJavaError
from pyspark.sql.functions import col
from pyspark.sql.utils import AnalysisException

import os

from Functions.export_dataframes import export_dataframes


def create_corresponding_train_subset_mappings(dfs: dict, changed_dfs: set,
                                               training_for_test_and_val: dict,
                                               train_prefix="train",
                                               other_prefixes=("val+test", "val", "test",),
                                               calendar_days_to_be_used_for_train_training_sbs=[37, 38, 39, ],
                                               calendar_days_to_be_used_for_train_testing_sbs=[40, 41, 42, 43, ],
                                               calendar_days_col="tweet_day_of_year",
                                               fix_different_ratio=True):
    """
    Data Split

    For val/test subsets we want to use the corresponding train subset for the training and the val/test subsets as they
    are for testing.
    For training subsets, we want to use the first three days (or as specified in the argument
    <calendar_days_to_be_used_for_train_training_sbs>) of the training subsets for the training and the later four days
    (or as in <calendar_days_to_be_used_for_train_testing_sbs>) for testing (i.e. Thu, Fri, Sat -> Training; Sun, Mon,
    Tue, Wed -> Testing).
    Note that for the val/test, we do not need to export/import anything at this stage (for training, we just use the
    pure training dataset), but for training, we need to save the split dataset.
    """

    mapping_dfs = {}
    resulting_dfs = {}

    for dfs_key in dfs:

        dfs_key_found = False

        for other_prefix in other_prefixes:
            if other_prefix in dfs_key:
                dfs_key_found = True
                # e.g. test_1pct_subset will be changed into train_1pct_subset
                train_key = dfs_key.replace(other_prefix, train_prefix)
                # find and save the corresponding training dataset
                mapping_dfs[dfs_key] = training_for_test_and_val[train_key]
                # leave the test/val dataset for testing as is (irrelevant if already in changed_dfs or not)
                resulting_dfs[dfs_key] = dfs[dfs_key]

                # leave the inner for loop to prevent "val+train" to fall here multiple times
                break

        if train_prefix in dfs_key:
            dfs_key_found = True
            # split the dataset based on the day of the week
            mapping_dfs[dfs_key] = training_for_test_and_val[dfs_key].filter(
                col(calendar_days_col).isin(calendar_days_to_be_used_for_train_training_sbs))

            # if the train dfs_key is in the changed_dfs, it means that a backup was imported, not a previous version of
            # a fe04 file, therefore split needs to be done again.
            if dfs_key in changed_dfs:
                resulting_dfs[dfs_key] = training_for_test_and_val[dfs_key].filter(
                    col(calendar_days_col).isin(calendar_days_to_be_used_for_train_testing_sbs))
                # verify the dataset was split correctly
                print(dfs_key, dfs[dfs_key].count(), mapping_dfs[dfs_key].count(), resulting_dfs[dfs_key].count())
                assert dfs[dfs_key].count() == mapping_dfs[dfs_key].count() + resulting_dfs[dfs_key].count()

            else:
                resulting_dfs[dfs_key] = dfs[dfs_key]
                print(dfs_key, "Loaded from previous version!", mapping_dfs[dfs_key].count(), resulting_dfs[dfs_key].count())


        # fix ratios (more instances in train than in test/val as well as in the subset of train than in whole train)
        if fix_different_ratio and dfs_key_found and (dfs_key in changed_dfs):
            fraction = dfs[dfs_key].count() / mapping_dfs[dfs_key].count()
            mapping_dfs[dfs_key] = mapping_dfs[dfs_key].sample(withReplacement=False, fraction=fraction, seed=42)

        # assert we did not have an unexpected prefix
        assert dfs_key_found

    return mapping_dfs, resulting_dfs
