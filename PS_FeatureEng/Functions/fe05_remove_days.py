from pyspark.sql.functions import col
from Functions.pyspark_df_shape import pyspark_df_shape


def remove_days(df, days_column: str, days_to_remove: list, print_info=True, df_name="df name not specified"):
    """
    Removes days specified in <days_to_remove> and corresponding with the column <days_column> from <df>.

    Parameters
    ----------
    df: a PySpark dataframe
    days_column: the column in <df> with days to be compared with values in <days_to_remove>
    days_to_remove: a list with the days to be removed
    print_info: whether to print the intermediate information, e.g. for debugging purposes
    df_name: the name of the df (only relevant if <print_info> is true>

    Returns a Pyspark with excluded days
    -------
    """

    new_df = df.filter(~col(days_column).cast("int").isin(days_to_remove))

    if print_info:
        old_shape = pyspark_df_shape(df)
        new_shape = pyspark_df_shape(new_df)
        print(f"{df_name} had shape {old_shape} and now it has shape {new_shape}")

    return new_df
