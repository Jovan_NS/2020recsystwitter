def show_final_statistics(dfs, pandas_ref, calculate_steps=False, keys_for_printing_statistics=None,
                          working_on_cluster=True):
    """
    Prints the final DF statistics for dataframes in <dfs> which correspond to keys in <keys_for_printing_statistics>.

    Parameters
    ----------
    dfs: a dict with dataframes
    pandas_ref: a reference to Pandas (often pd)
    calculate_steps: must be True to proceed
    keys_for_printing_statistics: keys for those dataframes in <dfs> which are to be displayed
    working_on_cluster: must be False to proceed

    Returns
    -------
    """

    if (not calculate_steps) or (keys_for_printing_statistics is None) or (pandas_ref is None) or working_on_cluster:
        return

    for key in dfs:
        if key in keys_for_printing_statistics:
            with pandas_ref.option_context('display.max_rows', None, 'display.max_columns', None):
                print(f"Printing statistics for {key} of shape ({dfs[key].count()}, {len(dfs[key].columns)}).")
                dfs[key].printSchema()
                display(dfs[key].describe().toPandas())
