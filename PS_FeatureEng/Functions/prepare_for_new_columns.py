def prepare_for_new_columns(df, recreate_even_if_already_exists=True, col_name=None, col_names_list=None,
                            changed_dfs_set=None, key=None, print_resulting_decision=True):
    """
    Removes the column from col_name or columns from col_names_list and returns the resulting df columns if
    recreate_even_if_already_exists is set to true or if some but not all columns in col_names_list are present. If
    changed_dfs_set and key are both not none, key will be appended to changed_dfs_set if there are missing columns.
    """

    some_columns_missing = False
    missing_columns = dict()
    missing_columns[key] = set()

    if col_name is not None:
        if col_name not in df.columns:
            some_columns_missing = True
            missing_columns[key].add(col_name)

    if col_names_list is not None:
        for col in col_names_list:
            if col not in df.columns:
                some_columns_missing = True
                missing_columns[key].add(col)

    recreate_columns = recreate_even_if_already_exists or some_columns_missing

    if recreate_columns:
        if (changed_dfs_set is not None) and (key is not None):
            changed_dfs_set.add(key)
            # The code below was commented out to prevent too much output, since  it had been verified
            # if print_resulting_decision:
            #     print(f"New columns for {missing_columns} must be created.")

        if col_name is not None:
            if col_name in df.columns:
                df = df.drop(col_name)

        if col_names_list is not None:
            for cn in col_names_list:
                if cn in df.columns:
                    df = df.drop(cn)
                    # The code below was commented out to prevent too much output, since  it had been verified
                    # if print_resulting_decision:
                    #     print(f"The old column {cn} was dropped.")

    elif print_resulting_decision and key is not None:
        print(f"Old columns from {key} are kept.")

    return df, recreate_columns, changed_dfs_set
