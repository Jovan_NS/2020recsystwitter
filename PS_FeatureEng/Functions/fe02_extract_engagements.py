import pyspark.sql.functions as f


def extract_engagements(df, engaging='engaging_user_id', engaged='engaged_with_user_id', graph_id_prefix='graph_',
                        engagements=['like', 'reply', 'retweet', 'quote', 'react']):
    """
    Creates a dataframe of the form <graph_id_prefix+engaging, graph_id_prefix+engaged, count_<eng>, flag_<eng>>.
    The count_<eng> counts the number each engagement <eng> in <engagements> by the user in <graph_id_prefix+engaging>
    towards the user in  <graph_id_prefix+engaged>. The flag_<eng> a boolean column indicating whether the corresponding
    count_<eng> is >= 1.

    Parameters
    ----------
    df: Pyspark dataframe from which the graphs are built
    engaging: the name of the column with the engaging user
    engaged: the name of the column with the engaged-with user
    graph_id_prefix: the prefix of the resulting columns, cf. the description
    engagements: engagements to be considered

    Returns the resulting dataframe with four columns
    -------

    """

    df_sub = df.select([engaging, engaged, *engagements])

    # https://stackoverflow.com/questions/33882894/spark-sql-apply-aggregate-functions-to-a-list-of-columns
    sums = {eng: "sum" for eng in engagements}
    graph = df_sub.groupBy(engaging, engaged).agg(sums)

    for eng in engagements:
        graph = graph.withColumnRenamed("sum(" + eng + ")", "count_" + eng)
        graph = graph.withColumn("flag_" + eng, f.when((f.col("count_" + eng) > 0), True).otherwise(False))

    if graph_id_prefix is not None:
        graph = graph.withColumnRenamed(engaging, graph_id_prefix + engaging) \
            .withColumnRenamed(engaged, graph_id_prefix + engaged)

    return graph
