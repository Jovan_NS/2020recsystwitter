from pyspark.sql.functions import col, udf
from pyspark.sql.types import IntegerType


def get_value_from_dict(dict_col, dict_key):
    if (dict_col is None) or (dict_key not in dict_col.keys()):
        return 0

    return dict_col[dict_key]


def tweets_in_this_language_count(df, new_col_name, language_dict_col, tweet_language_col="language"):
    """
    Looks up how many tweets in the language the user has already seen/authored.

    Parameters
    ----------
    df: the source and target dataframe
    new_col_name: the name of the target column
    language_dict_col: the name of the column with the tweet language directory, previously created (expected
        "seen_languages_dict" or "authored_languages_dict")
    tweet_language_col: the name of the column with the language of the target tweet (expected "language")

    Returns the dataframe <resulting_df> with the new column added
    -------
    """

    get_value_from_dict_udf = udf(get_value_from_dict, IntegerType())

    # https://stackoverflow.com/questions/48305443/typeerror-column-object-is-not-callable-using-withcolumn
    return df.withColumn(new_col_name, get_value_from_dict_udf(col(language_dict_col), col(tweet_language_col)))
