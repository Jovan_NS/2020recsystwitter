# ispired by: https://stackoverflow.com/a/56408669


def remove_duplicate_columns(df, dup_suffix="_dup"):
    # create cols_new so that seen columns will have a suffix '_dup'
    cols_new = []
    seen = set()
    for c in df.columns:
        if c in seen:
            cols_new.append(c+dup_suffix)
        else:
            cols_new.append(c)
            seen.add(c)


    return df.toDF(*cols_new).select(*[c for c in cols_new if not c.endswith(dup_suffix)])
