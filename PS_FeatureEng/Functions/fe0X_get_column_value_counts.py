import pyspark.sql.functions as f


def get_column_value_counts(df, column_to_count_values="engaging_user_id", group_column="language", count_alias="value_counts"):
    """
    Counts the appearances of <group_column> for each value of <column_to_count_values> and returns it in a two-column
    dataframe where the first column corresponds to unique values of <column_to_count_values> and the second contains
    dicts with counts of <group_column>.
    Important note: The first field from a struct (key) can't be null, this will through a Py4JJavaError
    Explanation: https://stackoverflow.com/questions/66143273/in-spark-how-can-i-agg-a-column-into-a-frequency-map-with-the-unique-values-in-t

    Parameters
    ----------
    df -- df to get the value counts from
    column_to_count_values -- the column to count values from
    group_column -- the value to group by
    count_alias -- the name of the resulting column

    Returns
    -------
    This returns a two-column dataframe with the first column corresponding to unique values of <column_to_count_values>
    and the second being a dictionary where the keys correspond to unique values of <group_column> and the values
    corresponds to the counts of appearances of that <group_column> for the given value in column
    <column_to_count_values>.
    Illustration: https://prnt.sc/CIfWIFUttCKn
    """

    result = df.groupBy(
        column_to_count_values, group_column,
    ).agg(
        f.count("*").alias("value_counts_temp")
    ).groupBy(
        column_to_count_values
    ).agg(
        f.map_from_entries(f.collect_list(f.struct(group_column, "value_counts_temp"))).alias(count_alias)
    )

    return result
