import pyspark.sql.functions as f


def count_tweet_elements(df, inc, outc):
    
    ## The following more straight-forward version does not work with this version of PySpark (https://stackoverflow.com/a/45713585):
    # df[outc] = df.select(f.size(f.split(f.col(inc), "\t")).alias("help"))\
    #    .withColumn("value", f.when(f.col("help") == -1, f.lit(0))\
    #    .otherwise(f.col("help")))["value"]
    
    df = df.withColumn("help", f.size(f.split(f.col(inc), "\t")))
    df = df.withColumn(outc, f.when(df["help"] == -1, f.lit(0)).otherwise(df["help"]))
    df = df.drop("help")
    return df
