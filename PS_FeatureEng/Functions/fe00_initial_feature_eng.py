# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq  # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
from pyspark.sql.functions import udf
import pyspark.sql.types as t
from operator import add
from pyspark.sql.utils import AnalysisException

# Other imports
import os
import pandas as pd
from transformers import BertTokenizer, BertModel

def if_any_reaction_then_1(row):
    flag = 0
    for reaction in ["reply_timestamp", "retweet_timestamp", "retweet_with_comment_timestamp", "like_timestamp"]:
        if row[reaction] is not None:
            return 1

    return 0


def separate_and_decrypt(row):
    if row is None:
        return ""

    tweet_tokens = row.split("\t")
    return "".join(tokenizer.convert_tokens_to_string(tokenizer.convert_ids_to_tokens(tweet_tokens)))


def replace_name_if_wrong(name):
    if name[:4] == "enag":
        return "engag" + name[4:]
    return name


def intial_feature_eng(all_dfs: dict, recreate_even_if_already_exist = False):
    '''
    FeatureEng 01
    
    This script takes the resulting datasets from PySpark-DataSampeling.ipynb and performs basic data preparation and feature engineering. The resulting datasets have "FE_" in their names.
    '''

    changed_dfs = set()

    '''
    Reaction Flags

    like: a feature whose value is set to 0 if like_timestamp is None (an empty filed), and 1 if it is a timestamp.
    reply: analogues of like for reply_timestamp.
    retweet: analogues of like for retweet_timestamp.
    quote: analogues of like for retweet_with_comment_timestamp.
    react: true (=1) if any of the four engagements (like, reply, retweet, quote) occurred (is equal to 1).
    '''
    reactions = ["like", "reply", "retweet", "quote"]
    reaction_timestamps = [reaction + "_timestamp" for reaction in reactions]
    reaction_timestamps = [reaction.replace("quote", "retweet_with_comment") for reaction in reaction_timestamps]

    if_not_null_then_1_udf = udf(lambda x: 0 if x is None else 1, t.ByteType())

    for key in all_dfs:
        for reaction, ts in zip(reactions, reaction_timestamps):
            if (recreate_even_if_already_exist) or (reaction not in all_dfs[key].columns):
                all_dfs[key] = all_dfs[key].withColumn(reaction, if_not_null_then_1_udf(all_dfs[key][ts]))
                changed_dfs.add(key)

    udf_if_any_reaction_then_1 = udf(if_any_reaction_then_1, t.ByteType())

    for key in all_dfs:
        if (recreate_even_if_already_exist) or ("react" not in all_dfs[key].columns):
            all_dfs[key] = all_dfs[key].withColumn("react", udf_if_any_reaction_then_1(
                f.struct([all_dfs[key][x] for x in all_dfs[key].columns])))
            changed_dfs.add(key)
            print(f"Done with {key}.")

    '''
    Tweet Tokenisation

    We now want to detokenise tweet text.
    '''
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
    model = BertModel.from_pretrained('bert-base-multilingual-cased')

    udf_separate_and_decrypt = f.udf(separate_and_decrypt, t.StringType())

    for key in all_dfs:
        if (recreate_even_if_already_exist) or ("text" not in all_dfs[key].columns):
            all_dfs[key] = all_dfs[key].withColumn("text", udf_separate_and_decrypt(all_dfs[key]["text_tokens"]))
            changed_dfs.add(key)
            print(f"Done with {key}.")

    '''
    Rename Misspelled Columns

    The original data preparation snipped contained a spelling error as some columns contained a word "enaged" instead of "engaged".
    '''
    for key in all_dfs:
        correct_column_names = {name: replace_name_if_wrong(name) for name in all_dfs[key].columns}
        for dict_key in correct_column_names:
            if dict_key != correct_column_names[dict_key]:
                all_dfs[key] = all_dfs[key].withColumnRenamed(dict_key, correct_column_names[dict_key])

    return all_dfs, changed_dfs