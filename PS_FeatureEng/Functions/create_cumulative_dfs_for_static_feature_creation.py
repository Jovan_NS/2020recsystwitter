from pyspark.sql.utils import AnalysisException
from py4j.protocol import Py4JJavaError


def create_cumulative_dfs_for_static_feature_creation(dfs:dict, train_prefix="train",
                                                      other_prefixes=("val+test", "val", "test",),
                                                      column_mismatch_okay=False):
    """
    Data Split

    Since this type of features is modelled to represent more static aspects of the context (i.e. whether a
    relationship between two users even exists), we will use all information available. This is in particular:
    * full train (train)
    * full train+val (for val)
    * full train+test+val (for test+val)
    For a tweet from Friday in train dataset, we would therefore also include info from tweets from Saturday in the
    train dataset. In case of inconsistencies, we let the feature be positive if there is at least one positive
    instance, for practical and semantical reasons.
    """

    cumulative_dfs = {}

    for dfs_key in dfs:
        if train_prefix in dfs_key:
            # https://stackoverflow.com/a/66463461
            cumulative_dfs[dfs_key] = dfs[dfs_key].alias('cummulative_' + dfs_key)
            continue

        other_found = False
        for other_prefix in other_prefixes:
            if (not other_found) and (other_prefix in dfs_key):
                other_found = True
                train_key = dfs_key.replace(other_prefix, train_prefix)

                try:
                    cumulative_dfs[dfs_key] = dfs[train_key].unionByName(dfs[dfs_key])
                except (AnalysisException, Py4JJavaError) as e:
                    if column_mismatch_okay:
                        common_cols = set(dfs[train_key].columns).intersection(set(dfs[dfs_key].columns))
                        cumulative_dfs[dfs_key] = dfs[train_key].select(*common_cols).unionByName(dfs[dfs_key].select(*common_cols))
                    else:
                        raise e

                if column_mismatch_okay:
                    assert (len(cumulative_dfs[dfs_key].columns) == len(dfs[dfs_key].columns)) or \
                           (len(cumulative_dfs[dfs_key].columns) == len(dfs[train_key].columns))
                else:
                    assert len(dfs[train_key].columns) == len(dfs[dfs_key].columns)
                    assert len(cumulative_dfs[dfs_key].columns) == len(dfs[dfs_key].columns)
                assert cumulative_dfs[dfs_key].count() == (dfs[train_key].count() + dfs[dfs_key].count())

        assert other_found

    return cumulative_dfs
