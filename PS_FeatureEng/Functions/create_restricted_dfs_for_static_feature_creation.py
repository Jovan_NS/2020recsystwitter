from pyspark.sql.functions import col

def create_restricted_dfs_for_static_feature_creation(dfs:dict, train_prefix="train", other_prefixes=("val+test","val","test",),
                                                      col_with_first_day_identifier="tweet_day_of_year", first_train_days=[37,38,39,]):
    """
    Data Split

    This function creates data split for features which are based on engagements (and should thus be unavailable for
    train and val datests). In particular, this will be:
    * first (three, by default) days of train (for train)
    * train dataset (for val, test, and test+val)
    """

    restricted_dfs = {}

    for dfs_key in dfs:
        if train_prefix in dfs_key:
            restricted_dfs[dfs_key] = dfs[dfs_key].filter(dfs[dfs_key][col_with_first_day_identifier].isin(first_train_days))
            continue

        other_found = False
        for other_prefix in other_prefixes:
            if (not other_found) and (other_prefix in dfs_key):
                other_found = True
                train_key = dfs_key.replace(other_prefix, train_prefix)
                # https://stackoverflow.com/a/66463461
                restricted_dfs[dfs_key] = dfs[train_key].alias("train_version_in_place_of_"+other_prefix)

        assert other_found

    return restricted_dfs
