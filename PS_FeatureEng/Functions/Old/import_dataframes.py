import os
from py4j.protocol import Py4JJavaError
from pyspark.sql.utils import AnalysisException


def help_import_dataframes(spark, previous_files, backup_files, recreate_even_if_already_exist, filename,
                           backup_filename, HDFS_datafolder):
    import_failed = False

    if not recreate_even_if_already_exist:
        try:
            df = spark.read.parquet(os.path.join(HDFS_datafolder, filename))
            previous_files.append(filename)
        except (Py4JJavaError, AnalysisException) as e:
            import_failed = True

    if recreate_even_if_already_exist or import_failed:
        df = spark.read.parquet(os.path.join(HDFS_datafolder, backup_filename))
        backup_files.append(backup_filename)

    return df, previous_files, backup_files


def import_dataframes(spark, datasets=("train", "val", "test"),
                      sampling_techniques=("random", "EWU", "EU", "inter_EWU+EU", "tweet"),
                      sampling_percentages=("1pct", "2pct", "5pct", "10pct"), featureset_export_prefix="Encoding_",
                      backup_featureset_prefix="FE_", HDFS_datafolder="Data", recreate_even_if_already_exist=False,
                      dev=False):
    dfs = {}

    if dev:
        sampling_techniques = sampling_techniques[:1]  # only take the first sample
        sampling_percentages = sampling_percentages[:1]

    previous_files = []
    backup_files = []

    # read sampled datasets
    for sampling_percentage in sampling_percentages:
        for sampling_technique in sampling_techniques:
            for key in datasets:
                filename = featureset_export_prefix + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                backup_filename = backup_featureset_prefix + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                sample_key = key + "_" + sampling_technique + "_sample_" + sampling_percentage
                dfs[sample_key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                                       backup_files,
                                                                                       recreate_even_if_already_exist,
                                                                                       filename, backup_filename,
                                                                                       HDFS_datafolder)

    # read non-sampled dataset if not in DEV
    if not dev:
        for key in datasets:
            filename = featureset_export_prefix + key + ".parquet"
            backup_filename = backup_featureset_prefix + key + ".parquet"
            dfs[key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                            backup_files,
                                                                            recreate_even_if_already_exist, filename,
                                                                            backup_filename, HDFS_datafolder)

    print(f"Done with reading dataframes! Read {len(previous_files)} previous versions and {len(backup_files)} backups."
          f" \nPrevious versions: {previous_files}; \nbackups: {backup_files}.")
    files_to_be_changed = set([dfn.replace(backup_featureset_prefix, "").replace(".parquet", "") for dfn in backup_files])
    return dfs, files_to_be_changed
