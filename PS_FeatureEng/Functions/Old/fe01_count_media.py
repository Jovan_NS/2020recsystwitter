import pyspark.sql.functions as f
import pyspark.sql.types as t
from collections import Counter


def separate_and_count_media(row, val_names=["Photo", "Video", "Gif"], sep="\t"):
    new_vals = [0] * len(val_names)

    if row is None:
        return str(new_vals).strip("[]")

    elems_list = row.split(sep)
    occurances = Counter(elems_list)

    for i, val_name in enumerate(val_names):
        new_vals[i] = occurances[val_name]

    return str(new_vals).strip("[]")


separate_and_count_media = f.udf(separate_and_count_media, t.StringType())


def count_media(df):
    val_names = ["Photo", "Video", "Gif"]
    df = df.withColumn("present_media_counts", separate_and_count_media(df["present_media"]))
    # https://stackoverflow.com/questions/39235704/split-spark-dataframe-string-column-into-multiple-columns
    split_col = f.split(df["present_media_counts"], ", ") 
    for i, val_name in enumerate(val_names):
        df = df.withColumn(val_name.lower() + "s_count", split_col.getItem(i).cast(t.IntegerType()))

    df = df.withColumn("media_count",
                       sum(df[[col for col in [val_name.lower() + "s_count" for val_name in val_names]]]))
    df = df.drop("present_media_counts")

    return df
