import pyspark.sql.functions as f


'''
Set group_by_cols = "item" to get values for all tweets in the dataset. Set it to ["engaging_user_id", "item"] to get 
values only for tweets seen by the user.
Returns a tuple whose first element is the resulting df and second a boolean indicating whether the column had to be 
re-created (True if so, False if the column with the given name already existed.
'''
def get_frequency(target_df, column_name, source_df=None, group_by_cols="item",
                  new_col_prefix="", new_col_suffix=""):
    if source_df is None:
        source_df = target_df

    if (new_col_prefix is not None) and (new_col_prefix != "") and (new_col_prefix[-1] != "_"):
        new_col_prefix = new_col_prefix + "_"

    if (new_col_suffix is not None) and (new_col_suffix != "") and (new_col_suffix[0] != "_"):
        new_col_suffix = "_" + new_col_suffix

    new_col = new_col_prefix + column_name.replace("present_", "") + "_frequency" + new_col_suffix
    if new_col in target_df.columns:
        return target_df, False
    

    start = source_df.select("tweet_id", "engaging_user_id", f.split(f.col(column_name), "\t").alias("help"))
    exploded = start.select("tweet_id", "engaging_user_id", f.explode(f.col("help")).alias("item"))
    item_count = exploded.groupBy(group_by_cols).count()
    joined = exploded.join(item_count, on=group_by_cols)
    final = joined.groupBy("tweet_id", "engaging_user_id").agg(f.sum("count"))
    final = final.withColumnRenamed("sum(count)", new_col)
    target_df = target_df.join(final, on=["tweet_id", "engaging_user_id"], how="leftouter")

    return target_df, True 
