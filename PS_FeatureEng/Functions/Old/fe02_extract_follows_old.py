import pyspark.sql.functions as f

def extract_follows(df):
    """
    Extracts follows-relations of the first degree.
    
    Creates a comprehensive df with follows relations of the form <following_id, followed_id>
    containing only those pairs of users such that following_id follows followed_id. This is the
    case when following_id viewed a tweet by followed_id (by design of the dataset) - i.e. when 
    following_id == engaging_user_id and followed_id == engaged_with_user_id - or when following_id
    wrote a tweet seen by followed_id and engagee_follows_engager == True

    Note: all user pairs not in this dataset are sure to be false (i.e. not to follow each other) due 
    to the design of the datast.
    """
    
    engaging_follows_engaged = df.select(['engaging_user_id', 'engaged_with_user_id'])
    engaging_follows_engaged = engaging_follows_engaged.withColumnRenamed('engaging_user_id', 'following_id')
    engaging_follows_engaged = engaging_follows_engaged.withColumnRenamed('engaged_with_user_id', 'followed_id')
    
    engaged_follows_engaging = df.filter(f.col('engagee_follows_engager') == True).select(['engaged_with_user_id', 'engaging_user_id']).distinct()
    engaged_follows_engaging = engaged_follows_engaging.withColumnRenamed('engaged_with_user_id', 'following_id')
    engaged_follows_engaging = engaged_follows_engaging.withColumnRenamed('engaging_user_id', 'followed_id')
    
    
    return engaging_follows_engaged.union(engaged_follows_engaging).distinct()
