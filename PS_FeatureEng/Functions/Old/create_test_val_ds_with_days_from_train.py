from pyspark.sql.utils import AnalysisException
from py4j.protocol import Py4JJavaError
from pyspark.sql.functions import col


def create_test_val_ds_with_days_from_train(dfs:dict, train_prefix="train", other_prefixes=("val+test", "val", "test",),
                                            calendar_days_to_be_copied=[42,43,], calendar_days_col="tweet_day_of_year",
                                            column_mismatch_okay=False):
    """
    Data Split

    For test and val we want to include the last days of train, as it precedes the beginning time of test and val.
    """

    appended_dfs = {}

    for dfs_key in dfs:
        if train_prefix in dfs_key:
            # https://stackoverflow.com/a/66463461
            appended_dfs[dfs_key] = dfs[dfs_key].alias('appended_' + dfs_key)
            continue

        other_found = False
        for other_prefix in other_prefixes:
            if (not other_found) and (other_prefix in dfs_key):
                other_found = True
                train_key = dfs_key.replace(other_prefix, train_prefix)

                last_train_days = dfs[train_key].filter(col(calendar_days_col).isin(calendar_days_to_be_copied))

                try:
                    appended_dfs[dfs_key] = last_train_days.unionByName(dfs[dfs_key])
                except (AnalysisException, Py4JJavaError) as e:
                    if column_mismatch_okay:
                        common_cols = set(last_train_days.columns).intersection(set(dfs[dfs_key].columns))
                        appended_dfs[dfs_key] = last_train_days.select(*common_cols).unionByName(dfs[dfs_key].select(*common_cols))
                    else:
                        raise e

                if column_mismatch_okay:
                    assert (len(appended_dfs[dfs_key].columns) == len(dfs[dfs_key].columns)) or \
                           (len(appended_dfs[dfs_key].columns) == len(dfs[train_key].columns))
                else:
                    assert len(last_train_days.columns) == len(dfs[dfs_key].columns)
                    assert len(appended_dfs[dfs_key].columns) == len(dfs[dfs_key].columns)
                assert  (last_train_days.count() + dfs[dfs_key].count()) == appended_dfs[dfs_key].count()

        assert other_found

    return appended_dfs
