import pyspark.sql.functions as f


def get_engagements(target_df, column_name, source_df=None, engs=('like', 'reply', 'retweet', 'quote', 'react',),
                    group_by_cols="item", new_col_prefix="", new_col_suffix=""):
    """
    Set group_by_cols = "item" to get values for all tweets in the dataset. Set it to ["engaging_user_id", "item"] to
    get values only for tweets seen by the user.
    """

    if source_df is None:
        source_df = target_df

    if (new_col_prefix is not None) and (new_col_prefix != "") and (new_col_prefix[-1] != "_"):
        new_col_prefix = new_col_prefix + "_"

    if (new_col_suffix is not None) and (new_col_suffix != "") and (new_col_suffix[0] != "_"):
        new_col_suffix = "_" + new_col_suffix

    new_cols = [column_name.replace("present_", "") + "_" + eng + new_col_suffix for eng in engs]
    action_needed = False
    for new_col in new_cols:
        if new_col not in target_df.columns:
            action_needed = True
            break

    if not action_needed:
        return target_df, action_needed

    start = source_df.select("tweet_id", "engaging_user_id", *engs, f.split(f.col(column_name), "\t").alias("help"))
    exploded = start.select("tweet_id", "engaging_user_id", *engs, f.explode(f.col("help")).alias("item"))
    sums_engs = [f.sum(key) for key in engs]
    sums = exploded.groupBy(group_by_cols).agg(*sums_engs)
    joined = exploded.join(sums, on=group_by_cols)
    sums_of_sums = [f.sum("sum(" + key + ")") for key in engs]
    final = joined.groupBy("tweet_id", "engaging_user_id").agg(*sums_of_sums)
    "sum(count)"
    old_cols = ["sum(sum(" + key + "))" for key in engs]
    for old_col, new_col in zip(old_cols, new_cols):
        final = final.withColumnRenamed(old_col, new_col)
    target_df = target_df.join(final, on=["tweet_id", "engaging_user_id"], how="leftouter")

    return target_df, action_needed
