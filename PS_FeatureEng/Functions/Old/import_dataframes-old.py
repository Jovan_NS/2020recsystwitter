import os
from py4j.protocol import Py4JJavaError
from pyspark.sql.utils import AnalysisException


def help_import_dataframes(spark, previous_files, backup_files, recreate_even_if_already_exist, filename,
                           backup_filenames, HDFS_datafolder):
    file_loaded = False

    if not recreate_even_if_already_exist:
        try:
            df = spark.read.parquet(os.path.join(HDFS_datafolder, filename))

            previous_files.append(filename)
            file_loaded = True
        except (Py4JJavaError, AnalysisException) as e:
            pass

    if (recreate_even_if_already_exist or not file_loaded) and ((backup_filenames is not None) and
                                                                (type(backup_filenames) == list) and
                                                                (backup_filenames != [])):
        for backup_filename in backup_filenames:
            try:
                df = spark.read.parquet(os.path.join(HDFS_datafolder, backup_filename))

                backup_files.append(backup_filename)
                file_loaded = True
                break
            except (Py4JJavaError, AnalysisException) as e:
                pass

    if not file_loaded:
        raise e # if none of the (backup) file names could be found, forward the error

    return df, previous_files, backup_files


def import_dataframes(spark, datasets=("train", "val", "test"),
                      sampling_techniques=("random", "EWU", "EU", "inter_EWU+EU", "tweet"),
                      sampling_days=None,
                      sampling_percentages=("1pct", "2pct", "5pct", "10pct"), featureset_export_prefix="Encoding_",
                      backup_featureset_prefixes = None, HDFS_datafolder="Data", recreate_even_if_already_exist=False,
                      dev=False):
    dfs = {}

    process_day = lambda x: "day_" + x + "_" if x != "" else ""

    # process sampling days
    # assumption: sampling_days is None or dict of list-like with <datasets> as keys
    if sampling_days is None:
        sampling_days = {}
        for ds in datasets:
            sampling_days[ds] = ("",)
    else:
        for ds in datasets:
            if ds not in sampling_days:
                raise ValueError("sampling_days must be a None or a dict of list-like containing days")
            full_days_name = (process_day(x) for x in sampling_days[ds])
            sampling_days[ds] = tuple(full_days_name)  # convert the generator into a tuple

    if dev:
        sampling_techniques = sampling_techniques[:1]  # only take the first sample
        sampling_percentages = sampling_percentages[:1]
        for ds in datasets:
            sampling_days[ds] = list(sampling_days[ds][:3])
            if "" not in sampling_days[ds]:
                sampling_days[ds].append("")  # assure that non-dayed case is there as well
            sampling_days[ds] = tuple(sampling_days[ds])

    previous_files = []
    backup_files = []

    # read sampled datasets
    for sampling_percentage in sampling_percentages:
        for sampling_technique in sampling_techniques:
            for key in datasets:
                for day in sampling_days[key]:
                    filename = featureset_export_prefix + day + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                    if backup_featureset_prefixes is None:
                        backup_filenames = None
                    else:
                        backup_filenames = []
                        for backup_featureset_prefix in backup_featureset_prefixes:
                            if backup_featureset_prefix == "typed_":
                                backup_featureset_prefix = ""  # typed_ only for non-sampled datasets, cf. notebook DataPrep01
                            backup_filename = backup_featureset_prefix + day + key + "_" + sampling_technique + "_sample_" + sampling_percentage + ".parquet"
                            backup_filenames.append(backup_filename)

                    sample_key = day + key + "_" + sampling_technique + "_sample_" + sampling_percentage
                    dfs[sample_key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                                           backup_files,
                                                                                           recreate_even_if_already_exist,
                                                                                           filename, backup_filenames,
                                                                                           HDFS_datafolder)

    # read non-sampled dataset if not in DEV
    if not dev:
        for key in datasets:
            filename = featureset_export_prefix + key + ".parquet"
            backup_filename = backup_featureset_prefix + key + ".parquet"
            dfs[key], previous_files, backup_files = help_import_dataframes(spark, previous_files,
                                                                            backup_files,
                                                                            recreate_even_if_already_exist, filename,
                                                                            backup_filename, HDFS_datafolder)

    print(f"Done with reading dataframes! Read {len(previous_files)} previous versions and {len(backup_files)} backups."
          f" \nPrevious versions: {previous_files}; \nbackups: {backup_files}.")
    files_to_be_changed = set(
        [dfn.replace(backup_featureset_prefix, "").replace(".parquet", "") for dfn in backup_files])
    return dfs, files_to_be_changed
