import os


def export_dataframes(dfs, featureset_export_prefix, HDFS_datafolder, files_to_be_exported=None):

    if files_to_be_exported is None:
        files_to_be_exported = list(list(dfs.keys()))

    for key in files_to_be_exported:
        # https://forums.databricks.com/questions/21830/spark-how-to-simultaneously-read-from-and-write-to.html
        dfs[key].cache() # cache to avoid FileNotFoundException
        print(key, dfs[key].count()) # light action
        filename = featureset_export_prefix+key+".parquet"
        dfs[key].write.parquet(os.path.join(HDFS_datafolder, filename), mode = "overwrite")
        print(f"{filename} done and saved.")
