import pyspark.sql.functions as f


def user_proxy_count_positive_and_negative_tweet_elements(df,
                                               engagements = ['like', 'reply', 'retweet', 'quote', 'react'],
                                               column_name="hashtags"):

    """
    TODO: Update the description
    Count the number of positive and negative engagements for each tweet element (hashtag, link, domain) in <df>.

    Parameters
    ----------
    df -- the base dataframe (e.g. val)
    group_column -- expected "engaging_user_id" or "engaged_with_user_id", depending on who is the target
    engagements -- list of expected engagements
    column_name -- target column name (expected "hashtags", "present_links", or "present_domains")
    prefix -- used to indicate the value of <group_column> in the resulting column's name

    Returns
    -------
    A new dataframe with one row for every user from the column specified in <group_column> and the count
    """

    # split the list at \t https://prnt.sc/mN67KZtqDsJ4 https://prnt.sc/bJwNnJulO0Pi
    df_sub = df.select(["engaged_with_user_id", *engagements, f.split(f.col(column_name), "\t").alias("help")])

    # https://prnt.sc/6bnxGMzbQUBT
    exploded = df_sub.select("engaged_with_user_id", *engagements, f.explode(f.col("help")).alias("item"))

    prefix = column_name.replace("present_", "")

    # https://prnt.sc/6Wkq6dFWp3Z_
    all_engagements  = [f.count("*").alias(prefix+"_user_proxy_count_all_tweets")]
    positive_engagements  = [f.count(f.when(f.col(eng) == 1, True)).alias(prefix+"_user_proxy_count_positive_tweets_"+eng) for eng in engagements]
    negative_engagements = [f.count(f.when(f.col(eng) == 0, True)).alias(prefix+"_user_proxy_count_negative_tweets_"+eng) for eng in engagements]
    expressions = all_engagements + positive_engagements + negative_engagements  # append lists
    counted = exploded.groupby("item").agg(*expressions)

    # https://prnt.sc/5SkYNtKXzby2
    joined = exploded.join(counted, on="item", how="left")
    created_columns = [new_columns for new_columns in counted.columns if new_columns != "item"]
    sums = [f.sum(f.col(c)).alias(c) for c in created_columns]
    summed = joined.groupby("engaged_with_user_id").agg(*sums)

    return summed
