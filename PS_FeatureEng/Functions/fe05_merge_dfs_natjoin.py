from Functions.pyspark_df_shape import pyspark_df_shape


def merge_dfs_natjoin(main_new_df, other_new_dfs, print_info=True, final_df_name="Final dataframe name not specified"):
    """
    Merges the dataframe in <main_new_df> with all dataframes in <other_new_dfs> sequentially using natural/inner join.

    Parameters
    ----------
    main_new_df: the starting dataframe
    other_new_dfs: other new dataframes to be merged with <main_new_df> using natural join
    print_info: whether to print the intermediate information, e.g. for debugging purposes
    final_df_name: the name of the resulting df (only relevant if <print_info> is true>

    Returns The resulting joined dataframe <df>
    -------
    """

    df = main_new_df
    keys = ["tweet_id", "engaging_user_id"]

    for other in other_new_dfs:
        new_columns = [cols for cols in other.columns if cols not in df.columns]
        other_selected = other.select(keys + new_columns)
        df = df.join(other_selected, on=keys, how="inner")

    if print_info:
        print(f"The joined dataframe {final_df_name} has shape {pyspark_df_shape(df)}")

    return df
