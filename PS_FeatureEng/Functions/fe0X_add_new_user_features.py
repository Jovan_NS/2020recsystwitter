def add_new_user_features(df, new_features_df, df_user_id = "engaged_with_user_id", new_features_id = "engaged", default_value = 0):
    """
    Takes a dataframe <new_features_df> with unique user ids in the column <new_features_id> and appends those features
    to the corresponding users in column <df_user_id> of the dataframe <df>. It is expected but not mandated or checked
    that <new_features_id> in <new_features_df> is unique, but that <df_user_id> in <df> is not.

    Parameters
    ----------
    df -- the base dataframe (e.g. val)
    new_features_df -- the dataset with features (e.g. counted positive interaction in training)
    df_user_id -- the name of the target user column in <df>
    new_features_id -- the name of the target user column in <new_features_df>
    default_value -- default value for the new column in <df> for users that do not appear in <new_features_df>

    Returns
    -------
    A dataframe corresponding to the <df> with added columns

    """
    
    if(type(df_user_id) == str) and (type(new_features_id) == str):
        new_features_df = new_features_df.withColumnRenamed(new_features_id, df_user_id)
    elif df_user_id != new_features_id:
        raise ValueError("For lists rather than strings, the caller must ensure the names are equal!")
    
    df = df.join(new_features_df, on=df_user_id, how="left_outer")
    
    if default_value is not None:
        new_columns = new_features_df.columns
        # The lines below only removes the key column from the list of columns to be padded, not from the dataframe!
        if type(df_user_id)==str:
            new_columns.remove(df_user_id)
        else:
            for c in df_user_id:
                new_columns.remove(c)

        df = df.fillna(default_value, subset=new_columns)
        
    return df
