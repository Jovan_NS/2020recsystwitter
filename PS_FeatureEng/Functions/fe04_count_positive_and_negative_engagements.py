import pyspark.sql.functions as f


def count_positive_and_negative_engagements(df, group_column = 'engaging_user_id',
                                            engagements = ['like', 'reply', 'retweet', 'quote', 'react'],
                                            prefix = "engaging_"):

    """
    Count the number of positive and negative engagements and authored tweets for all users in the dataframe <df>.

    Parameters
    ----------
    df -- the base dataframe (e.g. val)
    group_column -- expected "engaging_user_id" or "engaged_with_user_id", depending on who is the target
    engagements -- list of expected engagements
    prefix -- used to indicate the value of <group_column> in the resulting column's name

    Returns
    -------
    A new dataframe with one row for every user from the column specified in <group_column> and the count
    """

    # http://prntscr.com/1x966pb


    # https://prnt.sc/W3UrRvrKai-y
    df_sub = df.select([group_column, *engagements])

    
    # http://prntscr.com/1x966pb
    all_engagements = [f.count("*").alias(prefix+"count_all_tweets")]
    positive_engagements = [f.count(f.when(f.col(eng) == 1, True)).alias(prefix+"count_positive_tweets_"+eng) for eng in engagements]
    negative_engagements = [f.count(f.when(f.col(eng) == 0, True)).alias(prefix+"count_negative_tweets_"+eng) for eng in engagements]
    expressions = all_engagements + positive_engagements + negative_engagements   # append lists
    
    result_df = df_sub.groupby(group_column).agg(*expressions)
    
    return result_df
