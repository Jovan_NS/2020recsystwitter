from pyspark.sql.functions import col


def ratio_of_two_features(df, col1, col2, ratio_name, add_1=None):
    """
    Calculates the ratio of columns <col1> and <col2> of <df>. If <add_1> is True, 1 will first be added to the two
    columns to avoid the division-by-zero error. If it is set to None, this will only be done if at lest one cell in
    <col2> is zero. If <add_1> is set to false and at least one cell in <col2>, the error will be thrown.

    Parameters
    ----------
    df: the source and target dataframe
    col1: the dividend column
    col2: the divisor column
    ratio_name: the name of the new column
    add_1: whether to add 1 to <col1> and <col2> before the division

    Returns The dataframe <df> with a new column for the ratio
    -------
    """

    if (col1 not in df.columns) or (col2 not in df.columns):
        raise ValueError(f"Missing columns! {col1} in df.columns=={col1 in df.columns}; {col2} in df.columns=="
                         f"{col2 in df.columns}\nAll columns of df: {df.columns}")

    if add_1 is None:
        try:
            return df.withColumn(ratio_name, col(col1) / col(col2))
        except:
            return df.withColumn(ratio_name, (col(col1)+1) / (col(col2)+1))

    elif add_1:
        return df.withColumn(ratio_name, (col(col1)+1) / (col(col2)+1))

    else:
        return df.withColumn(ratio_name, col(col1) / col(col2))
