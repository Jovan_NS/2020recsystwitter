import pyspark.sql.functions as f
from pyspark.sql import Window
import sys


hours = lambda i: int(i * 3600)


def resolve_column_naming(column_name, new_col_prefix, new_col_suffix):
    if (new_col_prefix is not None) and (new_col_prefix != "") and (new_col_prefix[-1] != "_"):
        new_col_prefix = new_col_prefix + "_"

    if (new_col_suffix is not None) and (new_col_suffix != "") and (new_col_suffix[0] != "_"):
        new_col_suffix = "_" + new_col_suffix

    new_col = new_col_prefix + column_name.replace("present_", "") + "_frequency" + new_col_suffix

    return new_col


def get_engagements_frequency(target_df, column_name, source_df=None,
                              engs=['like', 'reply', 'retweet', 'quote', 'react',],
                              group_by_cols=["item"], prior_hours=None, new_col_prefix="", new_col_suffix=""):
    """
    Set group_by_cols = "item" to get values for all tweets in the dataset. Set it to ["engaging_user_id", "item"] to
    get values only for tweets seen by the user.
    """

    new_col = resolve_column_naming(column_name, new_col_prefix, new_col_suffix)

    start = source_df.select("tweet_id", "engaging_user_id", "engaged_with_user_id", *engs, f.split(f.col(column_name), "\t").alias("help"))
    exploded = start.select("tweet_id", "engaging_user_id", "engaged_with_user_id", *engs, f.explode(f.col("help")).alias("item"))
    sums_engs = [f.sum(key).alias(key + "_count") for key in engs]

    '''
    # Version which would be used had we had a window
    if prior_hours is not None:
        w = (Window().partitionBy(*group_by_cols).orderBy(f.col("tweet_timestamp").cast("long")).
             rangeBetween(-hours(prior_hours), 0))
    else:
        w = (Window().partitionBy(*group_by_cols).orderBy(f.col("tweet_timestamp").cast("long")).
             rangeBetween(-sys.maxsize, 0))
    
    sums = exploded.select(*group_by_cols, "tweet_timestamp", *sums_engs, f.count(f.col("*")).over(w).alias("total_count"))
    '''

    sums_engs = sums_engs + [f.count(f.col("*")).alias("total_count")]
    sums = exploded.groupBy(group_by_cols).agg(*sums_engs)

    # no need for
    # joined = exploded.join(sums, on=(group_by_cols+["tweet_timestamp"]))
    # as we grouped by group_by_cols for the whole source_df above
    joined = exploded.join(sums, on=group_by_cols)

    sums_of_sums = [f.sum((key+"_count")).alias(key+"_"+new_col) for key in (engs + ["total"])]
    # "tweet_id", "engaging_user_id" together identify an instance uniquely
    final = joined.groupBy("tweet_id", "engaging_user_id").agg(*sums_of_sums)
    target_df = target_df.join(final, on=["tweet_id", "engaging_user_id"], how="leftouter")

    return target_df
