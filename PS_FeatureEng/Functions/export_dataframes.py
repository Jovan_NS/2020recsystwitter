import os
from datetime import datetime


def export_dataframes(dfs, featureset_export_prefix, HDFS_datafolder, files_to_be_exported=None):
    """
    Exports PySpark dataframe into a .parquet file.

    Parameters
    ----------
    dfs: a dict of dataframes to be exported
    featureset_export_prefix: the filename prefix
    HDFS_datafolder: the folder to store the files
    files_to_be_exported: a subset of keys from <dfs> to be exported (if none, all dataframes in <dfs> will be exported)

    Returns
    -------
    """

    if files_to_be_exported is None:
        files_to_be_exported = list(list(dfs.keys()))

    for key in files_to_be_exported:
        # cf. https://forums.databricks.com/questions/21830/spark-how-to-simultaneously-read-from-and-write-to.html
        dfs[key].cache()  # cache to avoid FileNotFoundException
        print(f"Now at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')} exporting: {key}, ({dfs[key].count()}, {len(dfs[key].columns)})")  # light action
        filename = featureset_export_prefix+key+".parquet"
        dfs[key].write.parquet(os.path.join(HDFS_datafolder, filename), mode = "overwrite")
        print(f"{filename} done and saved.")
