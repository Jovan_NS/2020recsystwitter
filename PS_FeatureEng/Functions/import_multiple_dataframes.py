import os
from py4j.protocol import Py4JJavaError
from pyspark.sql.utils import AnalysisException


def help_import_multiple_dataframes(spark, filename: str, read_files: list, unread_files: list,
                                    filename_splits_that_can_be_missing: list, print_info: bool, hdfs_datafolder: str):
    """
    This function imports a single dataframe. If that is not possible, a backup is returned. If that too fails for
    all backups, the error is forwarded. Depending on whether the original dataframe or a backup was loaded,
    the respective filename is appended to the list in either read_files or backup_files, respectively.

    Parameters
    ----------
    spark: spark environment
    filename: the name of the dataframe file to be read
    read_files: list of dataframe files already loaded
    unread_files: list of filenames that were supposed loaded, but could not be found and whose name contains a
        substring from <filename_splits_that_can_be_missing>
    filename_splits_that_can_be_missing: if this is an iterable and not a None, all missing files that contain that
        string will be ignored and will not result in an error
    print_info: whether to print the intermediate information, e.g. for debugging purposes
    hdfs_datafolder: the path to the folder containing files to be loaded

    Returns the loaded dataframe <df>, updated <read_files>, updated <unread_files>
    -------
    """

    file_loaded = False

    try:
        df = spark.read.parquet(os.path.join(hdfs_datafolder, filename))
        read_files.append(filename)
        file_loaded = True
        if print_info:
            print(f"Read {filename} of shape ({df.count()}, {len(df.columns)}).")
    except (Py4JJavaError, AnalysisException) as e:
        pass

    if not file_loaded:
        ok_to_be_missing = False
        if check_if_iterable_or_none(filename_splits_that_can_be_missing) and (filename_splits_that_can_be_missing is
                                                                               not None) and filename_splits_that_can_be_missing:
            for split_name in filename_splits_that_can_be_missing:
                if split_name in filename:
                    ok_to_be_missing = False
                    unread_files.append(filename)
                    print(f"File {filename} could not be found but this is OK because the name contains {split_name}.")
                    break

        if not ok_to_be_missing:
            raise ValueError("The dataframe <"+filename+"> was supposed to be found, but wasn't!")

    return df, read_files, unread_files


def check_if_iterable_or_none(var):
    """
    Checks that var is an iterable or None.
    """

    if var is None:
        return True

    try:
        _ = iter(var)
        return True
    except TypeError:
        return False


def import_multiple_dataframes(spark, filenames=None, filename_mains=None,
                               filename_prefixes=["Encoding_", "GraphBased_", "Time_", "Engagement_"],
                               filename_suffixes=[".parquet", ],
                               filename_splits_that_can_be_missing=["val+test", ],
                               print_info = True,
                               hdfs_datafolder="Data"):
    """
    This function imports multiple dataframes with engineered features and retruns them.

    Parameters
    ----------
    spark: spark environment
    filenames: an iterable with full filenames to be loaded (if this is not None, all <filename_*> variables will be
        ignored!)
    filename_mains: an iterable with the main parts of the filenames (eg. {'test_random_sample_1pct',
        'train_random_sample_1pct',  'val+test_random_sample_1pct',  'val_random_sample_1pct'})
    filename_prefixes: an iterable of filename prefixes to be attacked before the filename main to create the filename
    filename_suffixes: an iterable of filename suffixes to be attached after the filename main to create the filename
    filename_splits_that_can_be_missing: if this is an iterable and not a None, all missing files that contain that
        string will be ignored and will not result in an error
    print_info: whether to print the intermediate information, e.g. for debugging purposes
    hdfs_datafolder: the path to the folder containing files to be loaded

    Returns a dictionary with loaded dataframes <dfs>
    -------
    """

    if ((not check_if_iterable_or_none(filenames)) or (not check_if_iterable_or_none(filename_mains)) or
            (not check_if_iterable_or_none(filename_prefixes)) or
            (not check_if_iterable_or_none(filename_suffixes)) or
            (not check_if_iterable_or_none(filename_splits_that_can_be_missing))):
        raise ValueError("<filenames>, <filename_mains>, <filename_prefixes>, <filename_suffixes>, "
                         "<filename_splits_that_can_be_missing>  must be either None or iterables.")

    if filenames is None:
        if filename_mains is None:
            raise ValueError("<filenames> and <filename_mains> cannot both be None.")
        else:
            filenames = list()
            for main in filename_mains:
                for prefix in filename_prefixes:
                    for suffix in filename_suffixes:
                        filenames.append(prefix + main + suffix)
    elif filename_mains is not None:
        print("<filenames> is not None, so <filename_*> will all be ignored.")

    dfs = {}
    read_files = []
    unread_files = []

    for filename in filenames:
        # "explain.this".split(".")[0] == "explain" https://prnt.sc/PDZpIjLyaonc
        sample_key = filename.split(".")[0]
        dfs[sample_key], read_files, unread_files = help_import_multiple_dataframes(spark=spark, filename=filename,
                                                                                   read_files=read_files,
                                                                                   unread_files=unread_files,
                                                                                   filename_splits_that_can_be_missing=filename_splits_that_can_be_missing,
                                                                                   print_info=print_info,
                                                                                   hdfs_datafolder=hdfs_datafolder)

    return dfs
