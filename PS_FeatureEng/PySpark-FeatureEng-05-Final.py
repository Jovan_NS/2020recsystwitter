#!/usr/bin/env python
# coding: utf-8

# # 05 Final
# 
# Here we join the features calculated in previous notebooks make sure everything is in order for the next steps in the pipeline.

# In[1]:


# PySpark import
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pyarrow.parquet as pq # https://sparkbyexamples.com/spark/spark-read-write-dataframe-parquet-example/
import pyspark.sql.functions as f
import pyspark.sql.types as t
from operator import add

from Functions.import_dataframes import import_dataframes
from Functions.export_dataframes import export_dataframes
from Functions.check_if_columns_are_missing import check_if_columns_are_missing
from Functions.prepare_for_new_columns import prepare_for_new_columns
from Functions.pyspark_df_shape import pyspark_df_shape


# In[2]:


# Other imports
import os
import string
import pandas as pd
import copy


# In[3]:


# If on the TU Wien's LBD cluster use yarn, otherwise use local
working_on_cluster = "adbs20" in os.path.abspath(os.getcwd())
master = "yarn" if "adbs20" in os.path.abspath(os.getcwd()) else "local"

# Initialise Spark session 
spark = SparkSession     .builder     .appName("Master_01528091_FeatureEng_05_Final")     .master(master)     .config("spark.executor.memory", "8G")     .config("spark.driver.memory", "5G")     .config("spark.driver.maxResultSize", "5G")     .config("spark.dynamicAllocation.initialExecutors", 4)     .config("spark.executor.cores", 8)     .config("spark.sql.broadcastTimeout", 900)     .config('spark.sql.session.timeZone', 'UTC')     .config('spark.driver.extraJavaOptions', '-Duser.timezone=UTC')     .config('spark.executor.extraJavaOptions', '-Duser.timezone=UTC')     .getOrCreate()


# In[4]:


sc = spark.sparkContext
sc


# ## Import Data

# In[5]:


# General setings
RECREATE_EVEN_IF_ALREADY_EXIST = True 
CALCULATE_STEPS = True # calculate inbetween steps - provides more transparent progress, but might slow down the run
DEV = True # set to true to use smaller datasets
FEATURESET_EXPORT_PREFIX = "Final_" # import-export filename prefix for features created here
BACKUP_PREFIX = "Encoding_" # in case a file with this prefix was not generated yet, load it from files with this prefix

HDFS_DATAFOLDER = "Data" if working_on_cluster else os.path.join("..", "Data")
SAMPLING_TECHNIQUES = ("random", "EWU", "EU", "inter_EWU+EU", "tweet")
SAMPLING_PERCENTAGES = ("1pct", "2pct", "5pct", "10pct")
TRAIN_NAME = "train"
VAL_NAME = "val"
TEST_NAME = "test"
VT_NAME = "val+test"

# note the distintion below in comparison to FE 00 and 01
IMPORT_DATASETS = (TRAIN_NAME, VAL_NAME, TEST_NAME, VT_NAME)


# In[6]:


dfs, changed_dfs = import_dataframes(spark, datasets=IMPORT_DATASETS,
       sampling_techniques=SAMPLING_TECHNIQUES,
       sampling_percentages=SAMPLING_PERCENTAGES,
       featureset_export_prefix=FEATURESET_EXPORT_PREFIX,
       backup_featureset_prefixes=BACKUP_PREFIX,
       recreate_even_if_already_exist=RECREATE_EVEN_IF_ALREADY_EXIST, 
       HDFS_datafolder=HDFS_DATAFOLDER, dev=DEV)


# In[7]:


if DEV:
    trn_key = TRAIN_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
    val_key = VAL_NAME + "_" + SAMPLING_TECHNIQUES[0] + "_sample_" + SAMPLING_PERCENTAGES[0]
else:
    trn_key = TRAIN_NAME
    val_key = VAL_NAME


# In[8]:


for key in dfs:
    if dfs[trn_key].columns != dfs[key].columns:
        print(key, dfs[key].columns)
        
print(trn_key, dfs[trn_key].columns)


# ### Merge Data
# 
# If some backup was loaded, then we first need to load all of the 4 resulting FE files. They must then be merged, sothat ratio features can be created.

# In[9]:


changed_dfs


# In[10]:


from Functions.import_multiple_dataframes import import_multiple_dataframes

if changed_dfs:
    new_dfs = import_multiple_dataframes(spark, filenames=None, filename_mains=changed_dfs,
                               filename_prefixes=("Encoding_", "GraphBased_", "Time_", "Engagement_"),
                               filename_suffixes=[".parquet", ],
                               filename_splits_that_can_be_missing=["val+test", ],
                               print_info=CALCULATE_STEPS,
                               hdfs_datafolder=HDFS_DATAFOLDER)
else:
    print("Since all dataframes were pre-loaded (<changed_dfs> is empty), there is nothing to merge. Running this notebook will result in no new exports.")


# In[11]:


if CALCULATE_STEPS and changed_dfs:
    for key in new_dfs:
        print(key)
        display(new_dfs[key].select([f.min(f.col("tweet_day_of_year").cast("int")), f.max(f.col("tweet_day_of_year").cast("int"))]).show())


# As we can see above (cf. https://prnt.sc/0N7zz5yMSubj), train subsets Engagement_train_* have fewer instances than others (e.g. Encoding_train_random_sample_1pct.parquet has shape (1415949, 51) while Engagement_train_random_sample_1pct.parquet is (803547, 139)). This is in Engagement, we wanted to use the first three days of the training subsets for the training and the later four days for testing (i.e. Thu, Fri, Sat -> Training; Sun, Mon, Tue, Wed -> Testing). We therefore fix this imbalance by removing Thu, Fri, and Sat (days 37, 38, and 39) from all train subsets.
# 
# 
# Meanwhile, val, test, and val+test for Time_* have more instances than others (e.g.  Encoding_val_random_sample_1pct.parquet of shape (102068, 51) and Time_val_random_sample_1pct.parquet of shape (130313, 107)). This is because we includde the last 48h of train, as it precedes the beginning time of test and val. Thus we must exclude the first 48 hours for Time_{val, test, val+test+}\_* (days 42 and 43).

# In[12]:


from Functions.fe05_remove_days import remove_days

if changed_dfs:
    for key in new_dfs:
        if ("train" in key) and ("Engagement" not in key):
            new_dfs[key] = remove_days(new_dfs[key], days_column="tweet_day_of_year", 
                                       days_to_remove=[37,38,39,], print_info=CALCULATE_STEPS,
                                       df_name=key)
        
        elif ("Time_" in key) and (("val+test" in key) or ("val" in key) or ("test" in key)):
            new_dfs[key] = remove_days(new_dfs[key], days_column="tweet_day_of_year", 
                                       days_to_remove=[42,43,], print_info=CALCULATE_STEPS,
                                       df_name=key)


# We can now see (cf. https://prnt.sc/Tq7NTl8MV_ej) that the number of instances is consitent across the dataset.
# 
# The next step is to merge the featureset datasets into one. This will be done based on Encoding_ dataset using natural join.

# In[13]:


from Functions.fe05_merge_dfs_natjoin import merge_dfs_natjoin


if changed_dfs:
    for key in new_dfs:
        if "Encoding_" in key:
            fe02 = key.replace("Encoding_", "GraphBased_")
            fe03 = key.replace("Encoding_", "Time_")
            fe04 = key.replace("Encoding_", "Engagement_")
            final_df_name = key.replace("Encoding_", FEATURESET_EXPORT_PREFIX)
            dfs[key.replace("Encoding_", "")] = merge_dfs_natjoin(new_dfs[key], [new_dfs[fe02], new_dfs[fe03], new_dfs[fe04]],
                                         print_info=CALCULATE_STEPS, final_df_name=final_df_name)


# Finally, let us look at all of the features we have:

# In[14]:


cols = [col_name for col_name in dfs[trn_key].columns]
print(cols == dfs[val_key].columns, len(cols))
print(cols)
dfs[trn_key].printSchema()


# ## Exploratory Statistics

# ### Train

# In[15]:


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].limit(20).toPandas()) 


# In[16]:


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[trn_key].describe().toPandas()) 


# ### Val

# In[17]:


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[val_key].limit(20).toPandas()) 


# In[18]:


if CALCULATE_STEPS:
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        display(dfs[val_key].describe().toPandas()) 


# ## Export Dataframes

# In[19]:


export_dataframes(dfs=dfs, featureset_export_prefix=FEATURESET_EXPORT_PREFIX, HDFS_datafolder=HDFS_DATAFOLDER, 
                  files_to_be_exported=changed_dfs)


# In[20]:


print("Done!")

