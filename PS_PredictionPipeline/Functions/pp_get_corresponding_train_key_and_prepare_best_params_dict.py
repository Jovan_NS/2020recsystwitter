def get_corresponding_train_key_and_prepare_best_params_dict(best_params, corresponding_train_key, target):
    """
    Helper function for hyperparameter tuning. It determines the corresponding df name based on which the
    hyperparameters were tuned. because cross-validation is so expensive, we will not do it on the full dataset, but
    the 10pct random sample instead; for the same reason 2 and 5 pct are not built on 1pct as well.
    It also checks that the <best_params> dict is not empty at the appropriate depth

    Parameters
    ----------
    best_params: the dictionary with the hyper-parameters to be tuned or reloaded
    corresponding_train_key: the df name for the training dataset
    target: the appropriate engagement

    Returns
    -------
    corresponding_cv_df, best_params
    """

    if corresponding_train_key in ('train', 'val', 'test', 'val+test',):
        corresponding_cv_df = corresponding_train_key + "_random_sample_10pct"
    else:
        corresponding_cv_df = corresponding_train_key.replace("2pct", "1pct").replace("5pct", "1pct")
    if corresponding_cv_df not in best_params:
        best_params[corresponding_cv_df] = {}
    if target not in best_params[corresponding_cv_df]:
        best_params[corresponding_cv_df][target] = {}
    return corresponding_cv_df, best_params
