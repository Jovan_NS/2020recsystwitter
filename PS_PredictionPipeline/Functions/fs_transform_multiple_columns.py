import os
from Functions.export_dataframes import export_dataframes


def transform_multiple_columns(dfs, transformer_class, additional_transformer_class_args: dict,
                               transformer_model_class,
                               original_cols_list,
                               new_cols_suffix,
                               transformer_name_prefix,
                               transformer_folder,
                               hdfs_datafolder,
                               export_prefix,
                               recreate_even_if_already_exist,
                               rewrite_existing_models,
                               calculate_steps,
                               changed_dfs):
    """
    Iterates over columns of dataframe and applies (preloaded or recreated transformer).

    Parameters
    ----------
    dfs: dict of dataframes to be transformed
    transformer_class: e.g. mlf.StringIndexer
    additional_transformer_class_args: e.g. {handleInvalid:"error", stringOrderType:"frequencyDesc"}
    transformer_model_class: e.g. mlf.StringIndexerModel
    original_cols_list: list of columns to be transformed
    new_cols_suffix: the suffix for to each of the columns in <new_cols_suffix> to get <transformed_columns_list>
    transformer_name_prefix: e.g. "string_indexer", leading underscore not needed
    transformer_folder: folder name for the transformer model e.g. "StringIndexers"
    hdfs_datafolder: folder name for the data files, e.g. "Data". If None, will not be exported.
    export_prefix: prefix for the name of the data files, e.g. "Indexed_"
    recreate_even_if_already_exist: force recreation of new columns
    rewrite_existing_models: do not reuse the previously saved models of transformers
    calculate_steps: print inbetween steps - provides more transparent progress, but might slow down the run
    changed_dfs: a set of keys in dfs which are to be exported

    Returns
    -------
    Dict of new dataframes <new_dfs> and list of their new columns <transformed_columns_list>. 
    Updated <changed_dfs> is also returned.

    """
    new_dfs = {}
    transformed_columns_list = []

    for key in dfs:
        print(f"Creating or loading {transformer_name_prefix} for {key}.")
        new_dfs[key] = dfs[key]
        new_columns_added = False
        reloaded_models = []
        rewritten_models = []
        for original_col in original_cols_list:
            new_col = original_col + new_cols_suffix
            transformed_columns_list.append(new_col)
            if recreate_even_if_already_exist or (new_col not in new_dfs[key].columns):
                if new_col in new_dfs[key].columns:
                    new_dfs[key] = new_dfs[key].drop(new_col)

                model_does_not_exist = False
                transformer_name = transformer_name_prefix + "_" + original_col + "_for_" + key
                transformer_path = os.path.join(transformer_folder, transformer_name)
                if not rewrite_existing_models:
                    try:
                        model = transformer_model_class.load(transformer_path)
                        reloaded_models.append(transformer_name_prefix + " for " + original_col)
                    except:
                        model_does_not_exist = True
                        rewritten_models.append(transformer_name_prefix + " for " + original_col)

                if rewrite_existing_models or model_does_not_exist:
                    transformer = transformer_class(inputCol=original_col, outputCol=new_col,
                                                    **additional_transformer_class_args)
                    model = transformer.fit(new_dfs[key])
                    model.write().overwrite().save(transformer_path)
                    
                new_dfs[key] = model.transform(new_dfs[key])
                new_columns_added = True

        print(f"\tReloaded following existing models for {key}: {reloaded_models}.")
        print(f"\t(Re)wrote the followin models for {key}: {rewritten_models}.")
        
        if new_columns_added:
            if hdfs_datafolder is None:
                changed_dfs.add(key)
            else:
                export_dataframes(dfs={key: new_dfs[key]}, featureset_export_prefix=export_prefix,
                                  HDFS_datafolder=hdfs_datafolder, files_to_be_exported={key})
                if key in changed_dfs:
                    changed_dfs.remove(key)
            

    return new_dfs, list(set(transformed_columns_list)), changed_dfs
