import pandas as pd


def update_preliminary_evals(old_evals: pd.DataFrame, new_evals_dict: dict, samples: list, classifier_prefix="tree",
                             features_note="scaled", based_on="itself", flatten=False, target_col="target",
                             eval_col="evaluation"):
    """
    This function converts new_evals into a pandas dataframe and then merges it with old_evals (overwriting potential
    duplicates).

    Parameters
    ----------
    old_evals: a Pandas dataframe with old evals with columns [based_on][algorithm][sample][target][feature_selection]
    new_evals_dict: a dict with new evals of the form {[sample]: [target]: [feature_selection]: eval_value}}}
    samples: a list of samples for which evals are to be added
    target_col: column name in the flattened form describing whose values are individual tweet engagement goals
    eval_col: "prauc", "rce", or "evaluation" expected (relevant for flattened form only)
    classifier_prefix: the natural language name of the classifier
    features_note: a note about features to be saved in evals (e.g. scaled, oracle features included, etc.)
    based_on: "train" or "itself" expected
    flatten: whether to only have one eval per row (if false, one row will contain evals for each of the target
        engagements); cf. https://prnt.sc/rG1HiIpkXTig and https://prnt.sc/Gk1sTMvAn-d3

    Returns
    -------
    <updated_evals>
    """
    idx = 0
    df_dict = {}

    ### TRANSFORM NEW EVALS INTO DATAFRAME ###
    #  First, we have to transform new_evals[sample][target][feature_selection] into
    #  eval_dict[based_on][algorithm/classifier_prefix][sample][target][feature_selection]

    for sample in samples:
        for target in new_evals_dict[sample]:
            for feature_selection in new_evals_dict[sample][target]:
                if flatten:
                    df_dict[idx] = {"algorithm": classifier_prefix, "note": features_note, "trained_on": based_on,
                                    "sample": sample, "feature_selection": feature_selection, target_col: target,
                                    eval_col: new_evals_dict[sample][target][feature_selection]}
                    idx += 1
                else:
                    if (classifier_prefix, features_note, based_on, sample, feature_selection) not in df_dict:
                        df_dict[(classifier_prefix, features_note, based_on, sample, feature_selection)] = {}

                    df_dict[(classifier_prefix, features_note, based_on, sample, feature_selection)][target] = \
                        new_evals_dict[sample][target][feature_selection]

    new_df = pd.DataFrame.from_dict(df_dict, orient='index')
    combo_columns = ["algorithm", "note", "trained_on", "sample", "feature_selection"]
    if flatten:
        combo_columns = combo_columns + [target_col]
    else:
        new_df.index.set_names(combo_columns, inplace=True)
        # We have to temporarily save indices into columns, to be able to merge with old evals easier
        new_df = new_df.reset_index()

    ### MERGE WITH THE EXISTING DF ###
    
    if old_evals is None or type(old_evals) == dict:
        return new_df

    # https://stackoverflow.com/a/47107164 ; https://stackoverflow.com/a/55543744
    all_df = old_evals.merge(new_df[combo_columns], on=combo_columns, how="left", indicator=True)
    just_relevant_old = all_df[all_df._merge == 'left_only'].drop('_merge', axis=1)
    updated_evals = pd.concat([just_relevant_old, new_df], axis="index", ignore_index=True, sort=False)

    return updated_evals
