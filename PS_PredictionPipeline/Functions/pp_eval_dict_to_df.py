import pandas as pd


def eval_dict_to_df(eval_dict, flatten=False, note="scaled", metric_name="prauc"):
    """
    Converts a nested dict into a Pandas dataframe, according to the desired format

    Parameters
    ----------
    eval_dict: a dictionary with evaluations
    flatten: whether to only have one eval per row (if false, one row will contain evals for each of the engagements)
    note: a note about features to be saved in evals (e.g. scaled, oracle features included, etc.)
    metric_name: "prauc" or "rce" expected

    Returns
    -------
    The corresponding dataframe.
    """

    idx = 0
    df_dict = {}
    for based_on in eval_dict.keys():
        based_on_value = "train" if based_on else "itself"
        for algorithm in eval_dict[based_on].keys():
            for sample in eval_dict[based_on][algorithm].keys():
                for target in eval_dict[based_on][algorithm][sample].keys():
                    for feature_selection in eval_dict[based_on][algorithm][sample][target].keys():
                        if flatten:
                            df_dict[idx] = {"algorithm": algorithm, "note": note, "trained_on":based_on_value,
                                            "sample": sample, "feature_selection": feature_selection,
                                            "target": target,
                                            metric_name: eval_dict[based_on][algorithm][sample][target][
                                                feature_selection]}
                            idx += 1
                            #  https://prnt.sc/rG1HiIpkXTig
                        else:
                            if (algorithm, note, based_on_value, sample, feature_selection) not in df_dict:
                                df_dict[(algorithm, note, based_on_value, sample, feature_selection)] = {}

                            df_dict[(algorithm, note, based_on_value, sample, feature_selection)][target] = \
                                eval_dict[based_on][algorithm][sample][target][feature_selection]
                            #  https://prnt.sc/Gk1sTMvAn-d3

    df = pd.DataFrame.from_dict(df_dict, orient='index')
    if not flatten:
        df.index.names = ("algorithm", "note", "trained_on", "sample", "feature_selection")

    return df
