def semi_coin_multiple_cols(df1, df2, key_columns=["algorithm", "note", "feature_selection", "trained_on", "evaluated_on", "target",]):
    # https://stackoverflow.com/questions/63660610/how-to-perform-semi-join-with-multiple-columns-in-pandas
    # https://prnt.sc/xhEr1uoE7FLg
    df1 = df1[df1[key_columns].agg(tuple,1).isin(df2[key_columns].agg(tuple,1))]
    df2 = df2[df2[key_columns].agg(tuple,1).isin(df1[key_columns].agg(tuple,1))]
    return df1, df2

def remove_duplicates_help(df, key_columns, actual_value_cols, keep):
    if keep=="best":
        # https://stackoverflow.com/a/41650846
        return df.sort_values(actual_value_cols, ascending=False).drop_duplicates(key_columns).sort_index()
    return df.drop_duplicates(subset=key_columns, keep=keep)


def remove_duplicates_and_inconsistencies(prauc, rce=None, key_columns=None, sorting_order=None,
                                          value_cols=None, keep="last", print_progress=True):
    # https://prnt.sc/3qx4rpZrPTH_ ; https://prnt.sc/IZ_Qtm38laum ; https://prnt.sc/w0AngKFIj8EW ;
    # https://prnt.sc/dZkK0fVFsqDI ; https://prnt.sc/ieuesmfZQL2N
    if keep not in ("best", "first", "last", False,):
        raise ValueError("<keep> must be in one of: ‘best’ ‘first’, ‘last’, False. False removes all duplicates, "
                         "while ‘best’ keeps the best score.")

    if value_cols is None:
        value_cols = ["evaluation", "like", "reply", "retweet", "quote", "react", "prauc", "rce", ]
    actual_value_cols = [col for col in value_cols if col in prauc.columns]
    if key_columns is None:
        key_columns = [col for col in prauc.columns if col not in actual_value_cols]
    ps1 = prauc.shape
    prauc = remove_duplicates_help(df=prauc, key_columns=key_columns, actual_value_cols=actual_value_cols, keep=keep)
    if rce is not None:
        rs1 = rce.shape
        rce = remove_duplicates_help(df=rce, key_columns=key_columns, actual_value_cols=actual_value_cols, keep=keep)
        ps2 = prauc.shape
        rs2 = rce.shape
        prauc, rce = semi_coin_multiple_cols(df1=prauc, df2=rce, key_columns=key_columns)
        ps3 = prauc.shape
        rs3 = rce.shape
        if print_progress:
            print(f"Shapes of PRAUC/RCE at start: {ps1}/{rs1}, \nafter removing duplicates: {ps2}/{rs2} (kept {keep}), "
                  f"\nand after removing inconsistencies={ps3}/{rs3}.")
    else:
        if print_progress:
            print(f"Shape of the file at the start: {ps1}, after removing duplicates: {prauc.shape} (kept {keep}).")

    if sorting_order is not None:
        prauc = prauc.sort_values(by=sorting_order, axis="index", ignore_index=True)
        rce = rce.sort_values(by=sorting_order, axis="index", ignore_index=True) if rce is not None else None
    else:
        prauc.reset_index(drop=True, inplace=True)
        rce = rce.reset_index(drop=True, inplace=False) if rce is not None else None

    return prauc, rce
