# prediction function from the first version that worked (October 2022):
def predict_and_evaluate(dfs, features_dict, features_name="selected_features",
                         target_features=['like', 'reply', 'retweet', 'quote', 'react', ],
                         classifier_class=mlc.DecisionTreeClassifier,
                         classifier_model_class=mlc.DecisionTreeClassificationModel,
                         classifier_prefix="tree", classifier_folder="Classifiers", models_folder="Models",
                         rewrite_existing_models=False, print_progress=True, evaluate=True):
    models = {}
    prauc_evals = {}
    rce_evals = {}
    reloaded_models = []
    rewritten_models = []

    for key in dfs:
        models[key] = {}
        prauc_evals[key] = {}
        rce_evals[key] = {}

        print(f"Creating or loading {classifier_prefix} for {key}.")

        for target in target_features:
            models[key][target] = {}
            prauc_evals[key][target] = {}
            rce_evals[key][target] = {}

            for sel_features in features_dict[key][target]:
                model_does_not_exist = False
                full_model_name = "classifier_model_of_type-" + classifier_prefix + \
                                  "-for_features-" + sel_features + \
                                  "-for_dataset-" + key + \
                                  "-based_on_dataset-" + corresponding_train_key + \
                                  "-predicting_target-" + target
                model_path = os.path.join(models_folder, full_model_name)

                if not rewrite_existing_models:
                    try:
                        models[key][target][sel_features] = classifier_model_class.load(model_path)
                        reloaded_models.append(full_model_name)
                        if print_progress:
                            print(f"\tModel {full_model_name} for {key}/{target} loaded from file.")
                    except:
                        model_does_not_exist = True
                        rewritten_models.append(full_model_name)
                        if print_progress:
                            print(f"\tModel {full_model_name} for {key}/{target} must be recreated.")

                if rewrite_existing_models or model_does_not_exist:
                    classifier_instance = classifier_class(labelCol=target, featuresCol=features_name)
                    print("\t\t\t", sel_features)
                    models[key][target][sel_features] = classifier_instance.fit(
                        dfs[corresponding_train_key][sel_features])
                    models[key][target][sel_features].write().overwrite().save(model_path)

                    if print_progress:
                        print(
                            f"\t\tTraining {full_model_name} for {key}/{target} based on {corresponding_train_key} done.")

                if evaluate:
                    predictions = models[key][target][sel_features].transform(dfs[key][sel_features])
                    pred = [x[0] for x in predictions.select("prediction").collect()]
                    gt = [x[0] for x in predictions.select(target).collect()]
                    if print_progress:
                        print("\t\tPredicting & collecting {} done.".format(target))

                    pred = [x[0] for x in predictions.select("prediction").collect()]
                    gt = [x[0] for x in predictions.select(target).collect()]
                    prauc_evals[key][target][sel_features] = compute_prauc(pred, gt)
                    rce_evals[key][target][sel_features] = compute_rce(pred, gt)
                    if print_progress:
                        print("\t\tEvaluating {} done.".format(target))

    return models, prauc_evals, rce_evals