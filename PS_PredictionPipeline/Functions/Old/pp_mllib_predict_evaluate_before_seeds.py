import pyspark.ml.classification as mlc  # for LogisticRegression, LogisticRegressionModel, etc.
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
from py4j.protocol import Py4JJavaError
from pyspark.sql.utils import AnalysisException

import os
from datetime import datetime
import pandas as pd
import numpy as np

from Functions.pp_calculate_evaluation_metrics import compute_prauc, compute_rce
from Functions.pp_get_already_existing_evals import get_already_existing_evals
from Functions.pp_update_evals import update_evals
from Functions.pp_mllib_rce_evaluator import RCEEvaluator
from Functions.pickle_file import pickle_file
from Functions.unpickle_file import unpickle_file


def mllib_predict_evaluate(dfs, features_dict, features_name="selected_features",
                  target_features=('like', 'reply', 'retweet', 'quote', 'react', ),
                  classifier_class=mlc.DecisionTreeClassifier,
                  classifier_model_class=mlc.DecisionTreeClassificationModel,
                  classifier_prefix="tree", models_folder="Models", force_model_reloading=False, features_note="scaled",
                  csv_folder="Evals_csv", pickle_folder="Eval_pkl", flatten=False, base_predictions_on_train_dfs=True,
                  rewrite_existing_models=False, hypertune_params=None, dev=False, print_progress=True):
    """
    This function loops for each dataframe in <dfs> and predicts targets from <target_features> based on explanatory
    features listed in <features_dict>. It uses the classifier from PySpark's mllib specified in <classifier_class>
    with the corresponding model's class specified in <classifier_model_class>.

    Parameters
    ----------
    dfs: a dictionary of dataframes with a vectorised column with explanatory features
    features_dict: a dictionary with the selected description features
    features_name: the name of the vectorised column with explanatory features
    target_features: a list of target labels
    classifier_class: the class of the classifier from PySpark's mllib
    classifier_model_class: the class of the model of the classifier from PySpark's mllib
    classifier_prefix: the natural language name of the classifier
    models_folder: the name of the folder to save fitted classifier models in
    force_model_reloading: whether to reload a model if its evaluation already exists
    features_note: a note about features to be saved in evals (e.g. "scaled", "oracle", etc.)
    csv_folder: the name of the folder to save/load evaluations in/from .csv format (if not None)
    pickle_folder: the name of the folder to save/load evaluations in/from .csv format (if not None)
    flatten: whether to only have one eval per row (if false, one row will contain evals for each of the target
        engagements); cf. https://prnt.sc/rG1HiIpkXTig and https://prnt.sc/Gk1sTMvAn-d3
    base_predictions_on_train_dfs: whether to fit on the corresponding train dataset instead of the same dataset
    rewrite_existing_models: whether to recreate, refit, and rewrite classifier models
    hypertune_params: the grid of hyperparameters to tune of form [(param1, value1), (param2, value2,),]
        or None if no tuning ought to be done
    dev: whether dev mode is used; if True, "dev-" prefix will be used for CSV/pickle imports and exports
    print_progress: whether to print the fitting and prediction progress

    Returns
    -------
    fitted <models>, the sets <reloaded_models_set>, <rewritten_models_set>, and <skipped_models_set> as well as the
    updated evaluations <prauc_evals> and <rce_evals>
    """

    ### LOAD PREEXISTING EVALS ###
    based_on = "train" if base_predictions_on_train_dfs else "itself"
    hypertune_suffix = "-ht" if (hypertune_params is not None) else ""
    eval_path = "classifier-" + classifier_prefix + "-based_on-" + based_on + "-" + features_note + hypertune_suffix
    eval_path = "dev-" + eval_path if dev else eval_path
    params_path = "params_for_classifier-" + classifier_prefix + "-based_on-" + based_on + "-" + features_note
    if csv_folder is not None:
        eval_path = os.path.join(csv_folder, eval_path)

    if not rewrite_existing_models:
        try:
            prauc_evals = pd.read_csv(eval_path + "-prauc.csv")
            rce_evals = pd.read_csv(eval_path + "-rce.csv")
            already_existing_evals = get_already_existing_evals(prauc_evals, rce_evals, flatten)
            if print_progress:
                print(f"Read prauc evals {prauc_evals.shape} and rce evals {rce_evals.shape}.")

        except FileNotFoundError:
            prauc_evals = {}
            rce_evals = {}
            already_existing_evals = set()
            if print_progress:
                print(f"{eval_path}-prauc.csv/-rce.csv do not exist.")
    else:
        prauc_evals = None
        rce_evals = None
        already_existing_evals = set()
        if print_progress:
            print(f"Recreating and reevaluating models for {classifier_prefix}")

    ### LOAD PREEXISTING MODELS ###
    models = {}
    reloaded_models_set = set()
    rewritten_models_set = set()
    skipped_models_set = set()
    # for new evaluations:
    new_prauc = {}
    new_rce = {}
    # for hyperparameter tuning:
    if (not rewrite_existing_models) and (pickle_folder is not None) and (hypertune_params is not None):
        best_params = unpickle_file(name=params_path, path=pickle_folder, print_confirmation=print_progress, dev=dev)
        if best_params is None:
            best_params = {}
    else:
        best_params = {}

    for sample in dfs:
        models[sample] = {}
        new_prauc[sample] = {}
        new_rce[sample] = {}
        corresponding_train_key = sample.replace("val+test", "train").replace("val", "train").replace("test", "train") \
            if base_predictions_on_train_dfs else sample

        for target in target_features:
            models[sample][target] = {}
            new_prauc[sample][target] = {}
            new_rce[sample][target] = {}

            for feature_selection in features_dict[sample][target]:
                if flatten:
                    next_combo = (classifier_prefix, features_note, based_on, sample, feature_selection, target,)
                    # e.g. ('bayes', 'scaled', 'itself', 'test_random_sample_1pct', 'all', 'like')
                else:
                    next_combo = (classifier_prefix, features_note, based_on, sample, feature_selection,)
                    # e.g. ('bayes', 'scaled', 'itself', 'test_random_sample_1pct', 'all')

                ### PREDICT ###
                full_model_name = "classifier_model_of_type-" + classifier_prefix + \
                                  "-for_features-" + feature_selection + \
                                  "-for_dataset-" + sample + \
                                  "-based_on_dataset-" + corresponding_train_key + \
                                  "-predicting_target-" + target + \
                                  hypertune_suffix
                model_path = os.path.join(models_folder, full_model_name)

                # Attempt to load the model if allowed and it exists
                if rewrite_existing_models:
                    evaluate_model = True
                    create_model = True
                elif force_model_reloading or (next_combo not in already_existing_evals):
                    try:
                        models[sample][target][feature_selection] = classifier_model_class.load(model_path)
                        reloaded_models_set.add(full_model_name)
                        evaluate_model = True
                        create_model = False
                        if print_progress:
                            print(f"\tLoaded {full_model_name} at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")
                    except (Py4JJavaError, FileNotFoundError, AnalysisException) as error:
                        evaluate_model = True
                        create_model = True
                        if print_progress:
                                print(f"\tModel {full_model_name} does not exist, even though its evaluation does.")
                else:
                    evaluate_model = False
                    create_model = False
                    skipped_models_set.add(full_model_name)

                # Recreate the model if necessary
                if create_model:
                    if hypertune_params is None:
                        classifier_instance = classifier_class(labelCol=target, featuresCol=features_name)
                        models[sample][target][feature_selection] = classifier_instance.fit(
                            dfs[corresponding_train_key][feature_selection])
                        models[sample][target][feature_selection].write().overwrite().save(model_path)
                        rewritten_models_set.add(full_model_name)
                    else:  # do the hyperparameter tuning: https://spark.apache.org/docs/2.4.1/ml-tuning.html
                        # because cross-validation is so expensive, we will not do it on the full dataset, but the
                        # 10pct random sample instead; for the same reason 2 and 5 pct are not built on 1pct as well
                        if corresponding_train_key in ('train', 'val', 'test', 'val+test',):
                           corresponding_cv_df = corresponding_train_key + "_random_sample_10pct"
                        else:
                            corresponding_cv_df = corresponding_train_key.replace("2pct", "1pct").replace("5pct", "1pct")

                        if corresponding_cv_df not in best_params:
                            best_params[corresponding_cv_df] = {}

                        if target not in best_params[corresponding_cv_df]:
                            best_params[corresponding_cv_df][target] = {}

                        if feature_selection not in best_params[corresponding_cv_df][target]:
                            grid = ParamGridBuilder()
                            default_classifier_instance = classifier_class(labelCol=target, featuresCol=features_name)
                            for hp in hypertune_params:  # https://prnt.sc/YfXJjC59WeSO
                                grid = grid.addGrid(getattr(default_classifier_instance, hp), hypertune_params[hp])
                                # For a view of how this loop creates the hyperparameter grid, please cf.:
                                # https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-by-using-its-name-a-string
                                # and https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.ml.tuning.ParamGridBuilder.htm

                            grid = grid.build()
                            # https://stackoverflow.com/questions/51404344/custom-evaluator-in-pyspark
                            crossval = CrossValidator(estimator=default_classifier_instance,
                                                      estimatorParamMaps=grid,
                                                      evaluator=RCEEvaluator(predictionCol="prediction", labelCol=target),
                                                      numFolds=4,
                                                      seed=42)
                            # Run cross-validation, and choose the best set of parameters.
                            cv_model = crossval.fit(dfs[corresponding_cv_df][feature_selection])

                            # save the parameters into the dict:
                            # https://stackoverflow.com/questions/36697304/how-to-extract-model-hyper-parameters-from-spark-ml-in-pyspark
                            # https://prnt.sc/ekyr3-DU7WRZ ; https://prnt.sc/Pf66jKUYvunY ; https://prnt.sc/pn0eChJvBf1W
                            # ; https://prnt.sc/ppib34NKmD2a
                            best_params_map = cv_model.bestModel.extractParamMap()
                            best_params[corresponding_cv_df][target][feature_selection] = {k.name:best_params_map[k] for k in best_params_map.keys()}
                            #print("The best params ->", best_params[corresponding_cv_df][target][feature_selection])
                            #return crossval, cv_model, best_params_map
                            pickle_file(var=best_params, name=params_path, path=pickle_folder, print_confirmation=print_progress, dev=dev)

                        tuned_classifier_instance = classifier_class(**best_params[corresponding_cv_df][target][feature_selection])
                        models[sample][target][feature_selection] = tuned_classifier_instance.fit(
                            dfs[corresponding_train_key][feature_selection])
                        models[sample][target][feature_selection].write().overwrite().save(model_path)
                        rewritten_models_set.add(full_model_name)

                    if print_progress:
                        print(f"\t\tTraining {full_model_name} done at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

                ### EVALUATE ###
                if evaluate_model:
                    predictions = models[sample][target][feature_selection].transform(dfs[sample][feature_selection])
                    pred = [x[0] for x in predictions.select("prediction").collect()]
                    gt = [x[0] for x in predictions.select(target).collect()]
                    new_prauc[sample][target][feature_selection] = compute_prauc(pred, gt)
                    new_rce[sample][target][feature_selection] = compute_rce(pred, gt)
                    if print_progress:
                        print(f"\t\tEvaluation done for {sample}/{feature_selection}/{target}: "
                              f"{new_prauc[sample][target][feature_selection]}/{new_rce[sample][target][feature_selection]} "
                              f"at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

        if (new_prauc[sample] != {}) and (new_prauc[sample][target_features[0]] != {}):
            prauc_evals = update_evals(old_evals=prauc_evals, new_evals_dict=new_prauc, samples=[sample],
                                               metric_name="prauc", classifier_prefix=classifier_prefix, based_on=based_on,
                                               features_note=features_note, eval_path=eval_path, flatten=flatten)
            rce_evals = update_evals(old_evals=rce_evals, new_evals_dict=new_rce, samples=[sample], metric_name="rce",
                                             classifier_prefix=classifier_prefix, based_on=based_on,
                                             features_note=features_note, eval_path=eval_path, flatten=flatten)
            prauc_evals.to_csv(eval_path + "-prauc.csv", index=False)
            rce_evals.to_csv(eval_path + "-rce.csv", index=False)
            if print_progress:
                print(f"___ Exported all evals for {sample} at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}. ___ ")

    if not flatten:
        prauc_evals.set_index(["algorithm", "note", "trained_on", "sample", "feature_selection"],
                          verify_integrity=True, inplace=True)
        rce_evals.set_index(["algorithm", "note", "trained_on", "sample", "feature_selection"],
                        verify_integrity=True, inplace=True)
    return models, reloaded_models_set, rewritten_models_set, skipped_models_set, prauc_evals, rce_evals
