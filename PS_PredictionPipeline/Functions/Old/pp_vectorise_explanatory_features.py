import pyspark.ml.feature as mlf


def vectorise_explanatory_features(dfs,
                                   preselected_features_dict,
                                   featureset_to_features_note_map={"relevant_features":"scaled",},
                                   features_notes_to_be_vectorised=["scaled",],
                                   top_ns_to_be_vectorised=["all"],
                                   base_predictions_on_train_dfs=True):

    vec_dfs = {}
    sfs = {}

    for key in dfs:
        if base_predictions_on_train_dfs:
            corresponding_train_key = key.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = key

        vec_dfs[key] = {}
        sfs[key] = {}

        for featureset in preselected_features_dict[corresponding_train_key]:
            note = featureset_to_features_note_map[featureset]
            if (features_notes_to_be_vectorised is None) or (note in features_notes_to_be_vectorised):
                vec_dfs[key][featureset] = {}
                sfs[key][featureset] = {}

                for eng in preselected_features_dict[corresponding_train_key][featureset]:
                    vec_dfs[key][featureset][eng] = {}
                    sfs[key][featureset][eng] = {}

                    for num_features in preselected_features_dict[corresponding_train_key][featureset][eng]:
                        if (top_ns_to_be_vectorised is None) or (num_features in top_ns_to_be_vectorised):
                            sfs[key][featureset][eng][num_features] = \
                                preselected_features_dict[corresponding_train_key][featureset][eng][num_features]
                            vec_assembler = mlf.VectorAssembler(inputCols=sfs[key][featureset][eng][num_features],
                                                                outputCol=note)
                            vec_dfs[key][featureset][eng][num_features] = vec_assembler.setHandleInvalid("error").\
                                transform(dfs[key])

    return vec_dfs, sfs
