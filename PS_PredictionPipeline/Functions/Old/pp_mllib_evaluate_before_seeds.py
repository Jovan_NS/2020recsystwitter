import os
from datetime import datetime
import pickle
from Functions.pp_calculate_evaluation_metrics import compute_prauc, compute_rce


def mllib_evaluate(dfs, models, rewrite_existing_models=True, classifier_prefix="", print_progress=True,
                   pickle_folder="Evals_pickle", base_predictions_on_train_dfs=True):
    """
    Evaluates PySpark's mllib models and saves them as .tex and .csv files.

    Parameters
    ----------
    dfs: the dataframes the models are based on
    models: the prediction models
    rewrite_existing_models: whether to reevaluate models or to load .csv evaluations if they already exist
    classifier_prefix: the natural language name of the classifier
    print_progress: whether to print the evaluation progress
    pickle_folder: the name of the folder to save/load evaluations in/from .pkl format (if not None)
    base_predictions_on_train_dfs: whether the model was fitted based on corresponding train dataset

    Returns
    -------

    """

    if pickle_folder is not None:
        based_on_dataset = "train_subset" if base_predictions_on_train_dfs else "subset_itself"
        pickle_filename_prefix = "classifier_model_of_type-" + classifier_prefix + "-based_on-" + based_on_dataset
        if not rewrite_existing_models:
            try:
                with open(os.path.join(pickle_folder, pickle_filename_prefix+"_prauc.pkl"), "rb") as f:
                    prauc_evals = pickle.load(f)

                with open(os.path.join(pickle_folder, pickle_filename_prefix+"_rce.pkl"), "rb") as f:
                    rce_evals = pickle.load(f)

                if print_progress:
                    print(f"Evaluations for reloaded from {os.path.join(pickle_folder, pickle_filename_prefix)}.")

                return prauc_evals, rce_evals
            except FileNotFoundError:
                if print_progress:
                    print(f"{os.path.join(pickle_folder, pickle_filename_prefix)}_prauc.pkl/_rce.pkl do not exist.")

    new_prauc = {}
    new_rce = {}

    for sample in models:
        if base_predictions_on_train_dfs:
            corresponding_train_key = sample.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = sample

        if print_progress:
            print(f"Evaluating predictions for {sample} based on {corresponding_train_key} at "
                  f"{datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

        new_prauc[sample] = {}
        new_rce[sample] = {}
        for target in models[sample]:
            new_prauc[sample][target] = {}
            new_rce[sample][target] = {}

            for sel_features in models[sample][target]:
                predictions = models[sample][target][sel_features].transform(dfs[sample][sel_features])
                pred = [x[0] for x in predictions.select("prediction").collect()]
                gt = [x[0] for x in predictions.select(target).collect()]
                if print_progress:
                    print(f"\tPredicting & collecting for {sample}/{sel_features}/{target} done at "
                            f"{datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

                new_prauc[sample][target][sel_features] = compute_prauc(pred, gt)
                new_rce[sample][target][sel_features] = compute_rce(pred, gt)
                if print_progress:
                    print(f"\t\tEvaluation done for {sample}/{sel_features}/{target}: "
                            f"{new_prauc[sample][target][sel_features]}/{new_rce[sample][target][sel_features]} "
                            f"at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

        if pickle_folder is not None:
            try:
                with open(os.path.join(pickle_folder, pickle_filename_prefix + "_prauc.pkl"), "wb") as f:
                    pickle.dump(new_prauc, f)

                with open(os.path.join(pickle_folder, pickle_filename_prefix + "_rce.pkl"), "wb") as f:
                    pickle.dump(new_rce, f)

                if print_progress:
                    print(f"\t\t\tEvaluations saved to {os.path.join(pickle_folder, pickle_filename_prefix)}.")

            except FileNotFoundError as e:
                print(f"Could not save the .pkl files: {e}")

    return new_prauc, new_rce
