import os
from datetime import datetime
import pandas as pd
from Functions.pp_calculate_evaluation_metrics import compute_prauc, compute_rce


def mllib_evaluate(dfs, models, rewrite_existing_models=True, classifier_prefix="", print_progress=True,
             tex_folder="Eval_tex", csv_folder="Eval_csv", base_predictions_on_train_dfs=True):
    """
    Evaluates PySpark's mllib models and saves them as .tex and .csv files.

    Parameters
    ----------
    dfs: the dataframes the models are based on
    models: the prediction models
    rewrite_existing_models: whether to reevaluate models or to load .csv evaluations if they already exist
    classifier_prefix: the natural language name of the classifier
    print_progress: whether to print the evaluation progress
    tex_folder: the name of the folder to save evaluations in .tex format (if None, it will not be saved in .tex)
    csv_folder: the name of the folder to save evaluations in .csv format (if None, it will not be saved in .csv)
    base_predictions_on_train_dfs: whether the model was fitted based on corresponding train dataset

    Returns
    -------

    """

    prauc_evals = {}
    rce_evals = {}

    for key in models:
        if base_predictions_on_train_dfs:
            corresponding_train_key = key.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = key

        if print_progress:
            print(f"Evaluating predictions for {key} based on {corresponding_train_key} at "
                  f"{datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

        prauc_evals[key] = {}
        rce_evals[key] = {}
        for target in models[key]:
            prauc_evals[key][target] = {}
            rce_evals[key][target] = {}

            for sel_features in models[key][target]:
                csv_loaded = False
                classifier_name = "classifier_model_of_type-" + classifier_prefix + \
                                  "-for_features-" + sel_features + \
                                  "-for_dataset-" + key + \
                                  "-based_on_dataset-" + corresponding_train_key + \
                                  "-predicting_target-" + target
                if (not rewrite_existing_models) and (csv_folder is not None):
                    try:
                        prauc_evals[key][target][sel_features] = pd.read_csv(
                            os.path.join(csv_folder, "prauc", classifier_name + "prauc.csv"), sep='\t', index_col=False)
                        rce_evals[key][target][sel_features] = pd.read_csv(
                            os.path.join(csv_folder, "prauc", classifier_name + "rce.csv"), sep='\t', index_col=False)
                        csv_loaded = True
                        if print_progress:
                            print(f"\tEvaluations for {key}/{sel_features}/{target} reloaded.")
                    except FileNotFoundError:
                        pass

                if not csv_loaded:
                    predictions = models[key][target][sel_features].transform(dfs[key][sel_features])
                    pred = [x[0] for x in predictions.select("prediction").collect()]
                    gt = [x[0] for x in predictions.select(target).collect()]
                    if print_progress:
                        print(f"\tPredicting & collecting for {key}/{sel_features}/{target} done at "
                              f"{datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

                    prauc_evals[key][target][sel_features] = compute_prauc(pred, gt)
                    rce_evals[key][target][sel_features] = compute_rce(pred, gt)
                    if print_progress:
                        print(f"\t\tEvaluation done for {key}/{sel_features}/{target}: "
                              f"{prauc_evals[key][target][sel_features]}/{rce_evals[key][target][sel_features]} "
                              f"at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

                    if csv_folder is not None:
                        prauc_evals[key][target][sel_features].to_csv(
                            os.path.join(csv_folder, "prauc", classifier_name + "prauc.csv"), sep='\t', index=False)
                        rce_evals[key][target][sel_features].to_csv(
                            os.path.join(csv_folder, "rce", classifier_name + "rce.csv"), sep='\t', index=False)

                    if tex_folder is not None:
                        prauc_evals[key][target][sel_features].T.to_latex(
                            os.path.join(tex_folder, "prauc", classifier_name + "prauc.tex"))
                        rce_evals[key][target][sel_features].T.to_latex(
                            os.path.join(tex_folder, "rce", classifier_name + "rce.tex"))


    return prauc_evals, rce_evals