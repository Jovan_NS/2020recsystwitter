import pyspark.ml.feature as mlf


def vectorise_explanatory_features(dfs, selected_features, base_predictions_on_train_dfs=True):

    vec_dfs = {}
    sfs = {}
    target = "like"

    for key in dfs:
        if base_predictions_on_train_dfs:
            corresponding_train_key = key.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = key

        vec_dfs[key] = {}
        sfs[key] = {}
        for num_features in selected_features[corresponding_train_key][target]:
            sfs[key][num_features] = selected_features[corresponding_train_key][target][num_features]
            vec_assembler = mlf.VectorAssembler(inputCols=sfs[key][num_features], outputCol="selected_features")
            vec_dfs[key][num_features] = vec_assembler.setHandleInvalid("error").transform(dfs[key])

    return vec_dfs, sfs
