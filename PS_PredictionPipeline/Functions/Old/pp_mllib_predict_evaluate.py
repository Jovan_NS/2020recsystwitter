import pyspark.ml.classification as mlc  # for LogisticRegression, LogisticRegressionModel, etc.
from py4j.protocol import Py4JJavaError

import os
from datetime import datetime
import pandas as pd

from Functions.pp_calculate_evaluation_metrics import compute_prauc, compute_rce
from Functions.pp_get_already_existing_evals import get_already_existing_evals
from Functions.pp_update_evals import update_evals


def mllib_predict_evaluate(dfs, features_dict, features_name="selected_features",
                  target_features=('like', 'reply', 'retweet', 'quote', 'react', ),
                  classifier_class=mlc.DecisionTreeClassifier,
                  classifier_model_class=mlc.DecisionTreeClassificationModel,
                  classifier_prefix="tree", models_folder="Models", force_model_reloading=False, features_note="scaled",
                  csv_folder="Evals_csv", flatten=False, base_predictions_on_train_dfs=True,
                  rewrite_existing_models=False, print_progress=True):
    """
    This function loops for each dataframe in <dfs> and predicts targets from <target_features> based on explanatory
    features listed in <features_dict>. It uses the classifier from PySpark's mllib specified in <classifier_class>
    with the corresponding model's class specified in <classifier_model_class>.

    Parameters
    ----------
    dfs: a dictionary of dataframes with a vectorised column with explanatory features
    features_dict: a dictionary with the selected description features
    features_name: the name of the vectorised column with explanatory features
    target_features: a list of target labels
    classifier_class: the class of the classifier from PySpark's mllib
    classifier_model_class: the class of the model of the classifier from PySpark's mllib
    classifier_prefix: the natural language name of the classifier
    models_folder: the name of the folder to save fitted classifier models in
    force_model_reloading: whether to reload a model if its evaluation already exists
    features_note: a note about features to be saved in evals (e.g. "scaled", "oracle", etc.)
    csv_folder: the name of the folder to save/load evaluations in/from .csv format (if not None)
    flatten: whether to only have one eval per row (if false, one row will contain evals for each of the target
        engagements); cf. https://prnt.sc/rG1HiIpkXTig and https://prnt.sc/Gk1sTMvAn-d3
    base_predictions_on_train_dfs: whether to fit on the corresponding train dataset instead of the same dataset
    rewrite_existing_models: whether to recreate, refit, and rewrite classifier models
    print_progress: whether to print the fitting and prediction progress

    Returns
    -------
    fitted <models>, the sets <reloaded_models_set>, <rewritten_models_set>, and <skipped_models_set> as well as the
    updated evaluations <prauc_evals> and <rce_evals>
    """

    ### LOAD PREEXISTING EVALS ###
    based_on = "train" if base_predictions_on_train_dfs else "itself"
    eval_path = "classifier-" + classifier_prefix + "-based_on-" + based_on + "-" + features_note
    if csv_folder is not None:
        eval_path = os.path.join(csv_folder, eval_path)

    if not rewrite_existing_models:
        try:
            prauc_evals = pd.read_csv(eval_path + "-prauc.csv")
            rce_evals = pd.read_csv(eval_path + "-rce.csv")
            already_existing_evals = get_already_existing_evals(prauc_evals, rce_evals, flatten)
            if print_progress:
                print(f"Read prauc evals {prauc_evals.shape} and rce evals {rce_evals.shape}.")

        except FileNotFoundError:
            prauc_evals = {}
            rce_evals = {}
            already_existing_evals = set()
            if print_progress:
                print(f"{eval_path}-prauc.csv/-rce.csv do not exist.")
    else:
        prauc_evals = None
        rce_evals = None
        already_existing_evals = set()
        if print_progress:
            print(f"Recreating and reevaluating models for {classifier_prefix}")

    ### LOAD PREEXISTING MODELS ###
    models = {}
    reloaded_models_set = set()
    rewritten_models_set = set()
    skipped_models_set = set()
    # for new evaluations:
    new_prauc = {}
    new_rce = {}

    for sample in dfs:
        models[sample] = {}
        new_prauc[sample] = {}
        new_rce[sample] = {}
        corresponding_train_key = sample.replace("val+test", "train").replace("val", "train").replace("test", "train") \
            if base_predictions_on_train_dfs else sample

        for target in target_features:
            models[sample][target] = {}
            new_prauc[sample][target] = {}
            new_rce[sample][target] = {}

            for feature_selection in features_dict[sample][target]:
                if flatten:
                    next_combo = (classifier_prefix, features_note, based_on, sample, feature_selection, target,)
                    # e.g. ('bayes', 'scaled', 'itself', 'test_random_sample_1pct', 'all', 'like')
                else:
                    next_combo = (classifier_prefix, features_note, based_on, sample, feature_selection,)
                    # e.g. ('bayes', 'scaled', 'itself', 'test_random_sample_1pct', 'all')

                ### PREDICT ###
                full_model_name = "classifier_model_of_type-" + classifier_prefix + \
                                  "-for_features-" + feature_selection + \
                                  "-for_dataset-" + sample + \
                                  "-based_on_dataset-" + corresponding_train_key + \
                                  "-predicting_target-" + target
                model_path = os.path.join(models_folder, full_model_name)

                # Attempt to load the model if allowed and it exists
                if rewrite_existing_models:
                    evaluate_model = True
                    create_model = True
                elif force_model_reloading or (next_combo not in already_existing_evals):
                    try:
                        models[sample][target][feature_selection] = classifier_model_class.load(model_path)
                        reloaded_models_set.add(full_model_name)
                        evaluate_model = True
                        create_model = False
                        if print_progress:
                            print(f"\tLoaded {full_model_name} at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")
                    except (Py4JJavaError, FileNotFoundError) as error:
                        evaluate_model = True
                        create_model = True
                        if print_progress:
                                print(f"\tModel {full_model_name} does not exist, even though its evaluation does.")
                else:
                    evaluate_model = False
                    create_model = False
                    skipped_models_set.add(full_model_name)

                # Recreate the model if necessary
                if create_model:
                    classifier_instance = classifier_class(labelCol=target, featuresCol=features_name)
                    models[sample][target][feature_selection] = classifier_instance.fit(
                        dfs[corresponding_train_key][feature_selection])
                    models[sample][target][feature_selection].write().overwrite().save(model_path)
                    rewritten_models_set.add(full_model_name)

                    if print_progress:
                        print(f"\t\tTraining {full_model_name} done at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

                ### EVALUATE ###
                if evaluate_model:
                    predictions = models[sample][target][feature_selection].transform(dfs[sample][feature_selection])
                    pred = [x[0] for x in predictions.select("prediction").collect()]
                    gt = [x[0] for x in predictions.select(target).collect()]
                    new_prauc[sample][target][feature_selection] = compute_prauc(pred, gt)
                    new_rce[sample][target][feature_selection] = compute_rce(pred, gt)
                    if print_progress:
                        print(f"\t\tEvaluation done for {sample}/{feature_selection}/{target}: "
                              f"{new_prauc[sample][target][feature_selection]}/{new_rce[sample][target][feature_selection]} "
                              f"at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

        if (new_prauc[sample] != {}) and (new_prauc[sample][target_features[0]] != {}):
            prauc_evals = update_evals(old_evals=prauc_evals, new_evals_dict=new_prauc, samples=[sample],
                                               metric_name="prauc", classifier_prefix=classifier_prefix, based_on=based_on,
                                               features_note=features_note, eval_path=eval_path, flatten=flatten)
            rce_evals = update_evals(old_evals=rce_evals, new_evals_dict=new_rce, samples=[sample], metric_name="rce",
                                             classifier_prefix=classifier_prefix, based_on=based_on,
                                             features_note=features_note, eval_path=eval_path, flatten=flatten)
            prauc_evals.to_csv(eval_path + "-prauc.csv", index=False)
            rce_evals.to_csv(eval_path + "-rce.csv", index=False)
            if print_progress:
                print(f"___ Exported all evals for {sample} at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}. ___ ")

    if not flatten:
        prauc_evals.set_index(["algorithm", "note", "trained_on", "sample", "feature_selection"],
                          verify_integrity=True, inplace=True)
        rce_evals.set_index(["algorithm", "note", "trained_on", "sample", "feature_selection"],
                        verify_integrity=True, inplace=True)
    return models, reloaded_models_set, rewritten_models_set, skipped_models_set, prauc_evals, rce_evals
