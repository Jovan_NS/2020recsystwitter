from collections.abc import Iterable
from numpy.random import randint


def prepare_seeds(seeds):
    """
    Checks if <seeds> is None, an iterable of strictly non-negative ints or None values, or a single positive int. In
    the case of the last option, it returns a list of the corresponding large random values.

    Parameters
    ----------
    seeds: the current value of seeds

    Returns
    -------
    A valid iterable of strictly positive large ints.
    """

    if seeds is None:
        return [None]
    if isinstance(seeds, Iterable):
        for s in seeds:
            if s is not None:
                if type(s) != int:
                    raise ValueError("All values of seeds must be int or None; alternative have it be a single positive"
                                     " int or None.")
                if s <= 0:
                    raise ValueError("All values of seeds must be positive or None; alternative have it be a single "
                                     "positive int or None.")

    elif type(seeds) != int:
        raise ValueError("The argument seeds must be an iterable of positive ints or Nones, a single positive int, "
                         "or None.")

    elif type(seeds) == int:
        if seeds <= 0:
            raise ValueError("The argument seeds must be an iterable of positive ints or Nones, a single positive int, "
                             "or None.")

        new_seeds = list(randint(1, high=2 ** 31, size=seeds, dtype=int))
        seeds = new_seeds

    return seeds
