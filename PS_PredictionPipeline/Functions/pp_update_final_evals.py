import pandas as pd

def update_final_evals(old_evals:pd.DataFrame, new_evals_dict:dict, main_key:str=None, evaluated_on:str=None,
                       flatten=False, target_col="target", eval_col="evaluation"):
    """
    This function converts new_evals into a pandas dataframe and then merges it with old_evals (overwriting potential
    duplicates).

    Parameters
    ----------
    old_evals: a Pandas dataframe with old evals with columns ["algorithm", "note", "feature_selection", "trained_on", "evaluated_on"]
    new_evals_dict: a dict with new evals of the form {[main_key = "=".join([])]: [evaluated_on]: [target]: eval_value}}}
    main_key: main_key to be transferred (i.e. which part of the new_evals_dict is really new, None for full transfer)
    evaluated_on: evaluated_on key to be transferred (i.e. which part of the new_evals_dict is really new, None for full transfer
    flatten: whether to only have one eval per row (if false, one row will contain evals for each of the target
        engagements); cf. https://prnt.sc/rG1HiIpkXTig and https://prnt.sc/Gk1sTMvAn-d3
    target_col: column name in the flattened form describing whose values are individual tweet engagement goals
    eval_col: "prauc", "rce", or "evaluation" expected (relevant for flattened form only)

    Returns
    -------
    <updated_evals>
    """
    idx = 0
    df_dict = {}



    ### TRANSFORM NEW EVALS INTO DATAFRAME ###
    #  First, we have to transform new_evals[main_key = "=".join([algorithm, note, feature_selection, trained_on])]: [evaluated_on]: [target]: eval_value}} into
    #  eval_dict[algorithm][note][feature_selection][trained_on][evaluated_on][target]

    if (main_key is not None) and (evaluated_on is not None):
        for target in new_evals_dict[main_key][evaluated_on]:
            df_dict, idx = restructure_evals_dict(df_dict, idx, evaluated_on, flatten, main_key, target_col, eval_col,
                                                  new_evals_dict, target)
    else:
        for main_key in new_evals_dict:
            for evaluated_on in new_evals_dict[main_key]:
                for target in new_evals_dict[main_key][evaluated_on]:
                    df_dict, idx = restructure_evals_dict(df_dict, idx, evaluated_on, flatten, main_key, target_col,
                                                          eval_col, new_evals_dict, target)


    new_df = pd.DataFrame.from_dict(df_dict, orient='index')
    combo_columns = ["algorithm", "note", "feature_selection", "trained_on", "evaluated_on"]
    if flatten:
        combo_columns = combo_columns + [target_col]
    else:
        new_df.index.set_names(combo_columns, inplace=True)
        # We have to temporarily save indices into columns, to be able to merge with old evals easier
        new_df = new_df.reset_index()

    ### MERGE WITH THE EXISTING DF ###

    if old_evals is None or type(old_evals) == dict:
        return new_df

    # https://stackoverflow.com/a/47107164 ; https://stackoverflow.com/a/55543744
    all_df = old_evals.merge(new_df[combo_columns], on=combo_columns, how="left", indicator=True)
    just_relevant_old = all_df[all_df._merge == 'left_only'].drop('_merge', axis=1)
    updated_evals = pd.concat([just_relevant_old, new_df], axis="index", ignore_index=True, sort=False)

    return updated_evals


def restructure_evals_dict(df_dict, idx, evaluated_on, flatten, main_key, target_col, eval_col, new_evals_dict, target):
    # The form of the main key is "=".join([algorithm, note, feature_selection, trained_on])
    algorithm, note, feature_selection, trained_on = main_key.split("=")
    if flatten:
        df_dict[idx] = {"algorithm": algorithm, "note": note, "feature_selection": feature_selection,
                        "trained_on": trained_on, "evaluated_on": evaluated_on, target_col:target,
                        eval_col: new_evals_dict[main_key][evaluated_on][target]}
        idx += 1
    else:
        if (algorithm, note, feature_selection, trained_on, evaluated_on) not in df_dict:
            df_dict[(algorithm, note, feature_selection, trained_on, evaluated_on)] = {}

        df_dict[(algorithm, note, feature_selection, trained_on, evaluated_on)][target] = \
            new_evals_dict[main_key][evaluated_on][target]
    return df_dict, idx
