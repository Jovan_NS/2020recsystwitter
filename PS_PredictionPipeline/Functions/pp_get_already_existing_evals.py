import pandas as pd


def get_already_existing_evals(prauc_evals: pd.DataFrame, rce_evals: pd.DataFrame, flatten, recreate_missing_models):
    """
    Checks what combinations of ["algorithm", "trained_on", "sample", "feature_selection"] were already calculated and
    ensures there are no duplicates. It also performs basic data integrity assertions (same format, same size, same
    number of evals for both PRAUC and RCE).

    Parameters
    ----------
    prauc_evals: PRAUC evaluations dataframe
    rce_evals: RCE evaluations dataframe
    flatten: whether "target" column is in the dataframes
    recreate_missing_models: whether the calling function would recreate missing models, if False, NaNs must be excluded

    Returns
    -------
    already_existing_evals: a set of already existing evaluation combinations
    """

    assert list(prauc_evals.columns) == list(rce_evals.columns)
    assert prauc_evals.shape == rce_evals.shape
    assert flatten == ("target" in prauc_evals.columns)

    # remove rows with NaN values for evaluation, as they ought to be recalculated if recreate_missing_models=False
    if recreate_missing_models:
        prauc_evals = prauc_evals[~prauc_evals.isnull().any(axis=1)]
        rce_evals = rce_evals[~rce_evals.isnull().any(axis=1)]
    assert prauc_evals.shape == rce_evals.shape

    combo_columns = ["algorithm", "note", "trained_on", "sample", "feature_selection"]
    if flatten:
        combo_columns = combo_columns + ["target"]

    # Short form of: https://prnt.sc/s_N-7Xw2Lv9v
    return set(list(map(tuple, prauc_evals[combo_columns].values)))
