from pyspark.ml.evaluation import Evaluator

from Functions.pp_calculate_evaluation_metrics import compute_rce


class RCEEvaluator(Evaluator):

    def __init__(self, predictionCol="prediction", labelCol="label"):
        self.predictionCol = predictionCol
        self.labelCol = labelCol

    def _evaluate(self, dataset):
        # https://stackoverflow.com/questions/51404344/custom-evaluator-in-pyspark
        pred = [x[0] for x in dataset.select(self.predictionCol).collect()]
        gt = [x[0] for x in dataset.select(self.labelCol).collect()]
        return compute_rce(pred=pred, gt=gt)

    def isLargerBetter(self):
        return True