import pyspark.ml.feature as mlf


def vectorise_explanatory_features(dfs,
                                   preselected_features_dict,
                                   featureset_to_features_note_map={"relevant_features":"scaled",},
                                   selections_done_on_train_df=True):

    vec_dfs = {}

    for key in dfs:
        vec_dfs[key] = dfs[key]
        if selections_done_on_train_df:
            corresponding_train_key = key.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = key
        for featureset in preselected_features_dict[corresponding_train_key]:
            note = featureset_to_features_note_map[featureset]
            for eng in preselected_features_dict[corresponding_train_key][featureset]:
                for top_n_selected in preselected_features_dict[corresponding_train_key][featureset][eng]:
                    ev_col = "ev__" + top_n_selected + "__" + note + "__" + eng
                    ev_col = ev_col + "__sdotd" if selections_done_on_train_df else ev_col
                    if ev_col not in vec_dfs[key].columns:
                        vec_assembler = mlf.VectorAssembler(inputCols=preselected_features_dict[corresponding_train_key][featureset][eng][top_n_selected],
                                                            outputCol=ev_col,
                                                            handleInvalid="error")
                        vec_dfs[key] = vec_assembler.transform(vec_dfs[key])

    return vec_dfs
