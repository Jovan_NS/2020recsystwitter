import os
from datetime import datetime
from py4j.protocol import Py4JJavaError
import pyspark.ml.classification as mlc  # for LogisticRegression, LogisticRegressionModel, etc.


def mllib_predict(dfs, features_dict, features_name="selected_features",
                  target_features=['like', 'reply', 'retweet', 'quote', 'react', ],
                  classifier_class=mlc.DecisionTreeClassifier,
                  classifier_model_class=mlc.DecisionTreeClassificationModel,
                  classifier_prefix="tree", models_folder="Models",
                  base_predictions_on_train_dfs=True, rewrite_existing_models=False,
                  print_progress=True):
    """
    This function loops for each dataframe in <dfs> and predicts targets from <target_features> based on explanatory
    features listed in <features_dict>. It uses the classifier from PySpark's mllib specified in <classifier_class>
    with the corresponding model's class specified in <classifier_model_class>.

    Parameters
    ----------
    dfs: a dictionary of dataframes with a vectorised column with explanatory features
    features_dict: a dictionary with the selected description features
    features_name: the name of the vectorised column with explanatory features
    target_features: a list of target labels
    classifier_class: the class of the classifier from PySpark's mllib
    classifier_model_class: the class of the model of the classifier from PySpark's mllib
    classifier_prefix: the natural language name of the classifier
    models_folder: the name of the folder to save fitted classifier models in
    base_predictions_on_train_dfs: whether to fit on the corresponding train dataset instead of the same dataset
    rewrite_existing_models: whether to recreate, refit, and rewrite classifier models
    print_progress: whether to print the fitting and prediction progress

    Returns
    -------
    fitted <models>, the lists of <reloaded_models_list> and <rewritten_models_list>
    """

    models = {}
    reloaded_models_list = []
    rewritten_models_list = []

    for key in dfs:
        models[key] = {}

        if base_predictions_on_train_dfs:
            corresponding_train_key = key.replace("val+test", "train").replace("val", "train").replace("test", "train")
        else:
            corresponding_train_key = key

        print(f"At at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')} creating or loading {classifier_prefix} for "
              f"{key} based on {corresponding_train_key}.")

        for target in target_features:
            models[key][target] = {}

            for sel_features in features_dict[key][target]:
                model_does_not_exist = False
                full_model_name = "classifier_model_of_type-" + classifier_prefix + \
                                  "-for_features-" + sel_features + \
                                  "-for_dataset-" + key + \
                                  "-based_on_dataset-" + corresponding_train_key + \
                                  "-predicting_target-" + target
                model_path = os.path.join(models_folder, full_model_name)

                if not rewrite_existing_models:
                    try:
                        models[key][target][sel_features] = classifier_model_class.load(model_path)
                        reloaded_models_list.append(full_model_name)
                        if print_progress:
                            print(f"\tModel {full_model_name} loaded from file.")
                    except:
                        model_does_not_exist = True
                        if print_progress:
                            print(f"\tModel {full_model_name} must be recreated.")

                if rewrite_existing_models or model_does_not_exist:
                    classifier_instance = classifier_class(labelCol=target, featuresCol=features_name)
                    models[key][target][sel_features] = classifier_instance.fit(dfs[corresponding_train_key][sel_features])
                    rewritten_models_list.append(full_model_name)
                    models[key][target][sel_features].write().overwrite().save(model_path)

                    if print_progress:
                        print(
                            f"\t\tTraining {full_model_name} done at {datetime.now().strftime('%d.%m.%Y %H:%M:%S')}.")

    return models, reloaded_models_list, rewritten_models_list
